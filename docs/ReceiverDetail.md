
# ReceiverDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**area** | [**Area**](Area.md) |  |  [optional]
**areaId** | **String** |  |  [optional]
**attributes** | **Map&lt;String, String&gt;** |  |  [optional]
**building** | [**Building**](Building.md) |  |  [optional]
**lastReported** | [**Date**](Date.md) |  |  [optional]
**parent** | **String** |  |  [optional]
**physicalId** | **String** |  |  [optional]
**position** | [**GeoPoint**](GeoPoint.md) |  |  [optional]
**provisionedAt** | [**Date**](Date.md) |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**tags** | **List&lt;String&gt;** |  |  [optional]
**tenantId** | **String** |  |  [optional]
**tenantName** | **String** |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type |  [optional]
**name** | **String** | name |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNPROVISIONED | &quot;UNPROVISIONED&quot;
PROVISIONED | &quot;PROVISIONED&quot;
UP | &quot;UP&quot;
DOWN | &quot;DOWN&quot;
DEPROVISIONED | &quot;DEPROVISIONED&quot;
SOFTPROVISIONED | &quot;SOFTPROVISIONED&quot;
READY | &quot;READY&quot;



