
# PeripheralCapabilities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capabilities** | [**List&lt;Capability&gt;**](Capability.md) |  |  [optional]
**peripheralActions** | [**List&lt;ActionMeta&gt;**](ActionMeta.md) |  |  [optional]



