
# PropertyOrBuilder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**defaultInstanceForType** | [**Message**](Message.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**name** | **String** |  |  [optional]
**nameBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]
**value** | **String** |  |  [optional]
**valueBytes** | [**ByteString**](ByteString.md) |  |  [optional]



