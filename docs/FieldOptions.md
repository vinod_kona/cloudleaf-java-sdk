
# FieldOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**ctype** | [**CtypeEnum**](#CtypeEnum) |  |  [optional]
**defaultInstanceForType** | [**FieldOptions**](FieldOptions.md) |  |  [optional]
**deprecated** | **Boolean** |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**experimentalMapKey** | **String** |  |  [optional]
**experimentalMapKeyBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**lazy** | **Boolean** |  |  [optional]
**packed** | **Boolean** |  |  [optional]
**parserForType** | [**ParserFieldOptions**](ParserFieldOptions.md) |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]
**uninterpretedOptionCount** | **Integer** |  |  [optional]
**uninterpretedOptionList** | [**List&lt;UninterpretedOption&gt;**](UninterpretedOption.md) |  |  [optional]
**uninterpretedOptionOrBuilderList** | [**List&lt;UninterpretedOptionOrBuilder&gt;**](UninterpretedOptionOrBuilder.md) |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]
**weak** | **Boolean** |  |  [optional]


<a name="CtypeEnum"></a>
## Enum: CtypeEnum
Name | Value
---- | -----
STRING | &quot;STRING&quot;
CORD | &quot;CORD&quot;
STRING_PIECE | &quot;STRING_PIECE&quot;



