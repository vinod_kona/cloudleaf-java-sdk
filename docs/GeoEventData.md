
# GeoEventData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cId** | **String** |  |  [optional]
**eventTime** | **Long** |  |  [optional]
**leafId** | **String** |  |  [optional]
**peripheralId** | **String** |  |  [optional]
**sId** | **String** |  |  [optional]
**time** | **Long** |  |  [optional]
**value** | **String** |  |  [optional]



