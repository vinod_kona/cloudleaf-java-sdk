
# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time** | [**Date**](Date.md) |  |  [optional]
**value** | **Object** |  |  [optional]



