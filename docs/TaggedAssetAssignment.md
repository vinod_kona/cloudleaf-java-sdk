
# TaggedAssetAssignment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetExternalId** | **String** |  |  [optional]
**assetId** | **String** |  |  [optional]
**assetName** | **String** |  |  [optional]
**createdBy** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**lastSeen** | [**Date**](Date.md) |  |  [optional]
**status** | **String** |  |  [optional]
**tagId** | **String** |  |  [optional]
**updatedDt** | [**Date**](Date.md) |  |  [optional]



