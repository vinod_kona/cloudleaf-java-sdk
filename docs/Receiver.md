
# Receiver

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | **String** |  |  [optional]
**physicalId** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**tenantId** | **String** |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type |  [optional]
**name** | **String** | name |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNPROVISIONED | &quot;UNPROVISIONED&quot;
PROVISIONED | &quot;PROVISIONED&quot;
UP | &quot;UP&quot;
DOWN | &quot;DOWN&quot;
DEPROVISIONED | &quot;DEPROVISIONED&quot;
SOFTPROVISIONED | &quot;SOFTPROVISIONED&quot;
READY | &quot;READY&quot;



