# MetadataControllerApi

All URIs are relative to *https://dev.cloudleaf.io/cloudos*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCharacteristicMetaUsingPOST**](MetadataControllerApi.md#addCharacteristicMetaUsingPOST) | **POST** /api/1/meta/peripheral/{pType}/service/{sId}/characteristic/{cId} | add characteristic meta data
[**addDeviceMetadataUsingPOST**](MetadataControllerApi.md#addDeviceMetadataUsingPOST) | **POST** /api/1/meta/peripheral/{pType} | add peripheral metadata
[**addPeripheralTypeUsingPOST**](MetadataControllerApi.md#addPeripheralTypeUsingPOST) | **POST** /api/1/meta/pTypes | add peripheral type
[**addServiceMetadataUsingPOST**](MetadataControllerApi.md#addServiceMetadataUsingPOST) | **POST** /api/1/meta/peripheral/{pType}/service/{sId} | add service meta data
[**deleteCharacteristicMetadataUsingDELETE**](MetadataControllerApi.md#deleteCharacteristicMetadataUsingDELETE) | **DELETE** /api/1/meta/peripheral/{pType}/service/{sId}/characteristic/{cId} | delete characteristic metadata of a ptype
[**deletePTypeMetadataUsingDELETE**](MetadataControllerApi.md#deletePTypeMetadataUsingDELETE) | **DELETE** /api/1/meta/peripheral/{pType} | delete peripheral metadata
[**deletePeripheralTypeUsingDELETE**](MetadataControllerApi.md#deletePeripheralTypeUsingDELETE) | **DELETE** /api/1/meta/pTypes | delete peripheral type
[**deleteServiceMetadataUsingDELETE**](MetadataControllerApi.md#deleteServiceMetadataUsingDELETE) | **DELETE** /api/1/meta/peripheral/{pType}/service/{sId} | delete service metadata of a ptype
[**disablePeripheralTypesForTenantUsingPOST**](MetadataControllerApi.md#disablePeripheralTypesForTenantUsingPOST) | **POST** /api/1/meta/tenant/{tenantId}/pTypes/disable | disable peripheral types for tenant
[**disablePeripheralTypesUsingPOST**](MetadataControllerApi.md#disablePeripheralTypesUsingPOST) | **POST** /api/1/meta/pTypes/disable | disable peripheral types
[**enablePeripheralTypesUsingPOST**](MetadataControllerApi.md#enablePeripheralTypesUsingPOST) | **POST** /api/1/meta/pTypes/enable | enable peripheral types
[**getCapabilityUsingGET**](MetadataControllerApi.md#getCapabilityUsingGET) | **GET** /api/1/meta/{pType}/capability | get cability
[**getCharacteristicMetadataUsingGET**](MetadataControllerApi.md#getCharacteristicMetadataUsingGET) | **GET** /api/1/meta/peripheral/{pType}/service/{sId}/characteristic/{cId} | get characteristic metadata
[**getDeviceMetadataUsingGET**](MetadataControllerApi.md#getDeviceMetadataUsingGET) | **GET** /api/1/meta/peripheral/{pType} | get peripheral metadata
[**getKnownPeripheralTypesUsingGET**](MetadataControllerApi.md#getKnownPeripheralTypesUsingGET) | **GET** /api/1/meta/pTypes | get peripheral types
[**getServiceMetadataUsingGET**](MetadataControllerApi.md#getServiceMetadataUsingGET) | **GET** /api/1/meta/peripheral/{pType}/service/{sId} | get service metadata
[**updateDeviceMetadataUsingPUT**](MetadataControllerApi.md#updateDeviceMetadataUsingPUT) | **PUT** /api/1/meta/peripheral/{pType} | add/update peripheral metadata
[**updatePeripheralTypeImageUsingPUT**](MetadataControllerApi.md#updatePeripheralTypeImageUsingPUT) | **PUT** /api/1/meta/pTypes/image/{ptypeID} | upload image for peripheral type(cc/tag)
[**updatePeripheralTypeUsingPUT**](MetadataControllerApi.md#updatePeripheralTypeUsingPUT) | **PUT** /api/1/meta/pTypes/{typeRegEx} | update peripheral type


<a name="addCharacteristicMetaUsingPOST"></a>
# **addCharacteristicMetaUsingPOST**
> PeripheralMeta addCharacteristicMetaUsingPOST(pType, sId, cId, characteristicMeta)

add characteristic meta data

Uploads characteristic metadata for a peripheral.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
String sId = "sId_example"; // String | service identifier
String cId = "cId_example"; // String | characteristic identifier
CharacteristicMeta characteristicMeta = new CharacteristicMeta(); // CharacteristicMeta | Characteristic metadata
try {
    PeripheralMeta result = apiInstance.addCharacteristicMetaUsingPOST(pType, sId, cId, characteristicMeta);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#addCharacteristicMetaUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |
 **sId** | **String**| service identifier |
 **cId** | **String**| characteristic identifier |
 **characteristicMeta** | [**CharacteristicMeta**](CharacteristicMeta.md)| Characteristic metadata | [optional]

### Return type

[**PeripheralMeta**](PeripheralMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="addDeviceMetadataUsingPOST"></a>
# **addDeviceMetadataUsingPOST**
> PeripheralMeta addDeviceMetadataUsingPOST(pType, deviceMeta)

add peripheral metadata

Uploads metadata for a peripheral. Tasks and IdPath are output only and not used when uploading metadata

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
PeripheralMeta deviceMeta = new PeripheralMeta(); // PeripheralMeta | Peripheral metadata - I/O object used to describe the metadata of a peripheral, its services, characteristics, and the tasks to be performed by CloudLeaf readers to access data on the peripheral
try {
    PeripheralMeta result = apiInstance.addDeviceMetadataUsingPOST(pType, deviceMeta);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#addDeviceMetadataUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |
 **deviceMeta** | [**PeripheralMeta**](PeripheralMeta.md)| Peripheral metadata - I/O object used to describe the metadata of a peripheral, its services, characteristics, and the tasks to be performed by CloudLeaf readers to access data on the peripheral | [optional]

### Return type

[**PeripheralMeta**](PeripheralMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="addPeripheralTypeUsingPOST"></a>
# **addPeripheralTypeUsingPOST**
> String addPeripheralTypeUsingPOST(pType)

add peripheral type

Add a new peripheral type. Returns Status CREATED if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
PeripheralTypeMeta pType = new PeripheralTypeMeta(); // PeripheralTypeMeta | I/O object used to describe peripheral types
try {
    String result = apiInstance.addPeripheralTypeUsingPOST(pType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#addPeripheralTypeUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | [**PeripheralTypeMeta**](PeripheralTypeMeta.md)| I/O object used to describe peripheral types | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="addServiceMetadataUsingPOST"></a>
# **addServiceMetadataUsingPOST**
> PeripheralMeta addServiceMetadataUsingPOST(pType, sId, serviceMeta)

add service meta data

Uploads service metadata for a peripheral type

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
String sId = "sId_example"; // String | service identifier
ServiceMeta serviceMeta = new ServiceMeta(); // ServiceMeta | Service metadata
try {
    PeripheralMeta result = apiInstance.addServiceMetadataUsingPOST(pType, sId, serviceMeta);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#addServiceMetadataUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |
 **sId** | **String**| service identifier |
 **serviceMeta** | [**ServiceMeta**](ServiceMeta.md)| Service metadata | [optional]

### Return type

[**PeripheralMeta**](PeripheralMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteCharacteristicMetadataUsingDELETE"></a>
# **deleteCharacteristicMetadataUsingDELETE**
> String deleteCharacteristicMetadataUsingDELETE(pType, sId, cId)

delete characteristic metadata of a ptype

Delete characteristic meta data of a ptype. Returns Status OK if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
String sId = "sId_example"; // String | service identifier
String cId = "cId_example"; // String | characteristic identifier
try {
    String result = apiInstance.deleteCharacteristicMetadataUsingDELETE(pType, sId, cId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#deleteCharacteristicMetadataUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |
 **sId** | **String**| service identifier |
 **cId** | **String**| characteristic identifier |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="deletePTypeMetadataUsingDELETE"></a>
# **deletePTypeMetadataUsingDELETE**
> String deletePTypeMetadataUsingDELETE(pType)

delete peripheral metadata

Delete peripheral metadata. Returns Status OK if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
try {
    String result = apiInstance.deletePTypeMetadataUsingDELETE(pType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#deletePTypeMetadataUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="deletePeripheralTypeUsingDELETE"></a>
# **deletePeripheralTypeUsingDELETE**
> String deletePeripheralTypeUsingDELETE(regEx)

delete peripheral type

Delete peripheral type. Returns Status OK if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String regEx = "regEx_example"; // String | Peripheral type identifier
try {
    String result = apiInstance.deletePeripheralTypeUsingDELETE(regEx);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#deletePeripheralTypeUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **regEx** | **String**| Peripheral type identifier |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="deleteServiceMetadataUsingDELETE"></a>
# **deleteServiceMetadataUsingDELETE**
> String deleteServiceMetadataUsingDELETE(pType, sId)

delete service metadata of a ptype

Delete service meta data of a ptype. Returns Status OK if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
String sId = "sId_example"; // String | service identifier
try {
    String result = apiInstance.deleteServiceMetadataUsingDELETE(pType, sId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#deleteServiceMetadataUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |
 **sId** | **String**| service identifier |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="disablePeripheralTypesForTenantUsingPOST"></a>
# **disablePeripheralTypesForTenantUsingPOST**
> String disablePeripheralTypesForTenantUsingPOST(tenantId, types)

disable peripheral types for tenant

Disable peripheral types for this partner. Returns Status OK if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String tenantId = "tenantId_example"; // String | tenantId of the tenant
List<String> types = Arrays.asList(new List<String>()); // List<String> | Comma sperated list of peripheral types
try {
    String result = apiInstance.disablePeripheralTypesForTenantUsingPOST(tenantId, types);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#disablePeripheralTypesForTenantUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenantId** | **String**| tenantId of the tenant |
 **types** | **List&lt;String&gt;**| Comma sperated list of peripheral types | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="disablePeripheralTypesUsingPOST"></a>
# **disablePeripheralTypesUsingPOST**
> String disablePeripheralTypesUsingPOST(types)

disable peripheral types

Disable peripheral types for this partner. Returns Status OK if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
List<String> types = Arrays.asList(new List<String>()); // List<String> | Comma sperated list of peripheral types
try {
    String result = apiInstance.disablePeripheralTypesUsingPOST(types);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#disablePeripheralTypesUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **types** | **List&lt;String&gt;**| Comma sperated list of peripheral types | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="enablePeripheralTypesUsingPOST"></a>
# **enablePeripheralTypesUsingPOST**
> String enablePeripheralTypesUsingPOST(types)

enable peripheral types

Enable peripheral types for this partner. Returns Status OK if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
List<String> types = Arrays.asList(new List<String>()); // List<String> | Comma sperated list of peripheral types
try {
    String result = apiInstance.enablePeripheralTypesUsingPOST(types);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#enablePeripheralTypesUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **types** | **List&lt;String&gt;**| Comma sperated list of peripheral types | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getCapabilityUsingGET"></a>
# **getCapabilityUsingGET**
> PeripheralCapabilities getCapabilityUsingGET(pType)

get cability

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | pType
try {
    PeripheralCapabilities result = apiInstance.getCapabilityUsingGET(pType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#getCapabilityUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| pType |

### Return type

[**PeripheralCapabilities**](PeripheralCapabilities.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCharacteristicMetadataUsingGET"></a>
# **getCharacteristicMetadataUsingGET**
> ServiceMeta getCharacteristicMetadataUsingGET(pType, sId, cId)

get characteristic metadata

Get characteristic metadata 

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
String sId = "sId_example"; // String | service identifier
String cId = "cId_example"; // String | characteristic identifier
try {
    ServiceMeta result = apiInstance.getCharacteristicMetadataUsingGET(pType, sId, cId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#getCharacteristicMetadataUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |
 **sId** | **String**| service identifier |
 **cId** | **String**| characteristic identifier |

### Return type

[**ServiceMeta**](ServiceMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getDeviceMetadataUsingGET"></a>
# **getDeviceMetadataUsingGET**
> PeripheralMeta getDeviceMetadataUsingGET(pType, details)

get peripheral metadata

Get peripheral metadata for the type Id specified with or without details.  &#39;details&#x3D;false&#39; returns the IdPath and the Tasks for the peripheral. These provide information on how to extract the peripheral&#39;s instance Id and the operations to extract the data streams. If &#39;details&#x3D;true&#39;, the peripheral&#39;s service metadata is also returned

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
Boolean details = false; // Boolean | details
try {
    PeripheralMeta result = apiInstance.getDeviceMetadataUsingGET(pType, details);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#getDeviceMetadataUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |
 **details** | **Boolean**| details | [optional] [default to false]

### Return type

[**PeripheralMeta**](PeripheralMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getKnownPeripheralTypesUsingGET"></a>
# **getKnownPeripheralTypesUsingGET**
> List&lt;PeripheralTypeMeta&gt; getKnownPeripheralTypesUsingGET(status)

get peripheral types

Retrieve peripheral types - returns a list of regular expressions to match with the peripheral name

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String status = "status_example"; // String | status
try {
    List<PeripheralTypeMeta> result = apiInstance.getKnownPeripheralTypesUsingGET(status);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#getKnownPeripheralTypesUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **String**| status |

### Return type

[**List&lt;PeripheralTypeMeta&gt;**](PeripheralTypeMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getServiceMetadataUsingGET"></a>
# **getServiceMetadataUsingGET**
> ServiceMeta getServiceMetadataUsingGET(pType, sId, details)

get service metadata

Get service metadata 

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
String sId = "sId_example"; // String | service identifier
String details = "false"; // String | details
try {
    ServiceMeta result = apiInstance.getServiceMetadataUsingGET(pType, sId, details);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#getServiceMetadataUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |
 **sId** | **String**| service identifier |
 **details** | **String**| details | [optional] [default to false]

### Return type

[**ServiceMeta**](ServiceMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateDeviceMetadataUsingPUT"></a>
# **updateDeviceMetadataUsingPUT**
> PeripheralMeta updateDeviceMetadataUsingPUT(pType, deviceMeta)

add/update peripheral metadata

Similar to the POST operation - Updates metadata for a peripheral

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String pType = "pType_example"; // String | Peripheral type identifier
PeripheralMeta deviceMeta = new PeripheralMeta(); // PeripheralMeta | Peripheral metadata
try {
    PeripheralMeta result = apiInstance.updateDeviceMetadataUsingPUT(pType, deviceMeta);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#updateDeviceMetadataUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pType** | **String**| Peripheral type identifier |
 **deviceMeta** | [**PeripheralMeta**](PeripheralMeta.md)| Peripheral metadata | [optional]

### Return type

[**PeripheralMeta**](PeripheralMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updatePeripheralTypeImageUsingPUT"></a>
# **updatePeripheralTypeImageUsingPUT**
> String updatePeripheralTypeImageUsingPUT(ptypeID, file, deviceType)

upload image for peripheral type(cc/tag)

upload image of peripheral type(cc/tag). Returns Status OK if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String ptypeID = "ptypeID_example"; // String | Peripheral type regular expression
File file = new File("/path/to/file.txt"); // File | File Upload Key
String deviceType = "deviceType_example"; // String | tag/cc
try {
    String result = apiInstance.updatePeripheralTypeImageUsingPUT(ptypeID, file, deviceType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#updatePeripheralTypeImageUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ptypeID** | **String**| Peripheral type regular expression |
 **file** | **File**| File Upload Key |
 **deviceType** | **String**| tag/cc | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: */*

<a name="updatePeripheralTypeUsingPUT"></a>
# **updatePeripheralTypeUsingPUT**
> String updatePeripheralTypeUsingPUT(typeRegEx, pType)

update peripheral type

Update peripheral type. Returns Status CREATED if successful.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;


MetadataControllerApi apiInstance = new MetadataControllerApi();
String typeRegEx = "typeRegEx_example"; // String | Peripheral type regular expression
PeripheralTypeMeta pType = new PeripheralTypeMeta(); // PeripheralTypeMeta | I/O object used to describe peripheral types
try {
    String result = apiInstance.updatePeripheralTypeUsingPUT(typeRegEx, pType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetadataControllerApi#updatePeripheralTypeUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **typeRegEx** | **String**| Peripheral type regular expression |
 **pType** | [**PeripheralTypeMeta**](PeripheralTypeMeta.md)| I/O object used to describe peripheral types | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

