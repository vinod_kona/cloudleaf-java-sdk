
# TenantProcessRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applications** | [**List&lt;ApplicationProcessRule&gt;**](ApplicationProcessRule.md) |  |  [optional]
**tenantId** | **String** |  |  [optional]



