
# MethodDescriptor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**FileDescriptor**](FileDescriptor.md) |  |  [optional]
**fullName** | **String** |  |  [optional]
**index** | **Integer** |  |  [optional]
**inputType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**name** | **String** |  |  [optional]
**options** | [**MethodOptions**](MethodOptions.md) |  |  [optional]
**outputType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**service** | [**ServiceDescriptor**](ServiceDescriptor.md) |  |  [optional]



