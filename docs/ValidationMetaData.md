
# ValidationMetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**validationMetaData** | [**Map&lt;String, Map&lt;String, String&gt;&gt;**](Map.md) |  |  [optional]



