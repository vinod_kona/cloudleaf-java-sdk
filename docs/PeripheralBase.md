
# PeripheralBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**peripheralId** | **String** | Peripheral Id | 
**type** | **String** | Peripheral type | 
**deviceClass** | **String** | Peripheral Device Class |  [optional]
**name** | **String** | Name |  [optional]
**status** | [**StatusEnum**](#StatusEnum) | Status |  [optional]
**lastUpdate** | [**Date**](Date.md) | Time of last data update from this peripheral |  [optional]
**tenantId** | **String** | Owning tenant |  [optional]
**physicalId** | **String** | Physical Id, e.g., the MAC Address |  [optional]
**createdAt** | [**Date**](Date.md) | Peripheral provisioning date |  [optional]
**categoryId** | **String** | category Id |  [optional]
**assetId** | **String** | asset Id |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNPROVISIONED | &quot;UNPROVISIONED&quot;
PROVISIONED | &quot;PROVISIONED&quot;
DISABLED | &quot;DISABLED&quot;



