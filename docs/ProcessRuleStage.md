
# ProcessRuleStage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ruleset** | [**ProcessRuleSet**](ProcessRuleSet.md) |  |  [optional]
**stageId** | **String** |  |  [optional]



