
# AccessToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expiry** | [**Date**](Date.md) |  |  [optional]
**token** | **String** |  |  [optional]



