
# UninterpretedOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aggregateValue** | **String** |  |  [optional]
**aggregateValueBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**defaultInstanceForType** | [**UninterpretedOption**](UninterpretedOption.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**doubleValue** | **Double** |  |  [optional]
**identifierValue** | **String** |  |  [optional]
**identifierValueBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**nameCount** | **Integer** |  |  [optional]
**nameList** | [**List&lt;NamePart&gt;**](NamePart.md) |  |  [optional]
**nameOrBuilderList** | [**List&lt;NamePartOrBuilder&gt;**](NamePartOrBuilder.md) |  |  [optional]
**negativeIntValue** | **Long** |  |  [optional]
**parserForType** | [**ParserUninterpretedOption**](ParserUninterpretedOption.md) |  |  [optional]
**positiveIntValue** | **Long** |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]
**stringValue** | [**ByteString**](ByteString.md) |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]



