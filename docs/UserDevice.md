
# UserDevice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appPlatform** | [**AppPlatformEnum**](#AppPlatformEnum) |  |  [optional]
**confirmationKey** | **String** |  |  [optional]
**deviceIdentifier** | **String** |  |  [optional]
**deviceName** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**providerDetails** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**transport** | [**TransportEnum**](#TransportEnum) |  |  [optional]
**userId** | **String** |  |  [optional]


<a name="AppPlatformEnum"></a>
## Enum: AppPlatformEnum
Name | Value
---- | -----
IOS | &quot;ios&quot;
ANDROID | &quot;android&quot;
WINDOWS | &quot;windows&quot;


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNSUBSCRIBED | &quot;UNSUBSCRIBED&quot;
PENDING_CONFIRMATION | &quot;PENDING_CONFIRMATION&quot;
CONFIRMED | &quot;CONFIRMED&quot;
SUBSCRIBED | &quot;SUBSCRIBED&quot;
UNREACHABLE | &quot;UNREACHABLE&quot;


<a name="TransportEnum"></a>
## Enum: TransportEnum
Name | Value
---- | -----
WEB | &quot;web&quot;
EMAIL | &quot;email&quot;
APPLICATION | &quot;application&quot;
PUBNUB | &quot;pubnub&quot;
SMS | &quot;sms&quot;
VOICE | &quot;voice&quot;



