
# SensorTagSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**health** | [**DeviceEventResult**](DeviceEventResult.md) |  |  [optional]
**heartBeat** | [**DeviceEventResult**](DeviceEventResult.md) |  |  [optional]
**sensorTag** | [**SensorTag**](SensorTag.md) |  |  [optional]
**taggedAsset** | [**TaggedAsset**](TaggedAsset.md) |  |  [optional]



