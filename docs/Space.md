
# Space

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bounds** | [**BoundingBox**](BoundingBox.md) |  |  [optional]
**locus** | [**Point**](Point.md) |  |  [optional]
**order** | **Integer** |  |  [optional]
**tags** | **List&lt;String&gt;** |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type | 
**name** | **String** | name |  [optional]



