
# AssetsSensorTag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | [**List&lt;Asset&gt;**](Asset.md) |  |  [optional]
**sensorTags** | [**List&lt;SensorTag&gt;**](SensorTag.md) |  |  [optional]



