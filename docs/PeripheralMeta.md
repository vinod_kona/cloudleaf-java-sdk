
# PeripheralMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mobilityState** | [**MobilityStateEnum**](#MobilityStateEnum) |  |  [optional]
**services** | [**List&lt;ServiceMeta&gt;**](ServiceMeta.md) | Service metadata list |  [optional]
**tasks** | [**Map&lt;String, List&lt;Command&gt;&gt;**](List.md) |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type |  [optional]
**idPath** | [**IdCommand**](IdCommand.md) | ID/ extraction command for peripheral Command Meta Info |  [optional]
**name** | **String** | name |  [optional]


<a name="MobilityStateEnum"></a>
## Enum: MobilityStateEnum
Name | Value
---- | -----
FIXED | &quot;FIXED&quot;
MOBILE | &quot;MOBILE&quot;
FENCED | &quot;FENCED&quot;



