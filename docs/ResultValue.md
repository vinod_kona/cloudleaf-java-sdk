
# ResultValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reporterId** | **Object** |  |  [optional]
**reporterName** | **Object** |  |  [optional]
**time** | [**Date**](Date.md) |  |  [optional]
**value** | **Object** |  |  [optional]



