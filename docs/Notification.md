
# Notification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**Date**](Date.md) |  |  [optional]
**deliveredAt** | [**Date**](Date.md) |  |  [optional]
**level** | **String** |  |  [optional]
**message** | [**ClfMessage**](ClfMessage.md) |  |  [optional]
**source** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**topic** | [**NotificationTopic**](NotificationTopic.md) |  |  [optional]
**transport** | [**TransportEnum**](#TransportEnum) |  |  [optional]
**userId** | **String** |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNREAD | &quot;UNREAD&quot;
READ | &quot;READ&quot;
DELETED | &quot;DELETED&quot;


<a name="TransportEnum"></a>
## Enum: TransportEnum
Name | Value
---- | -----
WEB | &quot;web&quot;
EMAIL | &quot;email&quot;
APPLICATION | &quot;application&quot;
PUBNUB | &quot;pubnub&quot;
SMS | &quot;sms&quot;
VOICE | &quot;voice&quot;



