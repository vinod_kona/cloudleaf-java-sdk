
# DeviceEventResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceId** | **String** |  |  [optional]
**deviceType** | **String** |  |  [optional]
**events** | [**List&lt;EventDataResult&gt;**](EventDataResult.md) |  |  [optional]



