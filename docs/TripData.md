
# TripData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**destination** | **String** |  |  [optional]
**destinationId** | **String** |  |  [optional]
**distance** | **Double** |  |  [optional]
**dwellTime** | **Long** |  |  [optional]
**entry** | [**Date**](Date.md) |  |  [optional]
**exit** | [**Date**](Date.md) |  |  [optional]
**origin** | **String** |  |  [optional]
**originId** | **String** |  |  [optional]



