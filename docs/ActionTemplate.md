
# ActionTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicableTriggerTemplates** | **List&lt;String&gt;** |  |  [optional]
**description** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**inputForm** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**preResolvedParams** | **Map&lt;String, String&gt;** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**tenantId** | **String** |  |  [optional]
**type** | **String** |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
PARTIAL | &quot;PARTIAL&quot;
RESOLVED | &quot;RESOLVED&quot;
ARCHIVED | &quot;ARCHIVED&quot;



