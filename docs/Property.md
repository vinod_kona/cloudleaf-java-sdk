
# Property

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**defaultInstanceForType** | [**Property**](Property.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**name** | **String** |  |  [optional]
**nameBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**parserForType** | [**ParserProperty**](ParserProperty.md) |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]
**value** | **String** |  |  [optional]
**valueBytes** | [**ByteString**](ByteString.md) |  |  [optional]



