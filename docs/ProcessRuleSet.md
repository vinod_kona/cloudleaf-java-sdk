
# ProcessRuleSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**ruleClass** | **String** |  |  [optional]
**ruleId** | **String** |  |  [optional]
**rules** | [**List&lt;ProcessRule&gt;**](ProcessRule.md) |  |  [optional]



