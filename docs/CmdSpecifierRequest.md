
# CmdSpecifierRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**argv** | **List&lt;String&gt;** |  |  [optional]
**cmd** | **String** |  |  [optional]



