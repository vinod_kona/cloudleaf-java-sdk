
# BatteryStatResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetExternalId** | **String** |  |  [optional]
**assetName** | **String** |  |  [optional]
**assetType** | **String** |  |  [optional]
**bLevel** | **String** |  |  [optional]
**lastLocation** | **String** |  |  [optional]
**lastUpdated** | **String** |  |  [optional]
**taggedAssetId** | **String** |  |  [optional]



