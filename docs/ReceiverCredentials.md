
# ReceiverCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **String** |  |  [optional]
**apiSecret** | **String** |  |  [optional]
**leafId** | **String** |  |  [optional]
**receiverId** | **String** |  |  [optional]



