
# PropertyMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appType** | [**AppTypeEnum**](#AppTypeEnum) |  |  [optional]
**defaultValue** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**keyName** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**required** | **Boolean** |  |  [optional]
**simpleClassName** | **String** |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]


<a name="AppTypeEnum"></a>
## Enum: AppTypeEnum
Name | Value
---- | -----
TENANT | &quot;TENANT&quot;
USER | &quot;USER&quot;
USER_DEVICE | &quot;USER_DEVICE&quot;
SITE | &quot;SITE&quot;
AREA | &quot;AREA&quot;
RECEIVER | &quot;RECEIVER&quot;
PERIPHERAL | &quot;PERIPHERAL&quot;
CATEGORY | &quot;CATEGORY&quot;
PERIPHERALMETA | &quot;PERIPHERALMETA&quot;
ASSET | &quot;ASSET&quot;
SENSORTAG | &quot;SENSORTAG&quot;
TAGGEDASSET | &quot;TAGGEDASSET&quot;
RULES | &quot;RULES&quot;
ROLE | &quot;ROLE&quot;
CHANGELOG | &quot;changelog&quot;
TRIGGER_TEMPLATE | &quot;TRIGGER_TEMPLATE&quot;
ACTION_TEMPLATE | &quot;ACTION_TEMPLATE&quot;
RECIPE | &quot;RECIPE&quot;
SYSRECIPE | &quot;SYSRECIPE&quot;
NOTIFLEVEL | &quot;NOTIFLEVEL&quot;
OTHER | &quot;OTHER&quot;
SUBSCRIPTION | &quot;SUBSCRIPTION&quot;
USERGROUP | &quot;USERGROUP&quot;


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
KNOWN_APPLICATION_TYPE | &quot;KNOWN_APPLICATION_TYPE&quot;
JAVA_CLASS_TYPE | &quot;JAVA_CLASS_TYPE&quot;



