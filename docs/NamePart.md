
# NamePart

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**defaultInstanceForType** | [**NamePart**](NamePart.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**isExtension** | **Boolean** |  |  [optional]
**namePart** | **String** |  |  [optional]
**namePartBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**parserForType** | [**ParserNamePart**](ParserNamePart.md) |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]



