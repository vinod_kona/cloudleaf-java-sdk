
# ReceiverConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allowedTypes** | [**List&lt;PeripheralTypeMeta&gt;**](PeripheralTypeMeta.md) |  |  [optional]
**children** | [**List&lt;ReceiverConfig&gt;**](ReceiverConfig.md) |  |  [optional]
**config** | **Map&lt;String, String&gt;** |  |  [optional]
**id** | **String** |  |  [optional]
**mqttConfig** | [**MQTTConfigBean**](MQTTConfigBean.md) |  |  [optional]
**parent** | **String** |  |  [optional]
**physicalId** | **String** |  |  [optional]
**serverTime** | **Long** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**tenantId** | **String** |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type |  [optional]
**name** | **String** | name |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNPROVISIONED | &quot;UNPROVISIONED&quot;
PROVISIONED | &quot;PROVISIONED&quot;
UP | &quot;UP&quot;
DOWN | &quot;DOWN&quot;
DEPROVISIONED | &quot;DEPROVISIONED&quot;
SOFTPROVISIONED | &quot;SOFTPROVISIONED&quot;
READY | &quot;READY&quot;



