
# Building

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**PostalAddress**](PostalAddress.md) |  |  [optional]
**bounds** | [**BoundingBox**](BoundingBox.md) |  |  [optional]
**locus** | [**Point**](Point.md) |  |  [optional]
**numFloors** | **Integer** |  |  [optional]
**order** | **Integer** |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**tags** | **List&lt;String&gt;** |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type | 
**name** | **String** | name |  [optional]



