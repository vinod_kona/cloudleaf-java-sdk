
# State

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**type** | [**DataType**](DataType.md) |  |  [optional]



