
# DeviceEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceId** | **String** |  |  [optional]
**deviceType** | **String** |  |  [optional]
**level** | **String** |  |  [optional]
**payload** | [**Map&lt;String, List&lt;EventData&gt;&gt;**](List.md) |  |  [optional]
**time** | [**Date**](Date.md) |  |  [optional]



