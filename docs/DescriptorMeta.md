
# DescriptorMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**standard** | **Boolean** |  |  [optional]
**value** | **String** |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type |  [optional]
**name** | **String** | name |  [optional]



