
# TenantAndAdmin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**Customer**](Customer.md) |  |  [optional]
**userDTO** | [**UserDTO**](UserDTO.md) |  |  [optional]



