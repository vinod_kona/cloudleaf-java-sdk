
# TaggedAsset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**areaName** | **String** |  |  [optional]
**asset** | [**Asset**](Asset.md) |  |  [optional]
**assetState** | **String** |  |  [optional]
**buildingName** | **String** |  |  [optional]
**categoryImageURL** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**currentArea** | **String** |  |  [optional]
**currentAreaId** | **String** |  |  [optional]
**currentConnector** | **String** |  |  [optional]
**currentConnectorId** | **String** |  |  [optional]
**currentConnectorName** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**lastAreaName** | **String** |  |  [optional]
**lastBuildingName** | **String** |  |  [optional]
**lastSeen** | [**Date**](Date.md) |  |  [optional]
**modifiedAt** | [**Date**](Date.md) |  |  [optional]
**position** | [**GeoPoint**](GeoPoint.md) |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**sensorTag** | [**SensorTag**](SensorTag.md) |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**tagImageURL** | **String** |  |  [optional]
**tenantId** | **String** |  |  [optional]
**tenantName** | **String** |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNPROVISIONED | &quot;UNPROVISIONED&quot;
PROVISIONED | &quot;PROVISIONED&quot;
DISABLED | &quot;DISABLED&quot;



