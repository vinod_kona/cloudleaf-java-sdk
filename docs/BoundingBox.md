
# BoundingBox

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bottomRight** | [**Point**](Point.md) |  |  [optional]
**topLeft** | [**Point**](Point.md) |  |  [optional]



