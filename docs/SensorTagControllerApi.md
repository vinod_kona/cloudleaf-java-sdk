# SensorTagControllerApi

All URIs are relative to *https://dev.cloudleaf.io/cloudos*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSensorTagUsingPOST**](SensorTagControllerApi.md#createSensorTagUsingPOST) | **POST** /api/1/sensortag | create sensor tag
[**createSensorTagsUsingPOST**](SensorTagControllerApi.md#createSensorTagsUsingPOST) | **POST** /api/1/sensortag/bulkcreate | create sensor tags
[**deProvisionMultipleTaggedAssetUsingPOST**](SensorTagControllerApi.md#deProvisionMultipleTaggedAssetUsingPOST) | **POST** /api/1/taggedasset/deprovision | de-provision Multiple tagged TaggedAssets
[**deProvisionTaggedAssetUsingDELETE**](SensorTagControllerApi.md#deProvisionTaggedAssetUsingDELETE) | **DELETE** /api/1/taggedasset/{id} | de-provision TaggedAsset by ID
[**deleteSensorTagUsingDELETE**](SensorTagControllerApi.md#deleteSensorTagUsingDELETE) | **DELETE** /api/1/sensortag/{id} | delete tag by ID
[**deleteSensorTagsUsingPOST**](SensorTagControllerApi.md#deleteSensorTagsUsingPOST) | **POST** /api/1/sensortag/bulkdelete | delete tags in bulk
[**deleteTagStaticMetaInBulkUsingDELETE**](SensorTagControllerApi.md#deleteTagStaticMetaInBulkUsingDELETE) | **DELETE** /api/1/sensortag/staticmeta/bulk | update tag static meta in bulk
[**executeTagActionUsingPOST1**](SensorTagControllerApi.md#executeTagActionUsingPOST1) | **POST** /api/1/taggedasset/{id}/action | execute tag action
[**findAllSensorTagsByTypeUsingGET**](SensorTagControllerApi.md#findAllSensorTagsByTypeUsingGET) | **GET** /api/1/sensortags/type/{type} | find all tags for a tenant by type
[**findAllSensorTagsUsingGET**](SensorTagControllerApi.md#findAllSensorTagsUsingGET) | **GET** /api/1/sensortags/status/{status} | find all tags for a tenant by status
[**findAllSensorTagsUsingGET1**](SensorTagControllerApi.md#findAllSensorTagsUsingGET1) | **GET** /api/1/sensortags | find all tags for a tenant
[**findAllTaggedAssetsExportUsingGET**](SensorTagControllerApi.md#findAllTaggedAssetsExportUsingGET) | **GET** /api/1/taggedassets/export | Export Assets Snapshot
[**findAllTaggedAssetsUsingGET**](SensorTagControllerApi.md#findAllTaggedAssetsUsingGET) | **GET** /api/1/taggedassets | find all tagged assets for a tenant
[**findTaggedAssetsByAssetTypeUsingGET**](SensorTagControllerApi.md#findTaggedAssetsByAssetTypeUsingGET) | **GET** /api/1/taggedassets/type/{type} | find all tagged assets of a certain asset type for a tenant
[**geTaggedAssetAssignmentUsingGET**](SensorTagControllerApi.md#geTaggedAssetAssignmentUsingGET) | **GET** /api/1/taggedAsset/assignment/{id} | find tagged asset assignment
[**getAssetDataForAreaUsingGET3**](SensorTagControllerApi.md#getAssetDataForAreaUsingGET3) | **GET** /api/1/taggedasset/category/{catId}/area/{areaId} | find all tagged assets for a given area and category
[**getHistoryByAssetUsingGET**](SensorTagControllerApi.md#getHistoryByAssetUsingGET) | **GET** /api/1/taggedAsset/history/asset/{id} | find tagged asset history by asset
[**getSensorTagDetailsUsingGET**](SensorTagControllerApi.md#getSensorTagDetailsUsingGET) | **GET** /api/1/sensortagdetail/{id} | get sensortag detais 
[**getSensorTagUsingGET**](SensorTagControllerApi.md#getSensorTagUsingGET) | **GET** /api/1/sensortag/{id} | find tag by ID
[**getTaggedAssetByAssetUsingGET**](SensorTagControllerApi.md#getTaggedAssetByAssetUsingGET) | **GET** /api/1/taggedasset/asset/{id} | find tagged asset by Asset ID
[**getTaggedAssetBySensorTagUsingGET**](SensorTagControllerApi.md#getTaggedAssetBySensorTagUsingGET) | **GET** /api/1/taggedasset/sensorTag/{id} | find tagged asset by SensorTag ID
[**getTaggedAssetUsingGET**](SensorTagControllerApi.md#getTaggedAssetUsingGET) | **GET** /api/1/taggedasset/{id} | find tagged asset by ID
[**provisionTaggedAssetUsingPOST**](SensorTagControllerApi.md#provisionTaggedAssetUsingPOST) | **POST** /api/1/taggedasset | provision sensor tag
[**updateSensorTagUsingPUT**](SensorTagControllerApi.md#updateSensorTagUsingPUT) | **PUT** /api/1/sensortag/{id} | save/update tag
[**updateSensorTagUsingPUT1**](SensorTagControllerApi.md#updateSensorTagUsingPUT1) | **PUT** /api/1/taggedasset/{id} | save/update TaggedAsset
[**updateTagStaticMetaInBulkUsingPOST**](SensorTagControllerApi.md#updateTagStaticMetaInBulkUsingPOST) | **POST** /api/1/sensortag/staticmeta/bulk | create tag static meta in bulk


<a name="createSensorTagUsingPOST"></a>
# **createSensorTagUsingPOST**
> String createSensorTagUsingPOST(sensorTag)

create sensor tag

create sensor tag with given details

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
SensorTag sensorTag = new SensorTag(); // SensorTag | Sensor tag info to store or update
try {
    String result = apiInstance.createSensorTagUsingPOST(sensorTag);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#createSensorTagUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sensorTag** | [**SensorTag**](SensorTag.md)| Sensor tag info to store or update |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createSensorTagsUsingPOST"></a>
# **createSensorTagsUsingPOST**
> String createSensorTagsUsingPOST(sensorTags)

create sensor tags

create sensor tag with given details

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
List<SensorTag> sensorTags = Arrays.asList(new SensorTag()); // List<SensorTag> | Sensor tags info to store or update
try {
    String result = apiInstance.createSensorTagsUsingPOST(sensorTags);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#createSensorTagsUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sensorTags** | [**List&lt;SensorTag&gt;**](SensorTag.md)| Sensor tags info to store or update |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deProvisionMultipleTaggedAssetUsingPOST"></a>
# **deProvisionMultipleTaggedAssetUsingPOST**
> String deProvisionMultipleTaggedAssetUsingPOST(ids)

de-provision Multiple tagged TaggedAssets

De-provision a tag. Returns Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String ids = "ids_example"; // String | Comma delimited TaggedAsset Ids
try {
    String result = apiInstance.deProvisionMultipleTaggedAssetUsingPOST(ids);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#deProvisionMultipleTaggedAssetUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | **String**| Comma delimited TaggedAsset Ids |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="deProvisionTaggedAssetUsingDELETE"></a>
# **deProvisionTaggedAssetUsingDELETE**
> String deProvisionTaggedAssetUsingDELETE(id)

de-provision TaggedAsset by ID

De-provision a tag. Returns Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | TaggedAsset Id
try {
    String result = apiInstance.deProvisionTaggedAssetUsingDELETE(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#deProvisionTaggedAssetUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| TaggedAsset Id |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="deleteSensorTagUsingDELETE"></a>
# **deleteSensorTagUsingDELETE**
> Object deleteSensorTagUsingDELETE(id)

delete tag by ID

Delete a tag by id. Returns Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | Tag Id
try {
    Object result = apiInstance.deleteSensorTagUsingDELETE(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#deleteSensorTagUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Tag Id |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="deleteSensorTagsUsingPOST"></a>
# **deleteSensorTagsUsingPOST**
> List&lt;SensorTag&gt; deleteSensorTagsUsingPOST(sensorTags)

delete tags in bulk

Delete a tags. Returns Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
List<SensorTag> sensorTags = Arrays.asList(new SensorTag()); // List<SensorTag> | Sensor tags to delete
try {
    List<SensorTag> result = apiInstance.deleteSensorTagsUsingPOST(sensorTags);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#deleteSensorTagsUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sensorTags** | [**List&lt;SensorTag&gt;**](SensorTag.md)| Sensor tags to delete |

### Return type

[**List&lt;SensorTag&gt;**](SensorTag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="deleteTagStaticMetaInBulkUsingDELETE"></a>
# **deleteTagStaticMetaInBulkUsingDELETE**
> List&lt;CsvDataStatus&gt; deleteTagStaticMetaInBulkUsingDELETE(file)

update tag static meta in bulk

Return Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
File file = new File("/path/to/file.txt"); // File | file
try {
    List<CsvDataStatus> result = apiInstance.deleteTagStaticMetaInBulkUsingDELETE(file);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#deleteTagStaticMetaInBulkUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **File**| file |

### Return type

[**List&lt;CsvDataStatus&gt;**](CsvDataStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: */*

<a name="executeTagActionUsingPOST1"></a>
# **executeTagActionUsingPOST1**
> MqttResponse executeTagActionUsingPOST1(id, input)

execute tag action

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | id
MqttRequest input = new MqttRequest(); // MqttRequest | input
try {
    MqttResponse result = apiInstance.executeTagActionUsingPOST1(id, input);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#executeTagActionUsingPOST1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |
 **input** | [**MqttRequest**](MqttRequest.md)| input |

### Return type

[**MqttResponse**](MqttResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/x-protobuf
 - **Accept**: application/json, application/x-protobuf

<a name="findAllSensorTagsByTypeUsingGET"></a>
# **findAllSensorTagsByTypeUsingGET**
> SensorTag findAllSensorTagsByTypeUsingGET(type, tenantId)

find all tags for a tenant by type

Find All Tags for a Tenant by Type

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String type = "type_example"; // String | Tag Type
String tenantId = "tenantId_example"; // String | Tenant Id
try {
    SensorTag result = apiInstance.findAllSensorTagsByTypeUsingGET(type, tenantId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#findAllSensorTagsByTypeUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**| Tag Type |
 **tenantId** | **String**| Tenant Id | [optional]

### Return type

[**SensorTag**](SensorTag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findAllSensorTagsUsingGET"></a>
# **findAllSensorTagsUsingGET**
> SensorTag findAllSensorTagsUsingGET(status, tenantId)

find all tags for a tenant by status

Find All Tags for a Tenant by Status

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String status = "status_example"; // String | Tag Status
String tenantId = "tenantId_example"; // String | Tenant Id
try {
    SensorTag result = apiInstance.findAllSensorTagsUsingGET(status, tenantId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#findAllSensorTagsUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **String**| Tag Status | [enum: READY, ACTIVE, INACTIVE, FAILED]
 **tenantId** | **String**| Tenant Id | [optional]

### Return type

[**SensorTag**](SensorTag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findAllSensorTagsUsingGET1"></a>
# **findAllSensorTagsUsingGET1**
> SensorTag findAllSensorTagsUsingGET1(tenantId)

find all tags for a tenant

Find All Tags for a Tenant

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String tenantId = "tenantId_example"; // String | Tenant Id
try {
    SensorTag result = apiInstance.findAllSensorTagsUsingGET1(tenantId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#findAllSensorTagsUsingGET1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenantId** | **String**| Tenant Id | [optional]

### Return type

[**SensorTag**](SensorTag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findAllTaggedAssetsExportUsingGET"></a>
# **findAllTaggedAssetsExportUsingGET**
> SensorTag findAllTaggedAssetsExportUsingGET(tenantId, responseFormat)

Export Assets Snapshot

Get CSV of Asset snapshot about current area and entry times

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String tenantId = "tenantId_example"; // String | Tenant Id
String responseFormat = "JSON"; // String | JSON/CSV
try {
    SensorTag result = apiInstance.findAllTaggedAssetsExportUsingGET(tenantId, responseFormat);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#findAllTaggedAssetsExportUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenantId** | **String**| Tenant Id |
 **responseFormat** | **String**| JSON/CSV | [default to JSON]

### Return type

[**SensorTag**](SensorTag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findAllTaggedAssetsUsingGET"></a>
# **findAllTaggedAssetsUsingGET**
> SensorTag findAllTaggedAssetsUsingGET(tenantId)

find all tagged assets for a tenant

Find All monitored Assets for a Tenant

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String tenantId = "tenantId_example"; // String | Tenant Id
try {
    SensorTag result = apiInstance.findAllTaggedAssetsUsingGET(tenantId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#findAllTaggedAssetsUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenantId** | **String**| Tenant Id | [optional]

### Return type

[**SensorTag**](SensorTag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findTaggedAssetsByAssetTypeUsingGET"></a>
# **findTaggedAssetsByAssetTypeUsingGET**
> TaggedAsset findTaggedAssetsByAssetTypeUsingGET(type, tenantId)

find all tagged assets of a certain asset type for a tenant

Find All Assets for a Tenant

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String type = "type_example"; // String | Asset Type
String tenantId = "tenantId_example"; // String | Tenant Id
try {
    TaggedAsset result = apiInstance.findTaggedAssetsByAssetTypeUsingGET(type, tenantId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#findTaggedAssetsByAssetTypeUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**| Asset Type |
 **tenantId** | **String**| Tenant Id | [optional]

### Return type

[**TaggedAsset**](TaggedAsset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="geTaggedAssetAssignmentUsingGET"></a>
# **geTaggedAssetAssignmentUsingGET**
> TaggedAsset geTaggedAssetAssignmentUsingGET(id)

find tagged asset assignment

Find tagged asset assignment by tag id

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | Sensor Tag Id
try {
    TaggedAsset result = apiInstance.geTaggedAssetAssignmentUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#geTaggedAssetAssignmentUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Sensor Tag Id |

### Return type

[**TaggedAsset**](TaggedAsset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAssetDataForAreaUsingGET3"></a>
# **getAssetDataForAreaUsingGET3**
> TaggedAsset getAssetDataForAreaUsingGET3(catId, areaId)

find all tagged assets for a given area and category

Find all tagged assets for a given area and category

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String catId = "catId_example"; // String | catId
String areaId = "areaId_example"; // String | areaId
try {
    TaggedAsset result = apiInstance.getAssetDataForAreaUsingGET3(catId, areaId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#getAssetDataForAreaUsingGET3");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catId** | **String**| catId |
 **areaId** | **String**| areaId |

### Return type

[**TaggedAsset**](TaggedAsset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getHistoryByAssetUsingGET"></a>
# **getHistoryByAssetUsingGET**
> List&lt;TaggedAsset&gt; getHistoryByAssetUsingGET(id)

find tagged asset history by asset

find tagged asset history by asset

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | Asset Id
try {
    List<TaggedAsset> result = apiInstance.getHistoryByAssetUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#getHistoryByAssetUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Asset Id |

### Return type

[**List&lt;TaggedAsset&gt;**](TaggedAsset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSensorTagDetailsUsingGET"></a>
# **getSensorTagDetailsUsingGET**
> SensorTagDetail getSensorTagDetailsUsingGET(id, minimal)

get sensortag detais 

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | Tag Id
Boolean minimal = true; // Boolean | minimal
try {
    SensorTagDetail result = apiInstance.getSensorTagDetailsUsingGET(id, minimal);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#getSensorTagDetailsUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Tag Id |
 **minimal** | **Boolean**| minimal | [optional] [default to true]

### Return type

[**SensorTagDetail**](SensorTagDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSensorTagUsingGET"></a>
# **getSensorTagUsingGET**
> SensorTag getSensorTagUsingGET(id, suppressError)

find tag by ID

Find a tag by id

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | Tag Id
Boolean suppressError = false; // Boolean | Suppress Error
try {
    SensorTag result = apiInstance.getSensorTagUsingGET(id, suppressError);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#getSensorTagUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Tag Id |
 **suppressError** | **Boolean**| Suppress Error | [optional] [default to false]

### Return type

[**SensorTag**](SensorTag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getTaggedAssetByAssetUsingGET"></a>
# **getTaggedAssetByAssetUsingGET**
> TaggedAsset getTaggedAssetByAssetUsingGET(id)

find tagged asset by Asset ID

Find a tagged asset by asset id

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | Asset Id
try {
    TaggedAsset result = apiInstance.getTaggedAssetByAssetUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#getTaggedAssetByAssetUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Asset Id |

### Return type

[**TaggedAsset**](TaggedAsset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getTaggedAssetBySensorTagUsingGET"></a>
# **getTaggedAssetBySensorTagUsingGET**
> TaggedAsset getTaggedAssetBySensorTagUsingGET(id)

find tagged asset by SensorTag ID

Find a tagged asset by sensor tag id

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | Sensor Tag Id
try {
    TaggedAsset result = apiInstance.getTaggedAssetBySensorTagUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#getTaggedAssetBySensorTagUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Sensor Tag Id |

### Return type

[**TaggedAsset**](TaggedAsset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getTaggedAssetUsingGET"></a>
# **getTaggedAssetUsingGET**
> TaggedAsset getTaggedAssetUsingGET(id)

find tagged asset by ID

Find a tagged asset by id.  Returns Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | Tagged Asset Id
try {
    TaggedAsset result = apiInstance.getTaggedAssetUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#getTaggedAssetUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Tagged Asset Id |

### Return type

[**TaggedAsset**](TaggedAsset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="provisionTaggedAssetUsingPOST"></a>
# **provisionTaggedAssetUsingPOST**
> TaggedAsset provisionTaggedAssetUsingPOST(taggedAsset)

provision sensor tag

Create tag to asset provisioning

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
TaggedAsset taggedAsset = new TaggedAsset(); // TaggedAsset | Sensor tag and Asset data to bind
try {
    TaggedAsset result = apiInstance.provisionTaggedAssetUsingPOST(taggedAsset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#provisionTaggedAssetUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taggedAsset** | [**TaggedAsset**](TaggedAsset.md)| Sensor tag and Asset data to bind |

### Return type

[**TaggedAsset**](TaggedAsset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateSensorTagUsingPUT"></a>
# **updateSensorTagUsingPUT**
> String updateSensorTagUsingPUT(id, sensorTag)

save/update tag

Save/Update tag with given details. Return Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | User Id
SensorTag sensorTag = new SensorTag(); // SensorTag | User info to store or update
try {
    String result = apiInstance.updateSensorTagUsingPUT(id, sensorTag);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#updateSensorTagUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| User Id |
 **sensorTag** | [**SensorTag**](SensorTag.md)| User info to store or update |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updateSensorTagUsingPUT1"></a>
# **updateSensorTagUsingPUT1**
> String updateSensorTagUsingPUT1(id, taggedAsset)

save/update TaggedAsset

Save/Update tagged asset with given details. Returns Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
String id = "id_example"; // String | User Id
TaggedAsset taggedAsset = new TaggedAsset(); // TaggedAsset | Asset info to store or update
try {
    String result = apiInstance.updateSensorTagUsingPUT1(id, taggedAsset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#updateSensorTagUsingPUT1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| User Id |
 **taggedAsset** | [**TaggedAsset**](TaggedAsset.md)| Asset info to store or update |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updateTagStaticMetaInBulkUsingPOST"></a>
# **updateTagStaticMetaInBulkUsingPOST**
> List&lt;CsvDataStatus&gt; updateTagStaticMetaInBulkUsingPOST(file)

create tag static meta in bulk

Return Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;


SensorTagControllerApi apiInstance = new SensorTagControllerApi();
File file = new File("/path/to/file.txt"); // File | file
try {
    List<CsvDataStatus> result = apiInstance.updateTagStaticMetaInBulkUsingPOST(file);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SensorTagControllerApi#updateTagStaticMetaInBulkUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **File**| file |

### Return type

[**List&lt;CsvDataStatus&gt;**](CsvDataStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: */*

