
# ObjectType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **String** | identifier | 
**type** | **String** | type |  [optional]
**name** | **String** | name |  [optional]



