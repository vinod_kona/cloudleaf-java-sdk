
# StateResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetState** | **String** |  |  [optional]
**regionId** | **String** |  |  [optional]
**regionName** | **String** |  |  [optional]
**stateDwellTime** | **Long** |  |  [optional]
**stateEntry** | [**Date**](Date.md) |  |  [optional]
**stateExit** | [**Date**](Date.md) |  |  [optional]
**streamId** | **String** |  |  [optional]



