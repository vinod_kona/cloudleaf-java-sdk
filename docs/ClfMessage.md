
# ClfMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**altBody** | **String** |  |  [optional]
**body** | **String** |  |  [optional]
**subject** | **String** |  |  [optional]
**transport** | [**TransportEnum**](#TransportEnum) |  |  [optional]


<a name="TransportEnum"></a>
## Enum: TransportEnum
Name | Value
---- | -----
WEB | &quot;web&quot;
EMAIL | &quot;email&quot;
APPLICATION | &quot;application&quot;
PUBNUB | &quot;pubnub&quot;
SMS | &quot;sms&quot;
VOICE | &quot;voice&quot;



