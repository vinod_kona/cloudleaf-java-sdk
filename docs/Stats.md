
# Stats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;StatsValue&gt;**](StatsValue.md) | array of data values and the time the data was recorded |  [optional]



