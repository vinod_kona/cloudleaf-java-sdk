
# NotificationSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryRegEx** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**device** | [**UserDevice**](UserDevice.md) |  |  [optional]
**levelRegEx** | **String** |  |  [optional]
**modifiedAt** | [**Date**](Date.md) |  |  [optional]
**notifTypeRegEx** | **String** |  |  [optional]
**notificationStatus** | **String** |  |  [optional]
**regExString** | **String** |  |  [optional]
**tenantId** | **String** |  |  [optional]
**userId** | **String** |  |  [optional]



