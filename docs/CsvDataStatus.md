
# CsvDataStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **String** |  |  [optional]
**errorList** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**status** | **String** |  |  [optional]



