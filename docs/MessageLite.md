
# MessageLite

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**defaultInstanceForType** | [**MessageLite**](MessageLite.md) |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**parserForType** | [**ParserMessageLite**](ParserMessageLite.md) |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]



