
# Descriptor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**containingType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**enumTypes** | [**List&lt;EnumDescriptor&gt;**](EnumDescriptor.md) |  |  [optional]
**extensions** | [**List&lt;FieldDescriptor&gt;**](FieldDescriptor.md) |  |  [optional]
**fields** | [**List&lt;FieldDescriptor&gt;**](FieldDescriptor.md) |  |  [optional]
**file** | [**FileDescriptor**](FileDescriptor.md) |  |  [optional]
**fullName** | **String** |  |  [optional]
**index** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**nestedTypes** | [**List&lt;Descriptor&gt;**](Descriptor.md) |  |  [optional]
**options** | [**MessageOptions**](MessageOptions.md) |  |  [optional]



