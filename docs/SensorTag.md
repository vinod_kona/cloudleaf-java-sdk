
# SensorTag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**Date**](Date.md) |  |  [optional]
**id** | **String** |  |  [optional]
**modifiedAt** | [**Date**](Date.md) |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**tenantId** | **String** |  |  [optional]
**type** | **String** |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
READY | &quot;READY&quot;
ACTIVE | &quot;ACTIVE&quot;
INACTIVE | &quot;INACTIVE&quot;
FAILED | &quot;FAILED&quot;



