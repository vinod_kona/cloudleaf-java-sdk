
# PeripheralTypeMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**Date**](Date.md) |  |  [optional]
**deviceClass** | **String** |  |  [optional]
**enabled** | **Boolean** |  |  [optional]
**idCommand** | [**IdCommand**](IdCommand.md) |  |  [optional]
**modifiedAt** | [**Date**](Date.md) |  |  [optional]
**regEx** | **String** |  |  [optional]



