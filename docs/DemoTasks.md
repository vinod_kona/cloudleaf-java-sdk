
# DemoTasks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**peripheralId** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
ON | &quot;ON&quot;
OFF | &quot;OFF&quot;



