
# SensorTagDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sensorTag** | [**SensorTag**](SensorTag.md) |  |  [optional]
**taggedAsst** | [**TaggedAsset**](TaggedAsset.md) |  |  [optional]



