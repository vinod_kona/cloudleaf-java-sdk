
# Zone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**regions** | [**List&lt;Space&gt;**](Space.md) |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type |  [optional]
**name** | **String** | name |  [optional]



