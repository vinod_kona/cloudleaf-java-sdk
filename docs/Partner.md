
# Partner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customers** | [**List&lt;Customer&gt;**](Customer.md) |  |  [optional]
**peripheralTypes** | **List&lt;String&gt;** |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type | 
**name** | **String** | name |  [optional]



