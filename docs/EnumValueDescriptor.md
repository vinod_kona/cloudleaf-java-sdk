
# EnumValueDescriptor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**FileDescriptor**](FileDescriptor.md) |  |  [optional]
**fullName** | **String** |  |  [optional]
**index** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**number** | **Integer** |  |  [optional]
**options** | [**EnumValueOptions**](EnumValueOptions.md) |  |  [optional]
**type** | [**EnumDescriptor**](EnumDescriptor.md) |  |  [optional]



