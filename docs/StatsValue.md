
# StatsValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Long** |  |  [optional]
**max** | **Double** |  |  [optional]
**mean** | **Double** |  |  [optional]
**min** | **Double** |  |  [optional]
**stdev** | **Double** |  |  [optional]
**time** | [**Date**](Date.md) |  |  [optional]



