
# EnumDescriptor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**containingType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**file** | [**FileDescriptor**](FileDescriptor.md) |  |  [optional]
**fullName** | **String** |  |  [optional]
**index** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**options** | [**EnumOptions**](EnumOptions.md) |  |  [optional]
**values** | [**List&lt;EnumValueDescriptor&gt;**](EnumValueDescriptor.md) |  |  [optional]



