
# EventDataResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**level** | **String** |  |  [optional]
**module** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**time** | [**Date**](Date.md) |  |  [optional]
**type** | **String** |  |  [optional]
**value** | **String** |  |  [optional]



