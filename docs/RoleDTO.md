
# RoleDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**editable** | **Boolean** |  |  [optional]
**id** | **String** |  |  [optional]
**permissions** | **List&lt;String&gt;** |  |  [optional]
**tenantId** | **String** |  |  [optional]



