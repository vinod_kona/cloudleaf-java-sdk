# UserControllerApi

All URIs are relative to *https://dev.cloudleaf.io/cloudos*

Method | HTTP request | Description
------------- | ------------- | -------------
[**loginUsingPOST**](UserControllerApi.md#loginUsingPOST) | **POST** /api/1/user/login | login


<a name="loginUsingPOST"></a>
# **loginUsingPOST**
> AccessToken loginUsingPOST(creds)

login

Login a user/app via REST API call with credentials. All API calls have to be authenticated using a *login and secret* as credentials. This call will return a valid  **token** that need be sent as header parameter at the time of REST API invocation

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.UserControllerApi;


UserControllerApi apiInstance = new UserControllerApi();
UserCredentials creds = new UserCredentials(); // UserCredentials | Credential object
try {
    AccessToken result = apiInstance.loginUsingPOST(creds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserControllerApi#loginUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **creds** | [**UserCredentials**](UserCredentials.md)| Credential object |

### Return type

[**AccessToken**](AccessToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

