# DeviceCommandControllerApi

All URIs are relative to *https://dev.cloudleaf.io/cloudos*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCommandsByDeviceIdUsingDELETE**](DeviceCommandControllerApi.md#deleteCommandsByDeviceIdUsingDELETE) | **DELETE** /api/1/kdcp/{deviceId}/{issuedfrom} | Delete commands by device Id 
[**executeTagActionUsingPOST**](DeviceCommandControllerApi.md#executeTagActionUsingPOST) | **POST** /api/1/kdcp/{deviceId}/cmd | executeTagAction
[**getCmdBySeqNoUsingGET**](DeviceCommandControllerApi.md#getCmdBySeqNoUsingGET) | **GET** /api/1/kdcp/{userId}/{seqNo} | Get Commands issued by a source 
[**getCmdsByFromUsingGET**](DeviceCommandControllerApi.md#getCmdsByFromUsingGET) | **GET** /api/1/kdcp/{userId} | Get Commands issued by a source 
[**getCmdsHistoryByCmdUsingGET**](DeviceCommandControllerApi.md#getCmdsHistoryByCmdUsingGET) | **GET** /api/1/kdcp/{deviceId}/{cmd}/history | Get Commands History by Device Id and cmd 
[**getCmdsHistoryByDeviceIdUsingGET**](DeviceCommandControllerApi.md#getCmdsHistoryByDeviceIdUsingGET) | **GET** /api/1/kdcp/{deviceId}/history | Get Commands History by Device Id 
[**getCmdsHistoryByFromUsingGET**](DeviceCommandControllerApi.md#getCmdsHistoryByFromUsingGET) | **GET** /api/1/kdcp/{deviceId}/{cmd}/{userId}/history | Get Commands History by Device Id , Issued from and cmd 
[**getMostRecentCmdByCmdUsingGET**](DeviceCommandControllerApi.md#getMostRecentCmdByCmdUsingGET) | **GET** /api/1/kdcp/{deviceId}/{cmd}/recent | Get Most Recent Commands by Device Id and Command
[**getMostRecentCmdsByDeviceIdUsingGET**](DeviceCommandControllerApi.md#getMostRecentCmdsByDeviceIdUsingGET) | **GET** /api/1/kdcp/{deviceId}/recent | Get Most Recent Commands by Device Id


<a name="deleteCommandsByDeviceIdUsingDELETE"></a>
# **deleteCommandsByDeviceIdUsingDELETE**
> ResponseEntity deleteCommandsByDeviceIdUsingDELETE(deviceId, issuedfrom, fromTime)

Delete commands by device Id 

Delete commands by device Id 

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;


DeviceCommandControllerApi apiInstance = new DeviceCommandControllerApi();
String deviceId = "deviceId_example"; // String | deviceId
String issuedfrom = "issuedfrom_example"; // String | issuedfrom
Long fromTime = 789L; // Long | from specified time range in UTC millis
try {
    ResponseEntity result = apiInstance.deleteCommandsByDeviceIdUsingDELETE(deviceId, issuedfrom, fromTime);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceCommandControllerApi#deleteCommandsByDeviceIdUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String**| deviceId |
 **issuedfrom** | **String**| issuedfrom |
 **fromTime** | **Long**| from specified time range in UTC millis | [optional]

### Return type

[**ResponseEntity**](ResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executeTagActionUsingPOST"></a>
# **executeTagActionUsingPOST**
> Object executeTagActionUsingPOST(deviceId, commandSpecifier)

executeTagAction

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;


DeviceCommandControllerApi apiInstance = new DeviceCommandControllerApi();
String deviceId = "deviceId_example"; // String | deviceId
CmdSpecifierRequest commandSpecifier = new CmdSpecifierRequest(); // CmdSpecifierRequest | commandSpecifier
try {
    Object result = apiInstance.executeTagActionUsingPOST(deviceId, commandSpecifier);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceCommandControllerApi#executeTagActionUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String**| deviceId |
 **commandSpecifier** | [**CmdSpecifierRequest**](CmdSpecifierRequest.md)| commandSpecifier |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCmdBySeqNoUsingGET"></a>
# **getCmdBySeqNoUsingGET**
> CommandSpecifier getCmdBySeqNoUsingGET(userId, seqNo)

Get Commands issued by a source 

Get Commands issued by a source 

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;


DeviceCommandControllerApi apiInstance = new DeviceCommandControllerApi();
String userId = "userId_example"; // String | userId
Long seqNo = 789L; // Long | Sequence Number
try {
    CommandSpecifier result = apiInstance.getCmdBySeqNoUsingGET(userId, seqNo);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceCommandControllerApi#getCmdBySeqNoUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**| userId |
 **seqNo** | **Long**| Sequence Number |

### Return type

[**CommandSpecifier**](CommandSpecifier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCmdsByFromUsingGET"></a>
# **getCmdsByFromUsingGET**
> List&lt;CommandSpecifier&gt; getCmdsByFromUsingGET(userId)

Get Commands issued by a source 

Get Commands issued by a source 

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;


DeviceCommandControllerApi apiInstance = new DeviceCommandControllerApi();
String userId = "userId_example"; // String | userId
try {
    List<CommandSpecifier> result = apiInstance.getCmdsByFromUsingGET(userId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceCommandControllerApi#getCmdsByFromUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**| userId |

### Return type

[**List&lt;CommandSpecifier&gt;**](CommandSpecifier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCmdsHistoryByCmdUsingGET"></a>
# **getCmdsHistoryByCmdUsingGET**
> List&lt;CommandSpecifier&gt; getCmdsHistoryByCmdUsingGET(deviceId, cmd, fromTime, toTime, limit)

Get Commands History by Device Id and cmd 

Get Commands History by Device Id and cmd

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;


DeviceCommandControllerApi apiInstance = new DeviceCommandControllerApi();
String deviceId = "deviceId_example"; // String | Device Id
String cmd = "cmd_example"; // String | Command
Long fromTime = 789L; // Long | from specified time range in UTC millis
Long toTime = 789L; // Long | to specified time range in UTC millis
Integer limit = 300; // Integer | limit data values
try {
    List<CommandSpecifier> result = apiInstance.getCmdsHistoryByCmdUsingGET(deviceId, cmd, fromTime, toTime, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceCommandControllerApi#getCmdsHistoryByCmdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String**| Device Id |
 **cmd** | **String**| Command |
 **fromTime** | **Long**| from specified time range in UTC millis | [optional]
 **toTime** | **Long**| to specified time range in UTC millis | [optional]
 **limit** | **Integer**| limit data values | [optional] [default to 300]

### Return type

[**List&lt;CommandSpecifier&gt;**](CommandSpecifier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCmdsHistoryByDeviceIdUsingGET"></a>
# **getCmdsHistoryByDeviceIdUsingGET**
> List&lt;CommandSpecifier&gt; getCmdsHistoryByDeviceIdUsingGET(deviceId, fromTime, toTime, limit)

Get Commands History by Device Id 

Get Commands History by Device Id

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;


DeviceCommandControllerApi apiInstance = new DeviceCommandControllerApi();
String deviceId = "deviceId_example"; // String | Device Id
Long fromTime = 789L; // Long | from specified time range in UTC millis
Long toTime = 789L; // Long | to specified time range in UTC millis
Integer limit = 300; // Integer | limit data values
try {
    List<CommandSpecifier> result = apiInstance.getCmdsHistoryByDeviceIdUsingGET(deviceId, fromTime, toTime, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceCommandControllerApi#getCmdsHistoryByDeviceIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String**| Device Id |
 **fromTime** | **Long**| from specified time range in UTC millis | [optional]
 **toTime** | **Long**| to specified time range in UTC millis | [optional]
 **limit** | **Integer**| limit data values | [optional] [default to 300]

### Return type

[**List&lt;CommandSpecifier&gt;**](CommandSpecifier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCmdsHistoryByFromUsingGET"></a>
# **getCmdsHistoryByFromUsingGET**
> List&lt;CommandSpecifier&gt; getCmdsHistoryByFromUsingGET(deviceId, cmd, userId, fromTime, toTime, limit)

Get Commands History by Device Id , Issued from and cmd 

Get Commands History by Device Id, Issued from and cmd

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;


DeviceCommandControllerApi apiInstance = new DeviceCommandControllerApi();
String deviceId = "deviceId_example"; // String | Device Id
String cmd = "cmd_example"; // String | Command
String userId = "userId_example"; // String | userId
Long fromTime = 789L; // Long | from specified time range in UTC millis
Long toTime = 789L; // Long | to specified time range in UTC millis
Integer limit = 300; // Integer | limit data values
try {
    List<CommandSpecifier> result = apiInstance.getCmdsHistoryByFromUsingGET(deviceId, cmd, userId, fromTime, toTime, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceCommandControllerApi#getCmdsHistoryByFromUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String**| Device Id |
 **cmd** | **String**| Command |
 **userId** | **String**| userId |
 **fromTime** | **Long**| from specified time range in UTC millis | [optional]
 **toTime** | **Long**| to specified time range in UTC millis | [optional]
 **limit** | **Integer**| limit data values | [optional] [default to 300]

### Return type

[**List&lt;CommandSpecifier&gt;**](CommandSpecifier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMostRecentCmdByCmdUsingGET"></a>
# **getMostRecentCmdByCmdUsingGET**
> CommandSpecifier getMostRecentCmdByCmdUsingGET(deviceId, cmd)

Get Most Recent Commands by Device Id and Command

Get Most Recent Commands by Device Id and Command

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;


DeviceCommandControllerApi apiInstance = new DeviceCommandControllerApi();
String deviceId = "deviceId_example"; // String | Device Id
String cmd = "cmd_example"; // String | Command
try {
    CommandSpecifier result = apiInstance.getMostRecentCmdByCmdUsingGET(deviceId, cmd);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceCommandControllerApi#getMostRecentCmdByCmdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String**| Device Id |
 **cmd** | **String**| Command |

### Return type

[**CommandSpecifier**](CommandSpecifier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMostRecentCmdsByDeviceIdUsingGET"></a>
# **getMostRecentCmdsByDeviceIdUsingGET**
> List&lt;CommandSpecifier&gt; getMostRecentCmdsByDeviceIdUsingGET(deviceId)

Get Most Recent Commands by Device Id

Get Most Recent Commands by Device Id

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;


DeviceCommandControllerApi apiInstance = new DeviceCommandControllerApi();
String deviceId = "deviceId_example"; // String | Device Id
try {
    List<CommandSpecifier> result = apiInstance.getMostRecentCmdsByDeviceIdUsingGET(deviceId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceCommandControllerApi#getMostRecentCmdsByDeviceIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String**| Device Id |

### Return type

[**List&lt;CommandSpecifier&gt;**](CommandSpecifier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

