
# Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cId** | **String** | characteristic Id | 
**cName** | **String** | human readable characteristic name |  [optional]
**data** | [**List&lt;StreamResultValue&gt;**](StreamResultValue.md) | array of data values and the time the data was recorded |  [optional]
**format** | **String** | format type - CloudLeaf known system types are: &#39;STRING_TYPE&#39;, &#39;INT_TYPE&#39;, &#39;FLOAT_TYPE&#39;, &#39;DOUBLE_TYPE&#39;. Custom types can be defined. &#39;UNKNOWN_TYPE&#39; if format is not known | 
**oId** | **String** | object Id | 
**oName** | **String** | object Name |  [optional]
**oType** | [**OTypeEnum**](#OTypeEnum) | object Type | 
**sId** | **String** | service Id | 
**sName** | **String** | human readable service name |  [optional]
**sourceData** | [**List&lt;ResultValue&gt;**](ResultValue.md) | array of data values for a source and the time the data was recorded |  [optional]
**unit** | **String** | unit type - &#39;UNKNOWN_TYPE&#39; if type information is not known | 


<a name="OTypeEnum"></a>
## Enum: OTypeEnum
Name | Value
---- | -----
PERIPHERAL | &quot;PERIPHERAL&quot;
RECEIVER | &quot;RECEIVER&quot;
ORGANIZATION | &quot;ORGANIZATION&quot;
SITE | &quot;SITE&quot;
AREA | &quot;AREA&quot;
USER | &quot;USER&quot;



