
# IoctlOrBuilder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actionType** | [**ActionTypeEnum**](#ActionTypeEnum) |  |  [optional]
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**cid** | **String** |  |  [optional]
**cidBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**defaultInstanceForType** | [**Message**](Message.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**optionsCount** | **Integer** |  |  [optional]
**optionsList** | [**List&lt;Option&gt;**](Option.md) |  |  [optional]
**optionsOrBuilderList** | [**List&lt;OptionOrBuilder&gt;**](OptionOrBuilder.md) |  |  [optional]
**sid** | **String** |  |  [optional]
**sidBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]
**value** | [**ByteString**](ByteString.md) |  |  [optional]


<a name="ActionTypeEnum"></a>
## Enum: ActionTypeEnum
Name | Value
---- | -----
BROADCAST | &quot;BROADCAST&quot;
READ | &quot;READ&quot;
WRITE_WITHOUT_RESPONSE | &quot;WRITE_WITHOUT_RESPONSE&quot;
WRITE | &quot;WRITE&quot;
NOTIFY | &quot;NOTIFY&quot;
INDICATE | &quot;INDICATE&quot;
SIGNED_WRITE | &quot;SIGNED_WRITE&quot;
EXTENDED_PROPERTIES | &quot;EXTENDED_PROPERTIES&quot;
NOTIFY_ENCRYPTION_REQUIRED | &quot;NOTIFY_ENCRYPTION_REQUIRED&quot;
INDICATE_ENCRYPTION_REQUIRED | &quot;INDICATE_ENCRYPTION_REQUIRED&quot;
WRITABLE_AUXILIARIES | &quot;WRITABLE_AUXILIARIES&quot;



