
# Asset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  |  [optional]
**categoryName** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**description** | **String** |  |  [optional]
**externalId** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**imageURL** | **String** |  |  [optional]
**modifiedAt** | [**Date**](Date.md) |  |  [optional]
**name** | **String** |  |  [optional]
**payload** | **List&lt;String&gt;** |  |  [optional]
**payloadOf** | **String** |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**sortOrder** | **Integer** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**tenantId** | **String** |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNMONITORED | &quot;UNMONITORED&quot;
MONITORED | &quot;MONITORED&quot;
ARCHIVED | &quot;ARCHIVED&quot;



