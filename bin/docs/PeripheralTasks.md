
# PeripheralTasks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**peripheralId** | **String** |  |  [optional]
**tasks** | [**Map&lt;String, List&lt;Command&gt;&gt;**](List.md) |  |  [optional]
**type** | **String** |  |  [optional]



