
# NotificationTopic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**notificationType** | **String** |  |  [optional]
**tenantId** | **String** |  |  [optional]



