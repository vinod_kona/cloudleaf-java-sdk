
# Category

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetStateValues** | **List&lt;String&gt;** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**description** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**imageURL** | **String** |  |  [optional]
**metrics** | **List&lt;String&gt;** |  |  [optional]
**modifiedAt** | [**Date**](Date.md) |  |  [optional]
**name** | **String** |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**sortOrder** | **Integer** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**tenantId** | **String** |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
ACTIVE | &quot;ACTIVE&quot;
ARCHIVED | &quot;ARCHIVED&quot;
DELETED | &quot;DELETED&quot;



