
# AssetVisitResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**areaLocation** | **String** |  |  [optional]
**assetExternalId** | **String** |  |  [optional]
**assetName** | **String** |  |  [optional]
**assetType** | **String** |  |  [optional]
**dwellTime** | **String** |  |  [optional]
**entry** | **String** |  |  [optional]
**exit** | **String** |  |  [optional]
**site** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**taggedAssetId** | **String** |  |  [optional]



