
# CharacteristicMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configParams** | **Map&lt;String, String&gt;** |  |  [optional]
**converter** | **String** |  |  [optional]
**converterParams** | **Map&lt;String, String&gt;** |  |  [optional]
**descriptors** | [**List&lt;DescriptorMeta&gt;**](DescriptorMeta.md) | Descriptor list |  [optional]
**properties** | [**List&lt;PropertiesEnum&gt;**](#List&lt;PropertiesEnum&gt;) | Actions allowed on this characteristic |  [optional]
**unitType** | **String** | Unit type of the characteristic |  [optional]
**webUIConfig** | **Map&lt;String, String&gt;** |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type |  [optional]
**name** | **String** | name |  [optional]


<a name="List<PropertiesEnum>"></a>
## Enum: List&lt;PropertiesEnum&gt;
Name | Value
---- | -----
BROADCAST | &quot;Broadcast&quot;
READ | &quot;Read&quot;
WRITEWITHOUTRESPONSE | &quot;WriteWithoutResponse&quot;
WRITE | &quot;Write&quot;
NOTIFY | &quot;Notify&quot;
INDICATE | &quot;Indicate&quot;
SIGNEDWRITE | &quot;SignedWrite&quot;
EXTENDEDPROPERTIES | &quot;ExtendedProperties&quot;
NOTIFYENCRYPTIONREQUIRED | &quot;NotifyEncryptionRequired&quot;
INDICATEENCRYPTIONREQUIRED | &quot;IndicateEncryptionRequired&quot;
WRITABLEAUXILIARIES | &quot;WritableAuxiliaries&quot;



