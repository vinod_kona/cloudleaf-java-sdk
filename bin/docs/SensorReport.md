
# SensorReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetId** | **String** | Asset Id | 
**events** | [**List&lt;Event&gt;**](Event.md) | Events | 
**sensorId** | **String** | Sensor Id | 
**taggedAssetId** | **String** | TaggedAsset Id | 



