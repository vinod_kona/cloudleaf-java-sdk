
# ServiceMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**characteristics** | [**List&lt;CharacteristicMeta&gt;**](CharacteristicMeta.md) | Characteristics metadata list |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type |  [optional]
**name** | **String** | name |  [optional]



