
# MetricsCounters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**counts** | **Map&lt;String, String&gt;** |  |  [optional]
**name** | **String** |  |  [optional]
**tenant** | **String** |  |  [optional]
**tenantId** | **String** |  |  [optional]



