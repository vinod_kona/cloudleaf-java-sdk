
# CommandOrBuilder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**defaultInstanceForType** | [**Message**](Message.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**exec** | [**Ioctl**](Ioctl.md) |  |  [optional]
**execOrBuilder** | [**IoctlOrBuilder**](IoctlOrBuilder.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**setup** | [**Ioctl**](Ioctl.md) |  |  [optional]
**setupOrBuilder** | [**IoctlOrBuilder**](IoctlOrBuilder.md) |  |  [optional]
**teardown** | [**Ioctl**](Ioctl.md) |  |  [optional]
**teardownOrBuilder** | [**IoctlOrBuilder**](IoctlOrBuilder.md) |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]



