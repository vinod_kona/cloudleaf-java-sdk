
# Capability

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | [**ActionMeta**](ActionMeta.md) |  |  [optional]
**defaultOrder** | **Integer** |  |  [optional]
**description** | **String** |  |  [optional]
**displayable** | **Boolean** |  |  [optional]
**editable** | **Boolean** |  |  [optional]
**enabled** | **Boolean** |  |  [optional]
**name** | **String** |  |  [optional]
**state** | [**State**](State.md) |  |  [optional]



