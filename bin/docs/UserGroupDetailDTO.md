
# UserGroupDetailDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  |  [optional]
**tenantId** | **String** |  |  [optional]
**userGroupName** | **String** |  |  [optional]
**userId** | **String** |  |  [optional]



