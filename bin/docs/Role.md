
# Role

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**permissions** | **List&lt;String&gt;** |  |  [optional]
**tenantId** | **String** |  |  [optional]



