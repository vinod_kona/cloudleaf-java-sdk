
# Area

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bounds** | [**BoundingBox**](BoundingBox.md) |  |  [optional]
**deleted** | **Boolean** |  |  [optional]
**locus** | [**Point**](Point.md) |  |  [optional]
**order** | **Integer** |  |  [optional]
**receiver** | [**Receiver**](Receiver.md) |  |  [optional]
**tags** | **List&lt;String&gt;** |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type | 
**name** | **String** | name |  [optional]



