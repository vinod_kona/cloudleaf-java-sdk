
# SensorInventoryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetExternalId** | **String** |  |  [optional]
**assetId** | **String** |  |  [optional]
**batteryHealth** | **String** |  |  [optional]
**overallStatus** | **String** |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**sensorId** | **String** |  |  [optional]
**sensorType** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**taggedAssetId** | **String** |  |  [optional]



