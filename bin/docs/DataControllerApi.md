# DataControllerApi

All URIs are relative to *https://dev.cloudleaf.io/cloudos*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDataForCharacteristicUsingGET**](DataControllerApi.md#getDataForCharacteristicUsingGET) | **GET** /api/1/data/{id}/{sId}/{cId} | retrieve data stream range
[**getLastSnapshotUsingGET**](DataControllerApi.md#getLastSnapshotUsingGET) | **GET** /api/1/data/{id} | retrieve last data snapshot for peripheral
[**postListToStreamUsingPOST**](DataControllerApi.md#postListToStreamUsingPOST) | **POST** /api/1/data/stream/bulk | add bulk Event Data
[**postToStreamUsingPOST**](DataControllerApi.md#postToStreamUsingPOST) | **POST** /api/1/data/stream/{id} | add data for peripheral


<a name="getDataForCharacteristicUsingGET"></a>
# **getDataForCharacteristicUsingGET**
> Result getDataForCharacteristicUsingGET(id, sId, cId, to, from, limit)

retrieve data stream range

Gets the data stream in most recent time order for a given period of time for a characteristic for a given object.  The time period is defined using the &#39;from&#39; parameter and the &#39;to&#39; parameter.  If &#39;to&#39; is unspecified, the current time is used for the to time.  If &#39;from&#39; is unspecified, the from time is set to the start of the &#39;to&#39; day in UTC.  &#39;limit&#39; is used to restrict the set of data values returned, default 500.  If the limit is reached, the client will need to use this API again setting the &#39;to&#39; value to time of the last retrieve data value

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DataControllerApi;


DataControllerApi apiInstance = new DataControllerApi();
String id = "id_example"; // String | Stream Id
String sId = "sId_example"; // String | service identifier
String cId = "cId_example"; // String | characteristic identifier
Long to = 789L; // Long | to specified time range in UTC millis
Long from = 789L; // Long | from specified time range in UTC millis
Integer limit = 300; // Integer | limit data values
try {
    Result result = apiInstance.getDataForCharacteristicUsingGET(id, sId, cId, to, from, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DataControllerApi#getDataForCharacteristicUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Stream Id |
 **sId** | **String**| service identifier |
 **cId** | **String**| characteristic identifier |
 **to** | **Long**| to specified time range in UTC millis | [optional]
 **from** | **Long**| from specified time range in UTC millis | [optional]
 **limit** | **Integer**| limit data values | [optional] [default to 300]

### Return type

[**Result**](Result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getLastSnapshotUsingGET"></a>
# **getLastSnapshotUsingGET**
> String getLastSnapshotUsingGET(id)

retrieve last data snapshot for peripheral

Gets the last data value for every tracked service for a given peripheral

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DataControllerApi;


DataControllerApi apiInstance = new DataControllerApi();
String id = "id_example"; // String | id
try {
    String result = apiInstance.getLastSnapshotUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DataControllerApi#getLastSnapshotUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postListToStreamUsingPOST"></a>
# **postListToStreamUsingPOST**
> String postListToStreamUsingPOST(data)

add bulk Event Data

Uploads bulk events data to the peripheral stream

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DataControllerApi;


DataControllerApi apiInstance = new DataControllerApi();
EventListData data = new EventListData(); // EventListData | Event list data
try {
    String result = apiInstance.postListToStreamUsingPOST(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DataControllerApi#postListToStreamUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**EventListData**](EventListData.md)| Event list data |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/x-protobuf
 - **Accept**: application/json

<a name="postToStreamUsingPOST"></a>
# **postToStreamUsingPOST**
> String postToStreamUsingPOST(id, data)

add data for peripheral

Uploads event data for a service and adds to the peripheral stream

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DataControllerApi;


DataControllerApi apiInstance = new DataControllerApi();
String id = "id_example"; // String | Peripheral Id
EventData data = new EventData(); // EventData | Event data
try {
    String result = apiInstance.postToStreamUsingPOST(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DataControllerApi#postToStreamUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Peripheral Id |
 **data** | [**EventData**](EventData.md)| Event data |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/x-protobuf
 - **Accept**: application/json

