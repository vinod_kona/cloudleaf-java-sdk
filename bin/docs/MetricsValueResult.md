
# MetricsValueResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metrics** | [**List&lt;MetricsValue&gt;**](MetricsValue.md) |  |  [optional]
**scope** | **String** |  |  [optional]
**type** | **String** |  |  [optional]



