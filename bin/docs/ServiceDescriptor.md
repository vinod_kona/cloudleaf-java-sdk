
# ServiceDescriptor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**FileDescriptor**](FileDescriptor.md) |  |  [optional]
**fullName** | **String** |  |  [optional]
**index** | **Integer** |  |  [optional]
**methods** | [**List&lt;MethodDescriptor&gt;**](MethodDescriptor.md) |  |  [optional]
**name** | **String** |  |  [optional]
**options** | [**ServiceOptions**](ServiceOptions.md) |  |  [optional]



