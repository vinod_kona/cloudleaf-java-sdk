
# MQTTConfigBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brokerurl** | **String** |  |  [optional]
**enabled** | **Boolean** |  |  [optional]
**externalBrokerurl** | **String** |  |  [optional]
**password** | **String** |  |  [optional]
**userid** | **String** |  |  [optional]



