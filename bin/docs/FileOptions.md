
# FileOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**ccGenericServices** | **Boolean** |  |  [optional]
**defaultInstanceForType** | [**FileOptions**](FileOptions.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**goPackage** | **String** |  |  [optional]
**goPackageBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**javaGenerateEqualsAndHash** | **Boolean** |  |  [optional]
**javaGenericServices** | **Boolean** |  |  [optional]
**javaMultipleFiles** | **Boolean** |  |  [optional]
**javaOuterClassname** | **String** |  |  [optional]
**javaOuterClassnameBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**javaPackage** | **String** |  |  [optional]
**javaPackageBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**optimizeFor** | [**OptimizeForEnum**](#OptimizeForEnum) |  |  [optional]
**parserForType** | [**ParserFileOptions**](ParserFileOptions.md) |  |  [optional]
**pyGenericServices** | **Boolean** |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]
**uninterpretedOptionCount** | **Integer** |  |  [optional]
**uninterpretedOptionList** | [**List&lt;UninterpretedOption&gt;**](UninterpretedOption.md) |  |  [optional]
**uninterpretedOptionOrBuilderList** | [**List&lt;UninterpretedOptionOrBuilder&gt;**](UninterpretedOptionOrBuilder.md) |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]


<a name="OptimizeForEnum"></a>
## Enum: OptimizeForEnum
Name | Value
---- | -----
SPEED | &quot;SPEED&quot;
CODE_SIZE | &quot;CODE_SIZE&quot;
LITE_RUNTIME | &quot;LITE_RUNTIME&quot;



