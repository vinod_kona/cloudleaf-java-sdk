
# Point

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**height** | **Double** |  |  [optional]
**lat** | **Double** |  |  [optional]
**lng** | **Double** |  |  [optional]



