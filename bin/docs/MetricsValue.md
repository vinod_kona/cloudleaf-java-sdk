
# MetricsValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | **String** |  |  [optional]
**interval** | **Long** |  |  [optional]
**metrics** | **Map&lt;String, String&gt;** |  |  [optional]
**name** | **String** |  |  [optional]
**scope** | **String** |  |  [optional]
**time** | [**Date**](Date.md) |  |  [optional]
**type** | **String** |  |  [optional]



