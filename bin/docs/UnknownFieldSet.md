
# UnknownFieldSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**defaultInstanceForType** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**parserForType** | [**Parser**](Parser.md) |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]
**serializedSizeAsMessageSet** | **Integer** |  |  [optional]



