
# Command

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | [**List&lt;Ioctl&gt;**](Ioctl.md) |  |  [optional]
**options** | **Map&lt;String, String&gt;** | Map of options for a specific Action, for example read frequency, or value to be written |  [optional]
**cId** | **String** | Characteristic to act on | 
**action** | [**ActionEnum**](#ActionEnum) | Action to perform, for Id set to &#39;Read&#39; | 


<a name="ActionEnum"></a>
## Enum: ActionEnum
Name | Value
---- | -----
READ | &quot;Read&quot;
NOTIFY | &quot;Notify&quot;
WRITE | &quot;Write&quot;
WRITEWITHOUTRESPONSE | &quot;WriteWithoutResponse&quot;
INDICATE | &quot;Indicate&quot;



