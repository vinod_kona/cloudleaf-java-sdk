
# JobData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cassandraContactPt** | **String** |  |  [optional]
**dataSource** | **String** |  |  [optional]
**endTime** | **String** |  |  [optional]
**fileName** | **String** |  |  [optional]
**startTime** | **String** |  |  [optional]
**tenantId** | **String** |  |  [optional]



