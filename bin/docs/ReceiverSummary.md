
# ReceiverSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**health** | [**DeviceEventResult**](DeviceEventResult.md) |  |  [optional]
**heartBeat** | [**DeviceEventResult**](DeviceEventResult.md) |  |  [optional]
**receiverSummaryDetail** | [**ReceiverSummaryDetail**](ReceiverSummaryDetail.md) |  |  [optional]



