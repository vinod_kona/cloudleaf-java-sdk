
# Recipe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actionParams** | **Map&lt;String, String&gt;** |  |  [optional]
**actionTemplateId** | **String** |  |  [optional]
**active** | **Boolean** |  |  [optional]
**categoryId** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**id** | **String** |  |  [optional]
**modifiedAt** | [**Date**](Date.md) |  |  [optional]
**ruleIds** | **List&lt;String&gt;** |  |  [optional]
**scope** | [**ScopeEnum**](#ScopeEnum) |  |  [optional]
**tenantId** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**triggerParams** | **Map&lt;String, String&gt;** |  |  [optional]
**triggerTemplateId** | **String** |  |  [optional]
**userId** | **String** |  |  [optional]


<a name="ScopeEnum"></a>
## Enum: ScopeEnum
Name | Value
---- | -----
TENANT | &quot;TENANT&quot;
USER | &quot;USER&quot;



