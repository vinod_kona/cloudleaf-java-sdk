
# Customer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appRoles** | **List&lt;String&gt;** |  |  [optional]
**pTypes** | **List&lt;String&gt;** |  |  [optional]
**partners** | [**List&lt;Partner&gt;**](Partner.md) |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**zones** | [**List&lt;Zone&gt;**](Zone.md) |  |  [optional]
**identifier** | **String** | identifier | 
**type** | **String** | type | 
**name** | **String** | name |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
ACTIVE | &quot;ACTIVE&quot;
SUSPENDED | &quot;SUSPENDED&quot;



