
# FileDescriptor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dependencies** | [**List&lt;FileDescriptor&gt;**](FileDescriptor.md) |  |  [optional]
**enumTypes** | [**List&lt;EnumDescriptor&gt;**](EnumDescriptor.md) |  |  [optional]
**extensions** | [**List&lt;FieldDescriptor&gt;**](FieldDescriptor.md) |  |  [optional]
**messageTypes** | [**List&lt;Descriptor&gt;**](Descriptor.md) |  |  [optional]
**name** | **String** |  |  [optional]
**options** | [**FileOptions**](FileOptions.md) |  |  [optional]
**_package** | **String** |  |  [optional]
**publicDependencies** | [**List&lt;FileDescriptor&gt;**](FileDescriptor.md) |  |  [optional]
**services** | [**List&lt;ServiceDescriptor&gt;**](ServiceDescriptor.md) |  |  [optional]



