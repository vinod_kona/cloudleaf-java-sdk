
# CommandSpecifier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**argv** | **List&lt;String&gt;** |  |  [optional]
**cmd** | **String** |  |  [optional]
**doneAt** | **Long** |  |  [optional]
**from** | **String** |  |  [optional]
**rcvdAt** | **Long** |  |  [optional]
**result** | **String** |  |  [optional]
**retcode** | **Long** |  |  [optional]
**sentAt** | **Long** |  |  [optional]
**seqNo** | **Long** |  |  [optional]
**success** | **Boolean** |  |  [optional]



