
# InputType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**defaultValue** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**type** | [**DataType**](DataType.md) |  |  [optional]



