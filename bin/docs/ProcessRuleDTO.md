
# ProcessRuleDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **String** |  |  [optional]
**isRuleset** | **Boolean** |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**recipeId** | **String** |  |  [optional]
**ruleClass** | **String** |  |  [optional]
**ruleId** | **String** |  |  [optional]
**ruleType** | **String** |  |  [optional]
**stageId** | [**StageIdEnum**](#StageIdEnum) |  |  [optional]
**tenantId** | **String** |  |  [optional]


<a name="StageIdEnum"></a>
## Enum: StageIdEnum
Name | Value
---- | -----
CONVERTVALUE | &quot;ConvertValue&quot;
CHANGETRANSFORMTOMETRICS | &quot;ChangeTransformToMetrics&quot;
CHANGETRANSFORMTOSTREAM | &quot;ChangeTransformToStream&quot;
TRANSITION | &quot;Transition&quot;
ALERTSESSION | &quot;AlertSession&quot;
STATEALERTSESSION | &quot;StateAlertSession&quot;
COUNTALERTSESSION | &quot;CountAlertSession&quot;
PRESENCETRACKINGSESSION | &quot;PresenceTrackingSession&quot;
METRICS | &quot;Metrics&quot;
STATS | &quot;Stats&quot;
HEALTHSTATEALERTSESSION | &quot;HealthStateAlertSession&quot;
TEMPERATURESESSION | &quot;TemperatureSession&quot;



