# AssetControllerApi

All URIs are relative to *https://dev.cloudleaf.io/cloudos*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAssetUsingPOST**](AssetControllerApi.md#createAssetUsingPOST) | **POST** /api/1/asset | create asset
[**deleteAssetUsingDELETE**](AssetControllerApi.md#deleteAssetUsingDELETE) | **DELETE** /api/1/asset/{id} | delete asset by ID
[**findAllAssetsByTypeUsingGET**](AssetControllerApi.md#findAllAssetsByTypeUsingGET) | **GET** /api/1/assets/type/{assetType} | find all assets for a asset type
[**getAssetUsingGET**](AssetControllerApi.md#getAssetUsingGET) | **GET** /api/1/asset/{id} | find asset by ID
[**updateAssetUsingPUT1**](AssetControllerApi.md#updateAssetUsingPUT1) | **PUT** /api/1/asset/{id} | save/update asset


<a name="createAssetUsingPOST"></a>
# **createAssetUsingPOST**
> Asset createAssetUsingPOST(asset)

create asset

Create asset with given details.

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.AssetControllerApi;


AssetControllerApi apiInstance = new AssetControllerApi();
Asset asset = new Asset(); // Asset | Asset info to store or update
try {
    Asset result = apiInstance.createAssetUsingPOST(asset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetControllerApi#createAssetUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset** | [**Asset**](Asset.md)| Asset info to store or update |

### Return type

[**Asset**](Asset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteAssetUsingDELETE"></a>
# **deleteAssetUsingDELETE**
> Object deleteAssetUsingDELETE(id)

delete asset by ID

Delete an asset by id. Returns Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.AssetControllerApi;


AssetControllerApi apiInstance = new AssetControllerApi();
String id = "id_example"; // String | Tag Id
try {
    Object result = apiInstance.deleteAssetUsingDELETE(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetControllerApi#deleteAssetUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Tag Id |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="findAllAssetsByTypeUsingGET"></a>
# **findAllAssetsByTypeUsingGET**
> Asset findAllAssetsByTypeUsingGET(assetTypeId)

find all assets for a asset type

Find All Assets for a Asset Type

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.AssetControllerApi;


AssetControllerApi apiInstance = new AssetControllerApi();
String assetTypeId = "assetTypeId_example"; // String | Asset Type Id
try {
    Asset result = apiInstance.findAllAssetsByTypeUsingGET(assetTypeId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetControllerApi#findAllAssetsByTypeUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetTypeId** | **String**| Asset Type Id |

### Return type

[**Asset**](Asset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAssetUsingGET"></a>
# **getAssetUsingGET**
> Asset getAssetUsingGET(id)

find asset by ID

Find a tag by id

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.AssetControllerApi;


AssetControllerApi apiInstance = new AssetControllerApi();
String id = "id_example"; // String | Asset Id
try {
    Asset result = apiInstance.getAssetUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetControllerApi#getAssetUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Asset Id |

### Return type

[**Asset**](Asset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateAssetUsingPUT1"></a>
# **updateAssetUsingPUT1**
> String updateAssetUsingPUT1(id, asset)

save/update asset

Save/Update asset with given details. Returns Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.AssetControllerApi;


AssetControllerApi apiInstance = new AssetControllerApi();
String id = "id_example"; // String | User Id
Asset asset = new Asset(); // Asset | Asset info to store or update
try {
    String result = apiInstance.updateAssetUsingPUT1(id, asset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetControllerApi#updateAssetUsingPUT1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| User Id |
 **asset** | [**Asset**](Asset.md)| Asset info to store or update |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

