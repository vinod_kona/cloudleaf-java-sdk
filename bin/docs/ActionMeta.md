
# ActionMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actionId** | **String** |  |  [optional]
**bulkEnabled** | **Boolean** |  |  [optional]
**inputTypes** | [**List&lt;InputType&gt;**](InputType.md) |  |  [optional]



