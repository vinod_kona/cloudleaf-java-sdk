
# ServiceOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**defaultInstanceForType** | [**ServiceOptions**](ServiceOptions.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**parserForType** | [**ParserServiceOptions**](ParserServiceOptions.md) |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]
**uninterpretedOptionCount** | **Integer** |  |  [optional]
**uninterpretedOptionList** | [**List&lt;UninterpretedOption&gt;**](UninterpretedOption.md) |  |  [optional]
**uninterpretedOptionOrBuilderList** | [**List&lt;UninterpretedOptionOrBuilder&gt;**](UninterpretedOptionOrBuilder.md) |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]



