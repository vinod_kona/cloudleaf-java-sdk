
# OptionOrBuilder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**defaultInstanceForType** | [**Message**](Message.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**optionType** | [**OptionTypeEnum**](#OptionTypeEnum) |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]
**val** | **Long** |  |  [optional]


<a name="OptionTypeEnum"></a>
## Enum: OptionTypeEnum
Name | Value
---- | -----
CONNECTION_INTERVAL | &quot;CONNECTION_INTERVAL&quot;
NOTIFY_TIME_IN_MILLIS | &quot;NOTIFY_TIME_IN_MILLIS&quot;
NOTIFY_TIME_OUT_IN_MILLIS | &quot;NOTIFY_TIME_OUT_IN_MILLIS&quot;
PRIORITY | &quot;PRIORITY&quot;



