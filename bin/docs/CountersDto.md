
# CountersDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alert** | **Map&lt;String, String&gt;** |  |  [optional]
**gateway** | **Map&lt;String, String&gt;** |  |  [optional]
**tag** | **Map&lt;String, String&gt;** |  |  [optional]
**tenantId** | **String** |  |  [optional]
**tenantName** | **String** |  |  [optional]



