
# NotificationContext

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicableId** | **String** |  |  [optional]
**categoryId** | **String** |  |  [optional]
**deviceId** | **String** |  |  [optional]
**level** | **String** |  |  [optional]
**message** | **String** |  |  [optional]
**notifTime** | **Long** |  |  [optional]
**notifType** | **String** |  |  [optional]
**tenantId** | **String** |  |  [optional]
**transport** | **String** |  |  [optional]
**userId** | **String** |  |  [optional]



