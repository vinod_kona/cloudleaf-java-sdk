
# ObjectMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicationName** | **String** |  |  [optional]
**categories** | **List&lt;String&gt;** |  |  [optional]
**id** | **String** |  |  [optional]
**metric** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**properties** | [**List&lt;PropertyMeta&gt;**](PropertyMeta.md) |  |  [optional]
**stage** | **String** |  |  [optional]
**triggerGroups** | **List&lt;String&gt;** |  |  [optional]
**type** | **String** |  |  [optional]



