
# UserGroupDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  |  [optional]
**description** | **String** |  |  [optional]
**tenantId** | **String** |  |  [optional]
**userGroupName** | **String** |  |  [optional]



