# CustomerControllerApi

All URIs are relative to *https://dev.cloudleaf.io/cloudos*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCategoryUsingPOST**](CustomerControllerApi.md#addCategoryUsingPOST) | **POST** /api/1/customer/{custId}/category | add category
[**getCategoriesUsingGET**](CustomerControllerApi.md#getCategoriesUsingGET) | **GET** /api/1/customer/categories | get categories
[**removeCategoryUsingDELETE**](CustomerControllerApi.md#removeCategoryUsingDELETE) | **DELETE** /api/1/customer/category/{catId} | remove category
[**updateCategoryUsingPUT**](CustomerControllerApi.md#updateCategoryUsingPUT) | **PUT** /api/1/customer/category/{catId} | update category


<a name="addCategoryUsingPOST"></a>
# **addCategoryUsingPOST**
> Category addCategoryUsingPOST(custId, category)

add category

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.CustomerControllerApi;


CustomerControllerApi apiInstance = new CustomerControllerApi();
String custId = "custId_example"; // String | custId
Category category = new Category(); // Category | category
try {
    Category result = apiInstance.addCategoryUsingPOST(custId, category);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerControllerApi#addCategoryUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **custId** | **String**| custId |
 **category** | [**Category**](Category.md)| category |

### Return type

[**Category**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCategoriesUsingGET"></a>
# **getCategoriesUsingGET**
> List&lt;Category&gt; getCategoriesUsingGET()

get categories

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.CustomerControllerApi;


CustomerControllerApi apiInstance = new CustomerControllerApi();
try {
    List<Category> result = apiInstance.getCategoriesUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerControllerApi#getCategoriesUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Category&gt;**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeCategoryUsingDELETE"></a>
# **removeCategoryUsingDELETE**
> String removeCategoryUsingDELETE(catId)

remove category

Return Status OK if successful

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.CustomerControllerApi;


CustomerControllerApi apiInstance = new CustomerControllerApi();
String catId = "catId_example"; // String | catId
try {
    String result = apiInstance.removeCategoryUsingDELETE(catId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerControllerApi#removeCategoryUsingDELETE");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catId** | **String**| catId |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updateCategoryUsingPUT"></a>
# **updateCategoryUsingPUT**
> Category updateCategoryUsingPUT(catId, category)

update category

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.CustomerControllerApi;


CustomerControllerApi apiInstance = new CustomerControllerApi();
String catId = "catId_example"; // String | catId
Category category = new Category(); // Category | category
try {
    Category result = apiInstance.updateCategoryUsingPUT(catId, category);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerControllerApi#updateCategoryUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **catId** | **String**| catId |
 **category** | [**Category**](Category.md)| category |

### Return type

[**Category**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

