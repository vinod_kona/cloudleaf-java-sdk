
# Tasks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**commandsCount** | **Integer** |  |  [optional]
**commandsList** | [**List&lt;Command&gt;**](Command.md) |  |  [optional]
**commandsOrBuilderList** | [**List&lt;CommandOrBuilder&gt;**](CommandOrBuilder.md) |  |  [optional]
**defaultInstanceForType** | [**Tasks**](Tasks.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**parserForType** | [**ParserTasks**](ParserTasks.md) |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]



