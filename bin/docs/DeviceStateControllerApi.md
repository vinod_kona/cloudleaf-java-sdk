# DeviceStateControllerApi

All URIs are relative to *https://dev.cloudleaf.io/cloudos*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDeviceStateForAllUsingGET**](DeviceStateControllerApi.md#getDeviceStateForAllUsingGET) | **GET** /api/1/device/all/{id}/{type} | retrieve all device events in a time range
[**getDeviceStateForNameUsingGET**](DeviceStateControllerApi.md#getDeviceStateForNameUsingGET) | **GET** /api/1/device/single/{id}/{type}/{name}/{module} | retrieve named device events in time range
[**getLastDeviceStateInBulkUsingGET**](DeviceStateControllerApi.md#getLastDeviceStateInBulkUsingGET) | **GET** /api/1/device/latest/{type}/bulk | get last device state of all devices and modules in bulk for given type
[**getLastDeviceStateUsingGET**](DeviceStateControllerApi.md#getLastDeviceStateUsingGET) | **GET** /api/1/device/{id}/{type} | get last device state
[**getLastDeviceStateofModuleUsingGET**](DeviceStateControllerApi.md#getLastDeviceStateofModuleUsingGET) | **GET** /api/1/device/{id}/{type}/{level}/{module} | get last device state by id, type and module
[**postDeviceEventUsingPOST**](DeviceStateControllerApi.md#postDeviceEventUsingPOST) | **POST** /api/1/device/bulk | post device events in Bulk
[**postDeviceEventUsingPOST1**](DeviceStateControllerApi.md#postDeviceEventUsingPOST1) | **POST** /api/1/device/{id}/{type} | post device event


<a name="getDeviceStateForAllUsingGET"></a>
# **getDeviceStateForAllUsingGET**
> DeviceEventResult getDeviceStateForAllUsingGET(id, type, to, from, limit)

retrieve all device events in a time range

Gets all device events in a time range given id, type

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceStateControllerApi;


DeviceStateControllerApi apiInstance = new DeviceStateControllerApi();
String id = "id_example"; // String | device identifier
String type = "type_example"; // String | device type
Long to = 789L; // Long | to specified time range in UTC millis
Long from = 789L; // Long | from specified time range in UTC millis
Integer limit = 300; // Integer | limit data values
try {
    DeviceEventResult result = apiInstance.getDeviceStateForAllUsingGET(id, type, to, from, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceStateControllerApi#getDeviceStateForAllUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| device identifier |
 **type** | **String**| device type |
 **to** | **Long**| to specified time range in UTC millis | [optional]
 **from** | **Long**| from specified time range in UTC millis | [optional]
 **limit** | **Integer**| limit data values | [optional] [default to 300]

### Return type

[**DeviceEventResult**](DeviceEventResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getDeviceStateForNameUsingGET"></a>
# **getDeviceStateForNameUsingGET**
> DeviceEventResult getDeviceStateForNameUsingGET(id, type, name, module, to, from, limit)

retrieve named device events in time range

Gets the named device events in a time range given id, type, module, name

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceStateControllerApi;


DeviceStateControllerApi apiInstance = new DeviceStateControllerApi();
String id = "id_example"; // String | device identifier
String type = "type_example"; // String | device type
String name = "name_example"; // String | property name
String module = "module_example"; // String | module category
Long to = 789L; // Long | to specified time range in UTC millis
Long from = 789L; // Long | from specified time range in UTC millis
Integer limit = 300; // Integer | limit data values
try {
    DeviceEventResult result = apiInstance.getDeviceStateForNameUsingGET(id, type, name, module, to, from, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceStateControllerApi#getDeviceStateForNameUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| device identifier |
 **type** | **String**| device type |
 **name** | **String**| property name |
 **module** | **String**| module category |
 **to** | **Long**| to specified time range in UTC millis | [optional]
 **from** | **Long**| from specified time range in UTC millis | [optional]
 **limit** | **Integer**| limit data values | [optional] [default to 300]

### Return type

[**DeviceEventResult**](DeviceEventResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getLastDeviceStateInBulkUsingGET"></a>
# **getLastDeviceStateInBulkUsingGET**
> List&lt;DeviceEventResult&gt; getLastDeviceStateInBulkUsingGET(type)

get last device state of all devices and modules in bulk for given type

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceStateControllerApi;


DeviceStateControllerApi apiInstance = new DeviceStateControllerApi();
String type = "type_example"; // String | Device Type
try {
    List<DeviceEventResult> result = apiInstance.getLastDeviceStateInBulkUsingGET(type);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceStateControllerApi#getLastDeviceStateInBulkUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**| Device Type |

### Return type

[**List&lt;DeviceEventResult&gt;**](DeviceEventResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getLastDeviceStateUsingGET"></a>
# **getLastDeviceStateUsingGET**
> DeviceEventResult getLastDeviceStateUsingGET(id, type)

get last device state

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceStateControllerApi;


DeviceStateControllerApi apiInstance = new DeviceStateControllerApi();
String id = "id_example"; // String | Device Id
String type = "type_example"; // String | Device Type
try {
    DeviceEventResult result = apiInstance.getLastDeviceStateUsingGET(id, type);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceStateControllerApi#getLastDeviceStateUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Device Id |
 **type** | **String**| Device Type |

### Return type

[**DeviceEventResult**](DeviceEventResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getLastDeviceStateofModuleUsingGET"></a>
# **getLastDeviceStateofModuleUsingGET**
> DeviceEventResult getLastDeviceStateofModuleUsingGET(id, type, level, module)

get last device state by id, type and module

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceStateControllerApi;


DeviceStateControllerApi apiInstance = new DeviceStateControllerApi();
String id = "id_example"; // String | Device Id
String type = "type_example"; // String | Device Type
String level = "level_example"; // String | Level
String module = "module_example"; // String | module category
try {
    DeviceEventResult result = apiInstance.getLastDeviceStateofModuleUsingGET(id, type, level, module);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceStateControllerApi#getLastDeviceStateofModuleUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Device Id |
 **type** | **String**| Device Type |
 **level** | **String**| Level |
 **module** | **String**| module category |

### Return type

[**DeviceEventResult**](DeviceEventResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postDeviceEventUsingPOST"></a>
# **postDeviceEventUsingPOST**
> List&lt;String&gt; postDeviceEventUsingPOST(dataList)

post device events in Bulk

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceStateControllerApi;


DeviceStateControllerApi apiInstance = new DeviceStateControllerApi();
List<DeviceEvent> dataList = Arrays.asList(new DeviceEvent()); // List<DeviceEvent> | Event data List
try {
    List<String> result = apiInstance.postDeviceEventUsingPOST(dataList);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceStateControllerApi#postDeviceEventUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataList** | [**List&lt;DeviceEvent&gt;**](DeviceEvent.md)| Event data List |

### Return type

**List&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/x-protobuf
 - **Accept**: */*

<a name="postDeviceEventUsingPOST1"></a>
# **postDeviceEventUsingPOST1**
> String postDeviceEventUsingPOST1(id, type, data)

post device event

### Example
```java
// Import classes:
//import com.cloudleaf.cloudos.sdk.ApiException;
//import com.cloudleaf.cloudos.sdk.api.DeviceStateControllerApi;


DeviceStateControllerApi apiInstance = new DeviceStateControllerApi();
String id = "id_example"; // String | Device Id
String type = "type_example"; // String | Device Type
DeviceEvent data = new DeviceEvent(); // DeviceEvent | Event data
try {
    String result = apiInstance.postDeviceEventUsingPOST1(id, type, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceStateControllerApi#postDeviceEventUsingPOST1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Device Id |
 **type** | **String**| Device Type |
 **data** | [**DeviceEvent**](DeviceEvent.md)| Event data |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/x-protobuf
 - **Accept**: */*

