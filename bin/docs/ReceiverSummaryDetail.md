
# ReceiverSummaryDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**areaId** | **String** |  |  [optional]
**areaName** | **String** |  |  [optional]
**buildingId** | **String** |  |  [optional]
**buildingName** | **String** |  |  [optional]
**identifier** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**parentId** | **String** |  |  [optional]
**parentName** | **String** |  |  [optional]
**physicalId** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**tenantId** | **String** |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNPROVISIONED | &quot;UNPROVISIONED&quot;
PROVISIONED | &quot;PROVISIONED&quot;
UP | &quot;UP&quot;
DOWN | &quot;DOWN&quot;
DEPROVISIONED | &quot;DEPROVISIONED&quot;
SOFTPROVISIONED | &quot;SOFTPROVISIONED&quot;
READY | &quot;READY&quot;


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
OMNI_RECEIVER | &quot;OMNI_RECEIVER&quot;
MOBILE_RECEIVER | &quot;MOBILE_RECEIVER&quot;
OUT_OF_COVERAGE_PSEUDO_RECEIVER | &quot;OUT_OF_COVERAGE_PSEUDO_RECEIVER&quot;
SOFT_RECEIVER | &quot;SOFT_RECEIVER&quot;
LOCATION_MARKER | &quot;LOCATION_MARKER&quot;
CLOUD_CONNECTOR | &quot;CLOUD_CONNECTOR&quot;
MICRO_CLOUD_CONNECTOR | &quot;MICRO_CLOUD_CONNECTOR&quot;



