
# PostalAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **String** |  |  [optional]
**countryCode** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**stateCode** | **String** |  |  [optional]
**streetAddr** | **String** |  |  [optional]



