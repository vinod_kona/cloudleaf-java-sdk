
# ClfSearchResponseSensorInventoryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resultList** | [**List&lt;SensorInventoryDto&gt;**](SensorInventoryDto.md) |  |  [optional]
**scrollId** | **String** |  |  [optional]
**totalCount** | **Long** |  |  [optional]



