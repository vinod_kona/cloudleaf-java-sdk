
# AssetResultValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**areaName** | **Object** |  |  [optional]
**cId** | **String** |  |  [optional]
**receiverId** | **Object** |  |  [optional]
**time** | [**Date**](Date.md) |  |  [optional]
**value** | **Object** |  |  [optional]



