
# ValidationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **Integer** |  |  [optional]
**errorMessage** | **Map&lt;String, String&gt;** |  |  [optional]



