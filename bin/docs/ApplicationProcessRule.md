
# ApplicationProcessRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **String** |  |  [optional]
**stages** | [**List&lt;ProcessRuleStage&gt;**](ProcessRuleStage.md) |  |  [optional]



