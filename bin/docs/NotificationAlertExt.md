
# NotificationAlertExt

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alertType** | **String** |  |  [optional]
**areaName** | **String** |  |  [optional]
**assetCategoryId** | **String** |  |  [optional]
**assetCategoryName** | **String** |  |  [optional]
**assetExternalId** | **String** |  |  [optional]
**assetId** | **String** |  |  [optional]
**assetName** | **String** |  |  [optional]
**level** | **String** |  |  [optional]
**message** | **String** |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**siteName** | **String** |  |  [optional]
**sourceId** | **String** |  |  [optional]
**sourceType** | **String** |  |  [optional]
**tagId** | **String** |  |  [optional]
**taggedAssetId** | **String** |  |  [optional]
**tenantId** | **String** |  |  [optional]
**time** | [**Date**](Date.md) |  |  [optional]



