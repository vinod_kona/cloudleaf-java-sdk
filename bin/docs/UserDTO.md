
# UserDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**details** | **Map&lt;String, String&gt;** |  |  [optional]
**groups** | **List&lt;String&gt;** |  |  [optional]
**id** | **String** | user id | 
**locked** | **Boolean** |  |  [optional]
**modifiedTime** | [**Date**](Date.md) |  |  [optional]
**passwordExpiry** | [**Date**](Date.md) |  |  [optional]
**passwordExpiryInMillis** | **String** |  |  [optional]
**primaryEmail** | **String** |  |  [optional]
**proxyLogin** | **Boolean** |  |  [optional]
**proxyUser** | **String** |  |  [optional]
**securityQset** | **Boolean** |  |  [optional]
**securityQuestions** | **Map&lt;String, String&gt;** |  |  [optional]
**userDevices** | [**List&lt;UserDevice&gt;**](UserDevice.md) |  |  [optional]
**tenantId** | **String** | organization id the user belongs | 
**password** | **String** | user login password | 
**name** | **String** | user display name |  [optional]
**enabled** | **Boolean** | active user |  [optional]
**type** | [**TypeEnum**](#TypeEnum) | user type |  [optional]
**roles** | **List&lt;String&gt;** | roles |  [optional]
**permissions** | **List&lt;String&gt;** | permissions |  [optional]
**properties** | **Map&lt;String, String&gt;** | custom properties |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
NORMAL_USER | &quot;NORMAL_USER&quot;
API_USER | &quot;API_USER&quot;



