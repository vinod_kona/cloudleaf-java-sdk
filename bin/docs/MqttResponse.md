
# MqttResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allFields** | **Map&lt;String, Object&gt;** |  |  [optional]
**correlationId** | **String** |  |  [optional]
**correlationIdBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**dataCount** | **Integer** |  |  [optional]
**dataList** | [**List&lt;Property&gt;**](Property.md) |  |  [optional]
**dataOrBuilderList** | [**List&lt;PropertyOrBuilder&gt;**](PropertyOrBuilder.md) |  |  [optional]
**defaultInstanceForType** | [**MqttResponse**](MqttResponse.md) |  |  [optional]
**descriptorForType** | [**Descriptor**](Descriptor.md) |  |  [optional]
**initializationErrorString** | **String** |  |  [optional]
**initialized** | **Boolean** |  |  [optional]
**parserForType** | [**ParserMqttResponse**](ParserMqttResponse.md) |  |  [optional]
**responseCode** | **Integer** |  |  [optional]
**responseTime** | **Long** |  |  [optional]
**responseType** | **String** |  |  [optional]
**responseTypeBytes** | [**ByteString**](ByteString.md) |  |  [optional]
**serializedSize** | **Integer** |  |  [optional]
**unknownFields** | [**UnknownFieldSet**](UnknownFieldSet.md) |  |  [optional]



