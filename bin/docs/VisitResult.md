
# VisitResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dwellTime** | **Long** |  |  [optional]
**entry** | [**Date**](Date.md) |  |  [optional]
**exit** | [**Date**](Date.md) |  |  [optional]
**regionId** | **String** |  |  [optional]
**regionName** | **String** |  |  [optional]
**regionType** | **String** |  |  [optional]
**subRegions** | [**List&lt;VisitResult&gt;**](VisitResult.md) |  |  [optional]



