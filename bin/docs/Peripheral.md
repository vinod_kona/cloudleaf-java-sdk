
# Peripheral

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | [**Tasks**](Tasks.md) |  |  [optional]
**leafId** | **String** |  |  [optional]
**peripheralId** | **String** | Peripheral Id | 
**type** | **String** | Peripheral type | 
**deviceClass** | **String** | Peripheral Device Class |  [optional]
**name** | **String** | Name |  [optional]
**receiverId** | **String** | Leaf Reader Id | 
**data** | **Map&lt;String, String&gt;** | Relevant peripheral data at registration time |  [optional]
**status** | [**StatusEnum**](#StatusEnum) | Status |  [optional]
**lastUpdate** | [**Date**](Date.md) | Time of last data update from this peripheral |  [optional]
**tasks** | [**Map&lt;String, List&lt;Command&gt;&gt;**](List.md) | Map of tasks by service id |  [optional]
**tenantId** | **String** | Owning tenant |  [optional]
**physicalId** | **String** | Physical Id, e.g., the MAC Address |  [optional]
**tags** | **List&lt;String&gt;** | Tags associated with this peripheral |  [optional]
**createdAt** | [**Date**](Date.md) | Peripheral provisioning date |  [optional]
**description** | **String** | Description |  [optional]
**categoryId** | **String** | category Id |  [optional]
**assetId** | **String** | asset Id |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
UNPROVISIONED | &quot;UNPROVISIONED&quot;
PROVISIONED | &quot;PROVISIONED&quot;
DISABLED | &quot;DISABLED&quot;



