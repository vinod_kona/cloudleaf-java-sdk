package com.cloudleaf.cloudos.sdk.api;

import com.cloudleaf.cloudos.sdk.ApiClient;

import com.cloudleaf.cloudos.sdk.model.CmdSpecifierRequest;
import com.cloudleaf.cloudos.sdk.model.CommandSpecifier;
import com.cloudleaf.cloudos.sdk.model.ResponseEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")
@Component("com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi")
public class DeviceCommandControllerApi {
    private ApiClient apiClient;

    public DeviceCommandControllerApi() {
        this(new ApiClient());
    }

    @Autowired
    public DeviceCommandControllerApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Delete commands by device Id 
     * Delete commands by device Id 
     * <p><b>200</b> - Successfully deleted
     * <p><b>204</b> - No Content
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>417</b> - Exception while deleting the commands 
     * @param deviceId deviceId
     * @param issuedfrom issuedfrom
     * @param fromTime from specified time range in UTC millis
     * @return ResponseEntity
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity deleteCommandsByDeviceIdUsingDELETE(String deviceId, String issuedfrom, Long fromTime) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'deviceId' is set
        if (deviceId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'deviceId' when calling deleteCommandsByDeviceIdUsingDELETE");
        }
        
        // verify the required parameter 'issuedfrom' is set
        if (issuedfrom == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'issuedfrom' when calling deleteCommandsByDeviceIdUsingDELETE");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("deviceId", deviceId);
        uriVariables.put("issuedfrom", issuedfrom);
        String path = UriComponentsBuilder.fromPath("/api/1/kdcp/{deviceId}/{issuedfrom}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "fromTime", fromTime));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ResponseEntity> returnType = new ParameterizedTypeReference<ResponseEntity>() {};
        return apiClient.invokeAPI(path, HttpMethod.DELETE, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * executeTagAction
     * 
     * <p><b>200</b> - OK
     * <p><b>201</b> - Created
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param deviceId deviceId
     * @param commandSpecifier commandSpecifier
     * @return Object
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Object executeTagActionUsingPOST(String deviceId, CmdSpecifierRequest commandSpecifier) throws RestClientException {
        Object postBody = commandSpecifier;
        
        // verify the required parameter 'deviceId' is set
        if (deviceId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'deviceId' when calling executeTagActionUsingPOST");
        }
        
        // verify the required parameter 'commandSpecifier' is set
        if (commandSpecifier == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'commandSpecifier' when calling executeTagActionUsingPOST");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("deviceId", deviceId);
        String path = UriComponentsBuilder.fromPath("/api/1/kdcp/{deviceId}/cmd").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Object> returnType = new ParameterizedTypeReference<Object>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get Commands issued by a source 
     * Get Commands issued by a source 
     * <p><b>200</b> - Commands found
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Commands not found for the Commands issued by a source 
     * @param userId userId
     * @param seqNo Sequence Number
     * @return CommandSpecifier
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public CommandSpecifier getCmdBySeqNoUsingGET(String userId, Long seqNo) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'userId' is set
        if (userId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'userId' when calling getCmdBySeqNoUsingGET");
        }
        
        // verify the required parameter 'seqNo' is set
        if (seqNo == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'seqNo' when calling getCmdBySeqNoUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("userId", userId);
        uriVariables.put("seqNo", seqNo);
        String path = UriComponentsBuilder.fromPath("/api/1/kdcp/{userId}/{seqNo}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<CommandSpecifier> returnType = new ParameterizedTypeReference<CommandSpecifier>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get Commands issued by a source 
     * Get Commands issued by a source 
     * <p><b>200</b> - Commands found
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Commands not found for the Commands issued by a source 
     * @param userId userId
     * @return List&lt;CommandSpecifier&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<CommandSpecifier> getCmdsByFromUsingGET(String userId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'userId' is set
        if (userId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'userId' when calling getCmdsByFromUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("userId", userId);
        String path = UriComponentsBuilder.fromPath("/api/1/kdcp/{userId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<CommandSpecifier>> returnType = new ParameterizedTypeReference<List<CommandSpecifier>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get Commands History by Device Id and cmd 
     * Get Commands History by Device Id and cmd
     * <p><b>200</b> - Commands found
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Commands not found for the ID  and command supplied
     * @param deviceId Device Id
     * @param cmd Command
     * @param fromTime from specified time range in UTC millis
     * @param toTime to specified time range in UTC millis
     * @param limit limit data values
     * @return List&lt;CommandSpecifier&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<CommandSpecifier> getCmdsHistoryByCmdUsingGET(String deviceId, String cmd, Long fromTime, Long toTime, Integer limit) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'deviceId' is set
        if (deviceId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'deviceId' when calling getCmdsHistoryByCmdUsingGET");
        }
        
        // verify the required parameter 'cmd' is set
        if (cmd == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'cmd' when calling getCmdsHistoryByCmdUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("deviceId", deviceId);
        uriVariables.put("cmd", cmd);
        String path = UriComponentsBuilder.fromPath("/api/1/kdcp/{deviceId}/{cmd}/history").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "fromTime", fromTime));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "toTime", toTime));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "limit", limit));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<CommandSpecifier>> returnType = new ParameterizedTypeReference<List<CommandSpecifier>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get Commands History by Device Id 
     * Get Commands History by Device Id
     * <p><b>200</b> - Commands found
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Commands not found for the ID supplied
     * @param deviceId Device Id
     * @param fromTime from specified time range in UTC millis
     * @param toTime to specified time range in UTC millis
     * @param limit limit data values
     * @return List&lt;CommandSpecifier&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<CommandSpecifier> getCmdsHistoryByDeviceIdUsingGET(String deviceId, Long fromTime, Long toTime, Integer limit) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'deviceId' is set
        if (deviceId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'deviceId' when calling getCmdsHistoryByDeviceIdUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("deviceId", deviceId);
        String path = UriComponentsBuilder.fromPath("/api/1/kdcp/{deviceId}/history").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "fromTime", fromTime));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "toTime", toTime));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "limit", limit));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<CommandSpecifier>> returnType = new ParameterizedTypeReference<List<CommandSpecifier>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get Commands History by Device Id , Issued from and cmd 
     * Get Commands History by Device Id, Issued from and cmd
     * <p><b>200</b> - Commands found
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Commands not found for the ID, Issued from  and command supplied
     * @param deviceId Device Id
     * @param cmd Command
     * @param userId userId
     * @param fromTime from specified time range in UTC millis
     * @param toTime to specified time range in UTC millis
     * @param limit limit data values
     * @return List&lt;CommandSpecifier&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<CommandSpecifier> getCmdsHistoryByFromUsingGET(String deviceId, String cmd, String userId, Long fromTime, Long toTime, Integer limit) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'deviceId' is set
        if (deviceId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'deviceId' when calling getCmdsHistoryByFromUsingGET");
        }
        
        // verify the required parameter 'cmd' is set
        if (cmd == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'cmd' when calling getCmdsHistoryByFromUsingGET");
        }
        
        // verify the required parameter 'userId' is set
        if (userId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'userId' when calling getCmdsHistoryByFromUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("deviceId", deviceId);
        uriVariables.put("cmd", cmd);
        uriVariables.put("userId", userId);
        String path = UriComponentsBuilder.fromPath("/api/1/kdcp/{deviceId}/{cmd}/{userId}/history").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "fromTime", fromTime));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "toTime", toTime));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "limit", limit));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<CommandSpecifier>> returnType = new ParameterizedTypeReference<List<CommandSpecifier>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get Most Recent Commands by Device Id and Command
     * Get Most Recent Commands by Device Id and Command
     * <p><b>200</b> - Commands found
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Commands not found for the ID and command supplied
     * @param deviceId Device Id
     * @param cmd Command
     * @return CommandSpecifier
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public CommandSpecifier getMostRecentCmdByCmdUsingGET(String deviceId, String cmd) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'deviceId' is set
        if (deviceId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'deviceId' when calling getMostRecentCmdByCmdUsingGET");
        }
        
        // verify the required parameter 'cmd' is set
        if (cmd == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'cmd' when calling getMostRecentCmdByCmdUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("deviceId", deviceId);
        uriVariables.put("cmd", cmd);
        String path = UriComponentsBuilder.fromPath("/api/1/kdcp/{deviceId}/{cmd}/recent").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<CommandSpecifier> returnType = new ParameterizedTypeReference<CommandSpecifier>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get Most Recent Commands by Device Id
     * Get Most Recent Commands by Device Id
     * <p><b>200</b> - Commands found
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Commands not found for the ID supplied
     * @param deviceId Device Id
     * @return List&lt;CommandSpecifier&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<CommandSpecifier> getMostRecentCmdsByDeviceIdUsingGET(String deviceId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'deviceId' is set
        if (deviceId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'deviceId' when calling getMostRecentCmdsByDeviceIdUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("deviceId", deviceId);
        String path = UriComponentsBuilder.fromPath("/api/1/kdcp/{deviceId}/recent").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<CommandSpecifier>> returnType = new ParameterizedTypeReference<List<CommandSpecifier>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
}
