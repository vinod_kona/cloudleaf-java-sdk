package com.cloudleaf.cloudos.sdk.api;

import com.cloudleaf.cloudos.sdk.ApiClient;

import com.cloudleaf.cloudos.sdk.model.EventData;
import com.cloudleaf.cloudos.sdk.model.EventListData;
import com.cloudleaf.cloudos.sdk.model.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")
@Component("com.cloudleaf.cloudos.sdk.api.DataControllerApi")
public class DataControllerApi {
    private ApiClient apiClient;

    public DataControllerApi() {
        this(new ApiClient());
    }

    @Autowired
    public DataControllerApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * retrieve data stream range
     * Gets the data stream in most recent time order for a given period of time for a characteristic for a given object.  The time period is defined using the &#39;from&#39; parameter and the &#39;to&#39; parameter.  If &#39;to&#39; is unspecified, the current time is used for the to time.  If &#39;from&#39; is unspecified, the from time is set to the start of the &#39;to&#39; day in UTC.  &#39;limit&#39; is used to restrict the set of data values returned, default 500.  If the limit is reached, the client will need to use this API again setting the &#39;to&#39; value to time of the last retrieve data value
     * <p><b>200</b> - Success
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Invalid peripheral Id
     * @param id Stream Id
     * @param sId service identifier
     * @param cId characteristic identifier
     * @param to to specified time range in UTC millis
     * @param from from specified time range in UTC millis
     * @param limit limit data values
     * @return Result
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Result getDataForCharacteristicUsingGET(String id, String sId, String cId, Long to, Long from, Integer limit) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'id' when calling getDataForCharacteristicUsingGET");
        }
        
        // verify the required parameter 'sId' is set
        if (sId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'sId' when calling getDataForCharacteristicUsingGET");
        }
        
        // verify the required parameter 'cId' is set
        if (cId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'cId' when calling getDataForCharacteristicUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("id", id);
        uriVariables.put("sId", sId);
        uriVariables.put("cId", cId);
        String path = UriComponentsBuilder.fromPath("/api/1/data/{id}/{sId}/{cId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "to", to));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "from", from));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "limit", limit));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Result> returnType = new ParameterizedTypeReference<Result>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * retrieve last data snapshot for peripheral
     * Gets the last data value for every tracked service for a given peripheral
     * <p><b>200</b> - Success
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Invalid peripheral Id
     * @param id id
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String getLastSnapshotUsingGET(String id) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'id' when calling getLastSnapshotUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("id", id);
        String path = UriComponentsBuilder.fromPath("/api/1/data/{id}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * add bulk Event Data
     * Uploads bulk events data to the peripheral stream
     * <p><b>200</b> - Added
     * <p><b>201</b> - Created
     * <p><b>400</b> - Invalid input data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param data Event list data
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String postListToStreamUsingPOST(EventListData data) throws RestClientException {
        Object postBody = data;
        
        // verify the required parameter 'data' is set
        if (data == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'data' when calling postListToStreamUsingPOST");
        }
        
        String path = UriComponentsBuilder.fromPath("/api/1/data/stream/bulk").build().toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json", "application/x-protobuf"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * add data for peripheral
     * Uploads event data for a service and adds to the peripheral stream
     * <p><b>200</b> - Added
     * <p><b>201</b> - Created
     * <p><b>400</b> - Invalid input data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param id Peripheral Id
     * @param data Event data
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String postToStreamUsingPOST(String id, EventData data) throws RestClientException {
        Object postBody = data;
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'id' when calling postToStreamUsingPOST");
        }
        
        // verify the required parameter 'data' is set
        if (data == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'data' when calling postToStreamUsingPOST");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("id", id);
        String path = UriComponentsBuilder.fromPath("/api/1/data/stream/{id}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json", "application/x-protobuf"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
}
