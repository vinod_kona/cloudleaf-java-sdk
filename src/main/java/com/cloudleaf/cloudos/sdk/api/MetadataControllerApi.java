package com.cloudleaf.cloudos.sdk.api;

import com.cloudleaf.cloudos.sdk.ApiClient;

import com.cloudleaf.cloudos.sdk.model.CharacteristicMeta;
import java.io.File;
import com.cloudleaf.cloudos.sdk.model.PeripheralCapabilities;
import com.cloudleaf.cloudos.sdk.model.PeripheralMeta;
import com.cloudleaf.cloudos.sdk.model.PeripheralTypeMeta;
import com.cloudleaf.cloudos.sdk.model.ServiceMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")
@Component("com.cloudleaf.cloudos.sdk.api.MetadataControllerApi")
public class MetadataControllerApi {
    private ApiClient apiClient;

    public MetadataControllerApi() {
        this(new ApiClient());
    }

    @Autowired
    public MetadataControllerApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * add characteristic meta data
     * Uploads characteristic metadata for a peripheral.
     * <p><b>200</b> - Added characteristic metadata
     * <p><b>201</b> - Created
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param pType Peripheral type identifier
     * @param sId service identifier
     * @param cId characteristic identifier
     * @param characteristicMeta Characteristic metadata
     * @return PeripheralMeta
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public PeripheralMeta addCharacteristicMetaUsingPOST(String pType, String sId, String cId, CharacteristicMeta characteristicMeta) throws RestClientException {
        Object postBody = characteristicMeta;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling addCharacteristicMetaUsingPOST");
        }
        
        // verify the required parameter 'sId' is set
        if (sId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'sId' when calling addCharacteristicMetaUsingPOST");
        }
        
        // verify the required parameter 'cId' is set
        if (cId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'cId' when calling addCharacteristicMetaUsingPOST");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        uriVariables.put("sId", sId);
        uriVariables.put("cId", cId);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}/service/{sId}/characteristic/{cId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<PeripheralMeta> returnType = new ParameterizedTypeReference<PeripheralMeta>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * add peripheral metadata
     * Uploads metadata for a peripheral. Tasks and IdPath are output only and not used when uploading metadata
     * <p><b>200</b> - Added peripheral metadata
     * <p><b>201</b> - Created
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param pType Peripheral type identifier
     * @param deviceMeta Peripheral metadata - I/O object used to describe the metadata of a peripheral, its services, characteristics, and the tasks to be performed by CloudLeaf readers to access data on the peripheral
     * @return PeripheralMeta
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public PeripheralMeta addDeviceMetadataUsingPOST(String pType, PeripheralMeta deviceMeta) throws RestClientException {
        Object postBody = deviceMeta;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling addDeviceMetadataUsingPOST");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<PeripheralMeta> returnType = new ParameterizedTypeReference<PeripheralMeta>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * add peripheral type
     * Add a new peripheral type. Returns Status CREATED if successful.
     * <p><b>200</b> - OK
     * <p><b>201</b> - Added Type Info
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * <p><b>409</b> - Type already exists
     * @param pType I/O object used to describe peripheral types
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String addPeripheralTypeUsingPOST(PeripheralTypeMeta pType) throws RestClientException {
        Object postBody = pType;
        
        String path = UriComponentsBuilder.fromPath("/api/1/meta/pTypes").build().toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * add service meta data
     * Uploads service metadata for a peripheral type
     * <p><b>200</b> - Added service metadata
     * <p><b>201</b> - Created
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param pType Peripheral type identifier
     * @param sId service identifier
     * @param serviceMeta Service metadata
     * @return PeripheralMeta
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public PeripheralMeta addServiceMetadataUsingPOST(String pType, String sId, ServiceMeta serviceMeta) throws RestClientException {
        Object postBody = serviceMeta;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling addServiceMetadataUsingPOST");
        }
        
        // verify the required parameter 'sId' is set
        if (sId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'sId' when calling addServiceMetadataUsingPOST");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        uriVariables.put("sId", sId);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}/service/{sId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<PeripheralMeta> returnType = new ParameterizedTypeReference<PeripheralMeta>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * delete characteristic metadata of a ptype
     * Delete characteristic meta data of a ptype. Returns Status OK if successful.
     * <p><b>200</b> - Deleted characteristic metadata of a ptype
     * <p><b>204</b> - No Content
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * @param pType Peripheral type identifier
     * @param sId service identifier
     * @param cId characteristic identifier
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String deleteCharacteristicMetadataUsingDELETE(String pType, String sId, String cId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling deleteCharacteristicMetadataUsingDELETE");
        }
        
        // verify the required parameter 'sId' is set
        if (sId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'sId' when calling deleteCharacteristicMetadataUsingDELETE");
        }
        
        // verify the required parameter 'cId' is set
        if (cId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'cId' when calling deleteCharacteristicMetadataUsingDELETE");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        uriVariables.put("sId", sId);
        uriVariables.put("cId", cId);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}/service/{sId}/characteristic/{cId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.DELETE, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * delete peripheral metadata
     * Delete peripheral metadata. Returns Status OK if successful.
     * <p><b>200</b> - Deleted peripheral metadata
     * <p><b>204</b> - No Content
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * @param pType Peripheral type identifier
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String deletePTypeMetadataUsingDELETE(String pType) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling deletePTypeMetadataUsingDELETE");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.DELETE, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * delete peripheral type
     * Delete peripheral type. Returns Status OK if successful.
     * <p><b>200</b> - Deleted peripheral type
     * <p><b>204</b> - No Content
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * @param regEx Peripheral type identifier
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String deletePeripheralTypeUsingDELETE(String regEx) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'regEx' is set
        if (regEx == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'regEx' when calling deletePeripheralTypeUsingDELETE");
        }
        
        String path = UriComponentsBuilder.fromPath("/api/1/meta/pTypes").build().toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "regEx", regEx));

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.DELETE, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * delete service metadata of a ptype
     * Delete service meta data of a ptype. Returns Status OK if successful.
     * <p><b>200</b> - Deleted service metadata of a ptype
     * <p><b>204</b> - No Content
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * @param pType Peripheral type identifier
     * @param sId service identifier
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String deleteServiceMetadataUsingDELETE(String pType, String sId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling deleteServiceMetadataUsingDELETE");
        }
        
        // verify the required parameter 'sId' is set
        if (sId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'sId' when calling deleteServiceMetadataUsingDELETE");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        uriVariables.put("sId", sId);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}/service/{sId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.DELETE, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * disable peripheral types for tenant
     * Disable peripheral types for this partner. Returns Status OK if successful.
     * <p><b>200</b> - OK
     * <p><b>201</b> - Added Type Info
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param tenantId tenantId of the tenant
     * @param types Comma sperated list of peripheral types
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String disablePeripheralTypesForTenantUsingPOST(String tenantId, List<String> types) throws RestClientException {
        Object postBody = types;
        
        // verify the required parameter 'tenantId' is set
        if (tenantId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'tenantId' when calling disablePeripheralTypesForTenantUsingPOST");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("tenantId", tenantId);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/tenant/{tenantId}/pTypes/disable").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * disable peripheral types
     * Disable peripheral types for this partner. Returns Status OK if successful.
     * <p><b>200</b> - OK
     * <p><b>201</b> - Added Type Info
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param types Comma sperated list of peripheral types
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String disablePeripheralTypesUsingPOST(List<String> types) throws RestClientException {
        Object postBody = types;
        
        String path = UriComponentsBuilder.fromPath("/api/1/meta/pTypes/disable").build().toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * enable peripheral types
     * Enable peripheral types for this partner. Returns Status OK if successful.
     * <p><b>200</b> - OK
     * <p><b>201</b> - Added Type Info
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param types Comma sperated list of peripheral types
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String enablePeripheralTypesUsingPOST(List<String> types) throws RestClientException {
        Object postBody = types;
        
        String path = UriComponentsBuilder.fromPath("/api/1/meta/pTypes/enable").build().toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * get cability
     * 
     * <p><b>200</b> - OK
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param pType pType
     * @return PeripheralCapabilities
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public PeripheralCapabilities getCapabilityUsingGET(String pType) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling getCapabilityUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/{pType}/capability").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<PeripheralCapabilities> returnType = new ParameterizedTypeReference<PeripheralCapabilities>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * get characteristic metadata
     * Get characteristic metadata 
     * <p><b>200</b> - Get characteristic metadata
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Unknown peripheral type identifier
     * @param pType Peripheral type identifier
     * @param sId service identifier
     * @param cId characteristic identifier
     * @return ServiceMeta
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ServiceMeta getCharacteristicMetadataUsingGET(String pType, String sId, String cId) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling getCharacteristicMetadataUsingGET");
        }
        
        // verify the required parameter 'sId' is set
        if (sId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'sId' when calling getCharacteristicMetadataUsingGET");
        }
        
        // verify the required parameter 'cId' is set
        if (cId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'cId' when calling getCharacteristicMetadataUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        uriVariables.put("sId", sId);
        uriVariables.put("cId", cId);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}/service/{sId}/characteristic/{cId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ServiceMeta> returnType = new ParameterizedTypeReference<ServiceMeta>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * get peripheral metadata
     * Get peripheral metadata for the type Id specified with or without details.  &#39;details&#x3D;false&#39; returns the IdPath and the Tasks for the peripheral. These provide information on how to extract the peripheral&#39;s instance Id and the operations to extract the data streams. If &#39;details&#x3D;true&#39;, the peripheral&#39;s service metadata is also returned
     * <p><b>200</b> - Got peripheral metadata
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Unknown peripheral type identifier
     * @param pType Peripheral type identifier
     * @param details details
     * @return PeripheralMeta
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public PeripheralMeta getDeviceMetadataUsingGET(String pType, Boolean details) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling getDeviceMetadataUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "details", details));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<PeripheralMeta> returnType = new ParameterizedTypeReference<PeripheralMeta>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * get peripheral types
     * Retrieve peripheral types - returns a list of regular expressions to match with the peripheral name
     * <p><b>200</b> - Allowed Peripheral Types
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param status status
     * @return List&lt;PeripheralTypeMeta&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<PeripheralTypeMeta> getKnownPeripheralTypesUsingGET(String status) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'status' is set
        if (status == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'status' when calling getKnownPeripheralTypesUsingGET");
        }
        
        String path = UriComponentsBuilder.fromPath("/api/1/meta/pTypes").build().toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "status", status));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<PeripheralTypeMeta>> returnType = new ParameterizedTypeReference<List<PeripheralTypeMeta>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * get service metadata
     * Get service metadata 
     * <p><b>200</b> - Get service metadata
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Unknown peripheral type identifier
     * @param pType Peripheral type identifier
     * @param sId service identifier
     * @param details details
     * @return ServiceMeta
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ServiceMeta getServiceMetadataUsingGET(String pType, String sId, String details) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling getServiceMetadataUsingGET");
        }
        
        // verify the required parameter 'sId' is set
        if (sId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'sId' when calling getServiceMetadataUsingGET");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        uriVariables.put("sId", sId);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}/service/{sId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "details", details));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<ServiceMeta> returnType = new ParameterizedTypeReference<ServiceMeta>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * add/update peripheral metadata
     * Similar to the POST operation - Updates metadata for a peripheral
     * <p><b>200</b> - Updated Metadata
     * <p><b>201</b> - Created
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param pType Peripheral type identifier
     * @param deviceMeta Peripheral metadata
     * @return PeripheralMeta
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public PeripheralMeta updateDeviceMetadataUsingPUT(String pType, PeripheralMeta deviceMeta) throws RestClientException {
        Object postBody = deviceMeta;
        
        // verify the required parameter 'pType' is set
        if (pType == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'pType' when calling updateDeviceMetadataUsingPUT");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("pType", pType);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/peripheral/{pType}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<PeripheralMeta> returnType = new ParameterizedTypeReference<PeripheralMeta>() {};
        return apiClient.invokeAPI(path, HttpMethod.PUT, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * upload image for peripheral type(cc/tag)
     * upload image of peripheral type(cc/tag). Returns Status OK if successful.
     * <p><b>200</b> - Updated Type Info
     * <p><b>201</b> - Added Type Info
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param ptypeID Peripheral type regular expression
     * @param file File Upload Key
     * @param deviceType tag/cc
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String updatePeripheralTypeImageUsingPUT(String ptypeID, File file, String deviceType) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'ptypeID' is set
        if (ptypeID == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'ptypeID' when calling updatePeripheralTypeImageUsingPUT");
        }
        
        // verify the required parameter 'file' is set
        if (file == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'file' when calling updatePeripheralTypeImageUsingPUT");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("ptypeID", ptypeID);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/pTypes/image/{ptypeID}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "deviceType", deviceType));
        
        if (file != null)
            formParams.add("file", new FileSystemResource(file));

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "multipart/form-data"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.PUT, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * update peripheral type
     * Update peripheral type. Returns Status CREATED if successful.
     * <p><b>200</b> - Updated Type Info
     * <p><b>201</b> - Added Type Info
     * <p><b>400</b> - Invalid data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param typeRegEx Peripheral type regular expression
     * @param pType I/O object used to describe peripheral types
     * @return String
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public String updatePeripheralTypeUsingPUT(String typeRegEx, PeripheralTypeMeta pType) throws RestClientException {
        Object postBody = pType;
        
        // verify the required parameter 'typeRegEx' is set
        if (typeRegEx == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'typeRegEx' when calling updatePeripheralTypeUsingPUT");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("typeRegEx", typeRegEx);
        String path = UriComponentsBuilder.fromPath("/api/1/meta/pTypes/{typeRegEx}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "*/*"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<String> returnType = new ParameterizedTypeReference<String>() {};
        return apiClient.invokeAPI(path, HttpMethod.PUT, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
}
