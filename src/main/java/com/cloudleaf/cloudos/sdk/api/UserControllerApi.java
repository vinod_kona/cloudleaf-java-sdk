package com.cloudleaf.cloudos.sdk.api;

import com.cloudleaf.cloudos.sdk.ApiClient;

import com.cloudleaf.cloudos.sdk.model.AccessToken;
import com.cloudleaf.cloudos.sdk.model.UserCredentials;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")
@Component("com.cloudleaf.cloudos.sdk.api.UserControllerApi")
public class UserControllerApi {
    private ApiClient apiClient;

    public UserControllerApi() {
        this(new ApiClient());
    }

    @Autowired
    public UserControllerApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * login
     * Login a user/app via REST API call with credentials. All API calls have to be authenticated using a *login and secret* as credentials. This call will return a valid  **token** that need be sent as header parameter at the time of REST API invocation
     * <p><b>200</b> - Authenticated.  Token returned
     * <p><b>201</b> - Created
     * <p><b>400</b> - Invalid input data
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>404</b> - Not Found
     * @param creds Credential object
     * @return AccessToken
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public AccessToken loginUsingPOST(UserCredentials creds) throws RestClientException {
        Object postBody = creds;
        
        // verify the required parameter 'creds' is set
        if (creds == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'creds' when calling loginUsingPOST");
        }
        
        String path = UriComponentsBuilder.fromPath("/api/1/user/login").build().toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<AccessToken> returnType = new ParameterizedTypeReference<AccessToken>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
}
