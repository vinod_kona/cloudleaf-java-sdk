/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * NotificationContext
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class NotificationContext {
  @JsonProperty("applicableId")
  private String applicableId = null;

  @JsonProperty("categoryId")
  private String categoryId = null;

  @JsonProperty("deviceId")
  private String deviceId = null;

  @JsonProperty("level")
  private String level = null;

  @JsonProperty("message")
  private String message = null;

  @JsonProperty("notifTime")
  private Long notifTime = null;

  @JsonProperty("notifType")
  private String notifType = null;

  @JsonProperty("tenantId")
  private String tenantId = null;

  @JsonProperty("transport")
  private String transport = null;

  @JsonProperty("userId")
  private String userId = null;

  public NotificationContext applicableId(String applicableId) {
    this.applicableId = applicableId;
    return this;
  }

   /**
   * Get applicableId
   * @return applicableId
  **/
  @ApiModelProperty(value = "")
  public String getApplicableId() {
    return applicableId;
  }

  public void setApplicableId(String applicableId) {
    this.applicableId = applicableId;
  }

  public NotificationContext categoryId(String categoryId) {
    this.categoryId = categoryId;
    return this;
  }

   /**
   * Get categoryId
   * @return categoryId
  **/
  @ApiModelProperty(value = "")
  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public NotificationContext deviceId(String deviceId) {
    this.deviceId = deviceId;
    return this;
  }

   /**
   * Get deviceId
   * @return deviceId
  **/
  @ApiModelProperty(value = "")
  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public NotificationContext level(String level) {
    this.level = level;
    return this;
  }

   /**
   * Get level
   * @return level
  **/
  @ApiModelProperty(value = "")
  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public NotificationContext message(String message) {
    this.message = message;
    return this;
  }

   /**
   * Get message
   * @return message
  **/
  @ApiModelProperty(value = "")
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public NotificationContext notifTime(Long notifTime) {
    this.notifTime = notifTime;
    return this;
  }

   /**
   * Get notifTime
   * @return notifTime
  **/
  @ApiModelProperty(value = "")
  public Long getNotifTime() {
    return notifTime;
  }

  public void setNotifTime(Long notifTime) {
    this.notifTime = notifTime;
  }

  public NotificationContext notifType(String notifType) {
    this.notifType = notifType;
    return this;
  }

   /**
   * Get notifType
   * @return notifType
  **/
  @ApiModelProperty(value = "")
  public String getNotifType() {
    return notifType;
  }

  public void setNotifType(String notifType) {
    this.notifType = notifType;
  }

  public NotificationContext tenantId(String tenantId) {
    this.tenantId = tenantId;
    return this;
  }

   /**
   * Get tenantId
   * @return tenantId
  **/
  @ApiModelProperty(value = "")
  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }

  public NotificationContext transport(String transport) {
    this.transport = transport;
    return this;
  }

   /**
   * Get transport
   * @return transport
  **/
  @ApiModelProperty(value = "")
  public String getTransport() {
    return transport;
  }

  public void setTransport(String transport) {
    this.transport = transport;
  }

  public NotificationContext userId(String userId) {
    this.userId = userId;
    return this;
  }

   /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(value = "")
  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NotificationContext notificationContext = (NotificationContext) o;
    return Objects.equals(this.applicableId, notificationContext.applicableId) &&
        Objects.equals(this.categoryId, notificationContext.categoryId) &&
        Objects.equals(this.deviceId, notificationContext.deviceId) &&
        Objects.equals(this.level, notificationContext.level) &&
        Objects.equals(this.message, notificationContext.message) &&
        Objects.equals(this.notifTime, notificationContext.notifTime) &&
        Objects.equals(this.notifType, notificationContext.notifType) &&
        Objects.equals(this.tenantId, notificationContext.tenantId) &&
        Objects.equals(this.transport, notificationContext.transport) &&
        Objects.equals(this.userId, notificationContext.userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(applicableId, categoryId, deviceId, level, message, notifTime, notifType, tenantId, transport, userId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NotificationContext {\n");
    
    sb.append("    applicableId: ").append(toIndentedString(applicableId)).append("\n");
    sb.append("    categoryId: ").append(toIndentedString(categoryId)).append("\n");
    sb.append("    deviceId: ").append(toIndentedString(deviceId)).append("\n");
    sb.append("    level: ").append(toIndentedString(level)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    notifTime: ").append(toIndentedString(notifTime)).append("\n");
    sb.append("    notifType: ").append(toIndentedString(notifType)).append("\n");
    sb.append("    tenantId: ").append(toIndentedString(tenantId)).append("\n");
    sb.append("    transport: ").append(toIndentedString(transport)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

