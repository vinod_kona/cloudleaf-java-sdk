/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.cloudleaf.cloudos.sdk.model.DescriptorMeta;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Base Metadata Object Type
 */
@ApiModel(description = "Base Metadata Object Type")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class CharacteristicMeta {
  @JsonProperty("configParams")
  private Map<String, String> configParams = null;

  @JsonProperty("converter")
  private String converter = null;

  @JsonProperty("converterParams")
  private Map<String, String> converterParams = null;

  @JsonProperty("descriptors")
  private List<DescriptorMeta> descriptors = null;

  /**
   * Gets or Sets properties
   */
  public enum PropertiesEnum {
    BROADCAST("Broadcast"),
    
    READ("Read"),
    
    WRITEWITHOUTRESPONSE("WriteWithoutResponse"),
    
    WRITE("Write"),
    
    NOTIFY("Notify"),
    
    INDICATE("Indicate"),
    
    SIGNEDWRITE("SignedWrite"),
    
    EXTENDEDPROPERTIES("ExtendedProperties"),
    
    NOTIFYENCRYPTIONREQUIRED("NotifyEncryptionRequired"),
    
    INDICATEENCRYPTIONREQUIRED("IndicateEncryptionRequired"),
    
    WRITABLEAUXILIARIES("WritableAuxiliaries");

    private String value;

    PropertiesEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static PropertiesEnum fromValue(String text) {
      for (PropertiesEnum b : PropertiesEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("properties")
  private List<PropertiesEnum> properties = null;

  @JsonProperty("unitType")
  private String unitType = null;

  @JsonProperty("webUIConfig")
  private Map<String, String> webUIConfig = null;

  @JsonProperty("identifier")
  private String identifier = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("name")
  private String name = null;

  public CharacteristicMeta configParams(Map<String, String> configParams) {
    this.configParams = configParams;
    return this;
  }

  public CharacteristicMeta putConfigParamsItem(String key, String configParamsItem) {
    if (this.configParams == null) {
      this.configParams = new HashMap<String, String>();
    }
    this.configParams.put(key, configParamsItem);
    return this;
  }

   /**
   * Get configParams
   * @return configParams
  **/
  @ApiModelProperty(value = "")
  public Map<String, String> getConfigParams() {
    return configParams;
  }

  public void setConfigParams(Map<String, String> configParams) {
    this.configParams = configParams;
  }

  public CharacteristicMeta converter(String converter) {
    this.converter = converter;
    return this;
  }

   /**
   * Get converter
   * @return converter
  **/
  @ApiModelProperty(value = "")
  public String getConverter() {
    return converter;
  }

  public void setConverter(String converter) {
    this.converter = converter;
  }

  public CharacteristicMeta converterParams(Map<String, String> converterParams) {
    this.converterParams = converterParams;
    return this;
  }

  public CharacteristicMeta putConverterParamsItem(String key, String converterParamsItem) {
    if (this.converterParams == null) {
      this.converterParams = new HashMap<String, String>();
    }
    this.converterParams.put(key, converterParamsItem);
    return this;
  }

   /**
   * Get converterParams
   * @return converterParams
  **/
  @ApiModelProperty(value = "")
  public Map<String, String> getConverterParams() {
    return converterParams;
  }

  public void setConverterParams(Map<String, String> converterParams) {
    this.converterParams = converterParams;
  }

  public CharacteristicMeta descriptors(List<DescriptorMeta> descriptors) {
    this.descriptors = descriptors;
    return this;
  }

  public CharacteristicMeta addDescriptorsItem(DescriptorMeta descriptorsItem) {
    if (this.descriptors == null) {
      this.descriptors = new ArrayList<DescriptorMeta>();
    }
    this.descriptors.add(descriptorsItem);
    return this;
  }

   /**
   * Descriptor list
   * @return descriptors
  **/
  @ApiModelProperty(value = "Descriptor list")
  public List<DescriptorMeta> getDescriptors() {
    return descriptors;
  }

  public void setDescriptors(List<DescriptorMeta> descriptors) {
    this.descriptors = descriptors;
  }

  public CharacteristicMeta properties(List<PropertiesEnum> properties) {
    this.properties = properties;
    return this;
  }

  public CharacteristicMeta addPropertiesItem(PropertiesEnum propertiesItem) {
    if (this.properties == null) {
      this.properties = new ArrayList<PropertiesEnum>();
    }
    this.properties.add(propertiesItem);
    return this;
  }

   /**
   * Actions allowed on this characteristic
   * @return properties
  **/
  @ApiModelProperty(value = "Actions allowed on this characteristic")
  public List<PropertiesEnum> getProperties() {
    return properties;
  }

  public void setProperties(List<PropertiesEnum> properties) {
    this.properties = properties;
  }

  public CharacteristicMeta unitType(String unitType) {
    this.unitType = unitType;
    return this;
  }

   /**
   * Unit type of the characteristic
   * @return unitType
  **/
  @ApiModelProperty(value = "Unit type of the characteristic")
  public String getUnitType() {
    return unitType;
  }

  public void setUnitType(String unitType) {
    this.unitType = unitType;
  }

  public CharacteristicMeta webUIConfig(Map<String, String> webUIConfig) {
    this.webUIConfig = webUIConfig;
    return this;
  }

  public CharacteristicMeta putWebUIConfigItem(String key, String webUIConfigItem) {
    if (this.webUIConfig == null) {
      this.webUIConfig = new HashMap<String, String>();
    }
    this.webUIConfig.put(key, webUIConfigItem);
    return this;
  }

   /**
   * Get webUIConfig
   * @return webUIConfig
  **/
  @ApiModelProperty(value = "")
  public Map<String, String> getWebUIConfig() {
    return webUIConfig;
  }

  public void setWebUIConfig(Map<String, String> webUIConfig) {
    this.webUIConfig = webUIConfig;
  }

  public CharacteristicMeta identifier(String identifier) {
    this.identifier = identifier;
    return this;
  }

   /**
   * identifier
   * @return identifier
  **/
  @ApiModelProperty(required = true, value = "identifier")
  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public CharacteristicMeta type(String type) {
    this.type = type;
    return this;
  }

   /**
   * type
   * @return type
  **/
  @ApiModelProperty(value = "type")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public CharacteristicMeta name(String name) {
    this.name = name;
    return this;
  }

   /**
   * name
   * @return name
  **/
  @ApiModelProperty(value = "name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CharacteristicMeta characteristicMeta = (CharacteristicMeta) o;
    return Objects.equals(this.configParams, characteristicMeta.configParams) &&
        Objects.equals(this.converter, characteristicMeta.converter) &&
        Objects.equals(this.converterParams, characteristicMeta.converterParams) &&
        Objects.equals(this.descriptors, characteristicMeta.descriptors) &&
        Objects.equals(this.properties, characteristicMeta.properties) &&
        Objects.equals(this.unitType, characteristicMeta.unitType) &&
        Objects.equals(this.webUIConfig, characteristicMeta.webUIConfig) &&
        Objects.equals(this.identifier, characteristicMeta.identifier) &&
        Objects.equals(this.type, characteristicMeta.type) &&
        Objects.equals(this.name, characteristicMeta.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(configParams, converter, converterParams, descriptors, properties, unitType, webUIConfig, identifier, type, name);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CharacteristicMeta {\n");
    
    sb.append("    configParams: ").append(toIndentedString(configParams)).append("\n");
    sb.append("    converter: ").append(toIndentedString(converter)).append("\n");
    sb.append("    converterParams: ").append(toIndentedString(converterParams)).append("\n");
    sb.append("    descriptors: ").append(toIndentedString(descriptors)).append("\n");
    sb.append("    properties: ").append(toIndentedString(properties)).append("\n");
    sb.append("    unitType: ").append(toIndentedString(unitType)).append("\n");
    sb.append("    webUIConfig: ").append(toIndentedString(webUIConfig)).append("\n");
    sb.append("    identifier: ").append(toIndentedString(identifier)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

