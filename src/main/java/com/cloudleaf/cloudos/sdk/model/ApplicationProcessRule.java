/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.cloudleaf.cloudos.sdk.model.ProcessRuleStage;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * ApplicationProcessRule
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class ApplicationProcessRule {
  @JsonProperty("app_id")
  private String appId = null;

  @JsonProperty("stages")
  private List<ProcessRuleStage> stages = null;

  public ApplicationProcessRule appId(String appId) {
    this.appId = appId;
    return this;
  }

   /**
   * Get appId
   * @return appId
  **/
  @ApiModelProperty(value = "")
  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public ApplicationProcessRule stages(List<ProcessRuleStage> stages) {
    this.stages = stages;
    return this;
  }

  public ApplicationProcessRule addStagesItem(ProcessRuleStage stagesItem) {
    if (this.stages == null) {
      this.stages = new ArrayList<ProcessRuleStage>();
    }
    this.stages.add(stagesItem);
    return this;
  }

   /**
   * Get stages
   * @return stages
  **/
  @ApiModelProperty(value = "")
  public List<ProcessRuleStage> getStages() {
    return stages;
  }

  public void setStages(List<ProcessRuleStage> stages) {
    this.stages = stages;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApplicationProcessRule applicationProcessRule = (ApplicationProcessRule) o;
    return Objects.equals(this.appId, applicationProcessRule.appId) &&
        Objects.equals(this.stages, applicationProcessRule.stages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(appId, stages);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApplicationProcessRule {\n");
    
    sb.append("    appId: ").append(toIndentedString(appId)).append("\n");
    sb.append("    stages: ").append(toIndentedString(stages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

