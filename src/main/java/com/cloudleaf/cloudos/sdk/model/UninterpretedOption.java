/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.cloudleaf.cloudos.sdk.model.ByteString;
import com.cloudleaf.cloudos.sdk.model.Descriptor;
import com.cloudleaf.cloudos.sdk.model.NamePart;
import com.cloudleaf.cloudos.sdk.model.NamePartOrBuilder;
import com.cloudleaf.cloudos.sdk.model.ParserUninterpretedOption;
import com.cloudleaf.cloudos.sdk.model.UninterpretedOption;
import com.cloudleaf.cloudos.sdk.model.UnknownFieldSet;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * UninterpretedOption
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class UninterpretedOption {
  @JsonProperty("aggregateValue")
  private String aggregateValue = null;

  @JsonProperty("aggregateValueBytes")
  private ByteString aggregateValueBytes = null;

  @JsonProperty("allFields")
  private Map<String, Object> allFields = null;

  @JsonProperty("defaultInstanceForType")
  private UninterpretedOption defaultInstanceForType = null;

  @JsonProperty("descriptorForType")
  private Descriptor descriptorForType = null;

  @JsonProperty("doubleValue")
  private Double doubleValue = null;

  @JsonProperty("identifierValue")
  private String identifierValue = null;

  @JsonProperty("identifierValueBytes")
  private ByteString identifierValueBytes = null;

  @JsonProperty("initializationErrorString")
  private String initializationErrorString = null;

  @JsonProperty("initialized")
  private Boolean initialized = null;

  @JsonProperty("nameCount")
  private Integer nameCount = null;

  @JsonProperty("nameList")
  private List<NamePart> nameList = null;

  @JsonProperty("nameOrBuilderList")
  private List<NamePartOrBuilder> nameOrBuilderList = null;

  @JsonProperty("negativeIntValue")
  private Long negativeIntValue = null;

  @JsonProperty("parserForType")
  private ParserUninterpretedOption parserForType = null;

  @JsonProperty("positiveIntValue")
  private Long positiveIntValue = null;

  @JsonProperty("serializedSize")
  private Integer serializedSize = null;

  @JsonProperty("stringValue")
  private ByteString stringValue = null;

  @JsonProperty("unknownFields")
  private UnknownFieldSet unknownFields = null;

  public UninterpretedOption aggregateValue(String aggregateValue) {
    this.aggregateValue = aggregateValue;
    return this;
  }

   /**
   * Get aggregateValue
   * @return aggregateValue
  **/
  @ApiModelProperty(value = "")
  public String getAggregateValue() {
    return aggregateValue;
  }

  public void setAggregateValue(String aggregateValue) {
    this.aggregateValue = aggregateValue;
  }

  public UninterpretedOption aggregateValueBytes(ByteString aggregateValueBytes) {
    this.aggregateValueBytes = aggregateValueBytes;
    return this;
  }

   /**
   * Get aggregateValueBytes
   * @return aggregateValueBytes
  **/
  @ApiModelProperty(value = "")
  public ByteString getAggregateValueBytes() {
    return aggregateValueBytes;
  }

  public void setAggregateValueBytes(ByteString aggregateValueBytes) {
    this.aggregateValueBytes = aggregateValueBytes;
  }

  public UninterpretedOption allFields(Map<String, Object> allFields) {
    this.allFields = allFields;
    return this;
  }

  public UninterpretedOption putAllFieldsItem(String key, Object allFieldsItem) {
    if (this.allFields == null) {
      this.allFields = new HashMap<String, Object>();
    }
    this.allFields.put(key, allFieldsItem);
    return this;
  }

   /**
   * Get allFields
   * @return allFields
  **/
  @ApiModelProperty(value = "")
  public Map<String, Object> getAllFields() {
    return allFields;
  }

  public void setAllFields(Map<String, Object> allFields) {
    this.allFields = allFields;
  }

  public UninterpretedOption defaultInstanceForType(UninterpretedOption defaultInstanceForType) {
    this.defaultInstanceForType = defaultInstanceForType;
    return this;
  }

   /**
   * Get defaultInstanceForType
   * @return defaultInstanceForType
  **/
  @ApiModelProperty(value = "")
  public UninterpretedOption getDefaultInstanceForType() {
    return defaultInstanceForType;
  }

  public void setDefaultInstanceForType(UninterpretedOption defaultInstanceForType) {
    this.defaultInstanceForType = defaultInstanceForType;
  }

  public UninterpretedOption descriptorForType(Descriptor descriptorForType) {
    this.descriptorForType = descriptorForType;
    return this;
  }

   /**
   * Get descriptorForType
   * @return descriptorForType
  **/
  @ApiModelProperty(value = "")
  public Descriptor getDescriptorForType() {
    return descriptorForType;
  }

  public void setDescriptorForType(Descriptor descriptorForType) {
    this.descriptorForType = descriptorForType;
  }

  public UninterpretedOption doubleValue(Double doubleValue) {
    this.doubleValue = doubleValue;
    return this;
  }

   /**
   * Get doubleValue
   * @return doubleValue
  **/
  @ApiModelProperty(value = "")
  public Double getDoubleValue() {
    return doubleValue;
  }

  public void setDoubleValue(Double doubleValue) {
    this.doubleValue = doubleValue;
  }

  public UninterpretedOption identifierValue(String identifierValue) {
    this.identifierValue = identifierValue;
    return this;
  }

   /**
   * Get identifierValue
   * @return identifierValue
  **/
  @ApiModelProperty(value = "")
  public String getIdentifierValue() {
    return identifierValue;
  }

  public void setIdentifierValue(String identifierValue) {
    this.identifierValue = identifierValue;
  }

  public UninterpretedOption identifierValueBytes(ByteString identifierValueBytes) {
    this.identifierValueBytes = identifierValueBytes;
    return this;
  }

   /**
   * Get identifierValueBytes
   * @return identifierValueBytes
  **/
  @ApiModelProperty(value = "")
  public ByteString getIdentifierValueBytes() {
    return identifierValueBytes;
  }

  public void setIdentifierValueBytes(ByteString identifierValueBytes) {
    this.identifierValueBytes = identifierValueBytes;
  }

  public UninterpretedOption initializationErrorString(String initializationErrorString) {
    this.initializationErrorString = initializationErrorString;
    return this;
  }

   /**
   * Get initializationErrorString
   * @return initializationErrorString
  **/
  @ApiModelProperty(value = "")
  public String getInitializationErrorString() {
    return initializationErrorString;
  }

  public void setInitializationErrorString(String initializationErrorString) {
    this.initializationErrorString = initializationErrorString;
  }

  public UninterpretedOption initialized(Boolean initialized) {
    this.initialized = initialized;
    return this;
  }

   /**
   * Get initialized
   * @return initialized
  **/
  @ApiModelProperty(value = "")
  public Boolean isInitialized() {
    return initialized;
  }

  public void setInitialized(Boolean initialized) {
    this.initialized = initialized;
  }

  public UninterpretedOption nameCount(Integer nameCount) {
    this.nameCount = nameCount;
    return this;
  }

   /**
   * Get nameCount
   * @return nameCount
  **/
  @ApiModelProperty(value = "")
  public Integer getNameCount() {
    return nameCount;
  }

  public void setNameCount(Integer nameCount) {
    this.nameCount = nameCount;
  }

  public UninterpretedOption nameList(List<NamePart> nameList) {
    this.nameList = nameList;
    return this;
  }

  public UninterpretedOption addNameListItem(NamePart nameListItem) {
    if (this.nameList == null) {
      this.nameList = new ArrayList<NamePart>();
    }
    this.nameList.add(nameListItem);
    return this;
  }

   /**
   * Get nameList
   * @return nameList
  **/
  @ApiModelProperty(value = "")
  public List<NamePart> getNameList() {
    return nameList;
  }

  public void setNameList(List<NamePart> nameList) {
    this.nameList = nameList;
  }

  public UninterpretedOption nameOrBuilderList(List<NamePartOrBuilder> nameOrBuilderList) {
    this.nameOrBuilderList = nameOrBuilderList;
    return this;
  }

  public UninterpretedOption addNameOrBuilderListItem(NamePartOrBuilder nameOrBuilderListItem) {
    if (this.nameOrBuilderList == null) {
      this.nameOrBuilderList = new ArrayList<NamePartOrBuilder>();
    }
    this.nameOrBuilderList.add(nameOrBuilderListItem);
    return this;
  }

   /**
   * Get nameOrBuilderList
   * @return nameOrBuilderList
  **/
  @ApiModelProperty(value = "")
  public List<NamePartOrBuilder> getNameOrBuilderList() {
    return nameOrBuilderList;
  }

  public void setNameOrBuilderList(List<NamePartOrBuilder> nameOrBuilderList) {
    this.nameOrBuilderList = nameOrBuilderList;
  }

  public UninterpretedOption negativeIntValue(Long negativeIntValue) {
    this.negativeIntValue = negativeIntValue;
    return this;
  }

   /**
   * Get negativeIntValue
   * @return negativeIntValue
  **/
  @ApiModelProperty(value = "")
  public Long getNegativeIntValue() {
    return negativeIntValue;
  }

  public void setNegativeIntValue(Long negativeIntValue) {
    this.negativeIntValue = negativeIntValue;
  }

  public UninterpretedOption parserForType(ParserUninterpretedOption parserForType) {
    this.parserForType = parserForType;
    return this;
  }

   /**
   * Get parserForType
   * @return parserForType
  **/
  @ApiModelProperty(value = "")
  public ParserUninterpretedOption getParserForType() {
    return parserForType;
  }

  public void setParserForType(ParserUninterpretedOption parserForType) {
    this.parserForType = parserForType;
  }

  public UninterpretedOption positiveIntValue(Long positiveIntValue) {
    this.positiveIntValue = positiveIntValue;
    return this;
  }

   /**
   * Get positiveIntValue
   * @return positiveIntValue
  **/
  @ApiModelProperty(value = "")
  public Long getPositiveIntValue() {
    return positiveIntValue;
  }

  public void setPositiveIntValue(Long positiveIntValue) {
    this.positiveIntValue = positiveIntValue;
  }

  public UninterpretedOption serializedSize(Integer serializedSize) {
    this.serializedSize = serializedSize;
    return this;
  }

   /**
   * Get serializedSize
   * @return serializedSize
  **/
  @ApiModelProperty(value = "")
  public Integer getSerializedSize() {
    return serializedSize;
  }

  public void setSerializedSize(Integer serializedSize) {
    this.serializedSize = serializedSize;
  }

  public UninterpretedOption stringValue(ByteString stringValue) {
    this.stringValue = stringValue;
    return this;
  }

   /**
   * Get stringValue
   * @return stringValue
  **/
  @ApiModelProperty(value = "")
  public ByteString getStringValue() {
    return stringValue;
  }

  public void setStringValue(ByteString stringValue) {
    this.stringValue = stringValue;
  }

  public UninterpretedOption unknownFields(UnknownFieldSet unknownFields) {
    this.unknownFields = unknownFields;
    return this;
  }

   /**
   * Get unknownFields
   * @return unknownFields
  **/
  @ApiModelProperty(value = "")
  public UnknownFieldSet getUnknownFields() {
    return unknownFields;
  }

  public void setUnknownFields(UnknownFieldSet unknownFields) {
    this.unknownFields = unknownFields;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UninterpretedOption uninterpretedOption = (UninterpretedOption) o;
    return Objects.equals(this.aggregateValue, uninterpretedOption.aggregateValue) &&
        Objects.equals(this.aggregateValueBytes, uninterpretedOption.aggregateValueBytes) &&
        Objects.equals(this.allFields, uninterpretedOption.allFields) &&
        Objects.equals(this.defaultInstanceForType, uninterpretedOption.defaultInstanceForType) &&
        Objects.equals(this.descriptorForType, uninterpretedOption.descriptorForType) &&
        Objects.equals(this.doubleValue, uninterpretedOption.doubleValue) &&
        Objects.equals(this.identifierValue, uninterpretedOption.identifierValue) &&
        Objects.equals(this.identifierValueBytes, uninterpretedOption.identifierValueBytes) &&
        Objects.equals(this.initializationErrorString, uninterpretedOption.initializationErrorString) &&
        Objects.equals(this.initialized, uninterpretedOption.initialized) &&
        Objects.equals(this.nameCount, uninterpretedOption.nameCount) &&
        Objects.equals(this.nameList, uninterpretedOption.nameList) &&
        Objects.equals(this.nameOrBuilderList, uninterpretedOption.nameOrBuilderList) &&
        Objects.equals(this.negativeIntValue, uninterpretedOption.negativeIntValue) &&
        Objects.equals(this.parserForType, uninterpretedOption.parserForType) &&
        Objects.equals(this.positiveIntValue, uninterpretedOption.positiveIntValue) &&
        Objects.equals(this.serializedSize, uninterpretedOption.serializedSize) &&
        Objects.equals(this.stringValue, uninterpretedOption.stringValue) &&
        Objects.equals(this.unknownFields, uninterpretedOption.unknownFields);
  }

  @Override
  public int hashCode() {
    return Objects.hash(aggregateValue, aggregateValueBytes, allFields, defaultInstanceForType, descriptorForType, doubleValue, identifierValue, identifierValueBytes, initializationErrorString, initialized, nameCount, nameList, nameOrBuilderList, negativeIntValue, parserForType, positiveIntValue, serializedSize, stringValue, unknownFields);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UninterpretedOption {\n");
    
    sb.append("    aggregateValue: ").append(toIndentedString(aggregateValue)).append("\n");
    sb.append("    aggregateValueBytes: ").append(toIndentedString(aggregateValueBytes)).append("\n");
    sb.append("    allFields: ").append(toIndentedString(allFields)).append("\n");
    sb.append("    defaultInstanceForType: ").append(toIndentedString(defaultInstanceForType)).append("\n");
    sb.append("    descriptorForType: ").append(toIndentedString(descriptorForType)).append("\n");
    sb.append("    doubleValue: ").append(toIndentedString(doubleValue)).append("\n");
    sb.append("    identifierValue: ").append(toIndentedString(identifierValue)).append("\n");
    sb.append("    identifierValueBytes: ").append(toIndentedString(identifierValueBytes)).append("\n");
    sb.append("    initializationErrorString: ").append(toIndentedString(initializationErrorString)).append("\n");
    sb.append("    initialized: ").append(toIndentedString(initialized)).append("\n");
    sb.append("    nameCount: ").append(toIndentedString(nameCount)).append("\n");
    sb.append("    nameList: ").append(toIndentedString(nameList)).append("\n");
    sb.append("    nameOrBuilderList: ").append(toIndentedString(nameOrBuilderList)).append("\n");
    sb.append("    negativeIntValue: ").append(toIndentedString(negativeIntValue)).append("\n");
    sb.append("    parserForType: ").append(toIndentedString(parserForType)).append("\n");
    sb.append("    positiveIntValue: ").append(toIndentedString(positiveIntValue)).append("\n");
    sb.append("    serializedSize: ").append(toIndentedString(serializedSize)).append("\n");
    sb.append("    stringValue: ").append(toIndentedString(stringValue)).append("\n");
    sb.append("    unknownFields: ").append(toIndentedString(unknownFields)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

