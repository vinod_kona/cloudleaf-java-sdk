/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.cloudleaf.cloudos.sdk.model.Command;
import com.cloudleaf.cloudos.sdk.model.CommandOrBuilder;
import com.cloudleaf.cloudos.sdk.model.Descriptor;
import com.cloudleaf.cloudos.sdk.model.ParserTasks;
import com.cloudleaf.cloudos.sdk.model.Tasks;
import com.cloudleaf.cloudos.sdk.model.UnknownFieldSet;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Tasks
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class Tasks {
  @JsonProperty("allFields")
  private Map<String, Object> allFields = null;

  @JsonProperty("commandsCount")
  private Integer commandsCount = null;

  @JsonProperty("commandsList")
  private List<Command> commandsList = null;

  @JsonProperty("commandsOrBuilderList")
  private List<CommandOrBuilder> commandsOrBuilderList = null;

  @JsonProperty("defaultInstanceForType")
  private Tasks defaultInstanceForType = null;

  @JsonProperty("descriptorForType")
  private Descriptor descriptorForType = null;

  @JsonProperty("initializationErrorString")
  private String initializationErrorString = null;

  @JsonProperty("initialized")
  private Boolean initialized = null;

  @JsonProperty("parserForType")
  private ParserTasks parserForType = null;

  @JsonProperty("serializedSize")
  private Integer serializedSize = null;

  @JsonProperty("unknownFields")
  private UnknownFieldSet unknownFields = null;

  public Tasks allFields(Map<String, Object> allFields) {
    this.allFields = allFields;
    return this;
  }

  public Tasks putAllFieldsItem(String key, Object allFieldsItem) {
    if (this.allFields == null) {
      this.allFields = new HashMap<String, Object>();
    }
    this.allFields.put(key, allFieldsItem);
    return this;
  }

   /**
   * Get allFields
   * @return allFields
  **/
  @ApiModelProperty(value = "")
  public Map<String, Object> getAllFields() {
    return allFields;
  }

  public void setAllFields(Map<String, Object> allFields) {
    this.allFields = allFields;
  }

  public Tasks commandsCount(Integer commandsCount) {
    this.commandsCount = commandsCount;
    return this;
  }

   /**
   * Get commandsCount
   * @return commandsCount
  **/
  @ApiModelProperty(value = "")
  public Integer getCommandsCount() {
    return commandsCount;
  }

  public void setCommandsCount(Integer commandsCount) {
    this.commandsCount = commandsCount;
  }

  public Tasks commandsList(List<Command> commandsList) {
    this.commandsList = commandsList;
    return this;
  }

  public Tasks addCommandsListItem(Command commandsListItem) {
    if (this.commandsList == null) {
      this.commandsList = new ArrayList<Command>();
    }
    this.commandsList.add(commandsListItem);
    return this;
  }

   /**
   * Get commandsList
   * @return commandsList
  **/
  @ApiModelProperty(value = "")
  public List<Command> getCommandsList() {
    return commandsList;
  }

  public void setCommandsList(List<Command> commandsList) {
    this.commandsList = commandsList;
  }

  public Tasks commandsOrBuilderList(List<CommandOrBuilder> commandsOrBuilderList) {
    this.commandsOrBuilderList = commandsOrBuilderList;
    return this;
  }

  public Tasks addCommandsOrBuilderListItem(CommandOrBuilder commandsOrBuilderListItem) {
    if (this.commandsOrBuilderList == null) {
      this.commandsOrBuilderList = new ArrayList<CommandOrBuilder>();
    }
    this.commandsOrBuilderList.add(commandsOrBuilderListItem);
    return this;
  }

   /**
   * Get commandsOrBuilderList
   * @return commandsOrBuilderList
  **/
  @ApiModelProperty(value = "")
  public List<CommandOrBuilder> getCommandsOrBuilderList() {
    return commandsOrBuilderList;
  }

  public void setCommandsOrBuilderList(List<CommandOrBuilder> commandsOrBuilderList) {
    this.commandsOrBuilderList = commandsOrBuilderList;
  }

  public Tasks defaultInstanceForType(Tasks defaultInstanceForType) {
    this.defaultInstanceForType = defaultInstanceForType;
    return this;
  }

   /**
   * Get defaultInstanceForType
   * @return defaultInstanceForType
  **/
  @ApiModelProperty(value = "")
  public Tasks getDefaultInstanceForType() {
    return defaultInstanceForType;
  }

  public void setDefaultInstanceForType(Tasks defaultInstanceForType) {
    this.defaultInstanceForType = defaultInstanceForType;
  }

  public Tasks descriptorForType(Descriptor descriptorForType) {
    this.descriptorForType = descriptorForType;
    return this;
  }

   /**
   * Get descriptorForType
   * @return descriptorForType
  **/
  @ApiModelProperty(value = "")
  public Descriptor getDescriptorForType() {
    return descriptorForType;
  }

  public void setDescriptorForType(Descriptor descriptorForType) {
    this.descriptorForType = descriptorForType;
  }

  public Tasks initializationErrorString(String initializationErrorString) {
    this.initializationErrorString = initializationErrorString;
    return this;
  }

   /**
   * Get initializationErrorString
   * @return initializationErrorString
  **/
  @ApiModelProperty(value = "")
  public String getInitializationErrorString() {
    return initializationErrorString;
  }

  public void setInitializationErrorString(String initializationErrorString) {
    this.initializationErrorString = initializationErrorString;
  }

  public Tasks initialized(Boolean initialized) {
    this.initialized = initialized;
    return this;
  }

   /**
   * Get initialized
   * @return initialized
  **/
  @ApiModelProperty(value = "")
  public Boolean isInitialized() {
    return initialized;
  }

  public void setInitialized(Boolean initialized) {
    this.initialized = initialized;
  }

  public Tasks parserForType(ParserTasks parserForType) {
    this.parserForType = parserForType;
    return this;
  }

   /**
   * Get parserForType
   * @return parserForType
  **/
  @ApiModelProperty(value = "")
  public ParserTasks getParserForType() {
    return parserForType;
  }

  public void setParserForType(ParserTasks parserForType) {
    this.parserForType = parserForType;
  }

  public Tasks serializedSize(Integer serializedSize) {
    this.serializedSize = serializedSize;
    return this;
  }

   /**
   * Get serializedSize
   * @return serializedSize
  **/
  @ApiModelProperty(value = "")
  public Integer getSerializedSize() {
    return serializedSize;
  }

  public void setSerializedSize(Integer serializedSize) {
    this.serializedSize = serializedSize;
  }

  public Tasks unknownFields(UnknownFieldSet unknownFields) {
    this.unknownFields = unknownFields;
    return this;
  }

   /**
   * Get unknownFields
   * @return unknownFields
  **/
  @ApiModelProperty(value = "")
  public UnknownFieldSet getUnknownFields() {
    return unknownFields;
  }

  public void setUnknownFields(UnknownFieldSet unknownFields) {
    this.unknownFields = unknownFields;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Tasks tasks = (Tasks) o;
    return Objects.equals(this.allFields, tasks.allFields) &&
        Objects.equals(this.commandsCount, tasks.commandsCount) &&
        Objects.equals(this.commandsList, tasks.commandsList) &&
        Objects.equals(this.commandsOrBuilderList, tasks.commandsOrBuilderList) &&
        Objects.equals(this.defaultInstanceForType, tasks.defaultInstanceForType) &&
        Objects.equals(this.descriptorForType, tasks.descriptorForType) &&
        Objects.equals(this.initializationErrorString, tasks.initializationErrorString) &&
        Objects.equals(this.initialized, tasks.initialized) &&
        Objects.equals(this.parserForType, tasks.parserForType) &&
        Objects.equals(this.serializedSize, tasks.serializedSize) &&
        Objects.equals(this.unknownFields, tasks.unknownFields);
  }

  @Override
  public int hashCode() {
    return Objects.hash(allFields, commandsCount, commandsList, commandsOrBuilderList, defaultInstanceForType, descriptorForType, initializationErrorString, initialized, parserForType, serializedSize, unknownFields);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Tasks {\n");
    
    sb.append("    allFields: ").append(toIndentedString(allFields)).append("\n");
    sb.append("    commandsCount: ").append(toIndentedString(commandsCount)).append("\n");
    sb.append("    commandsList: ").append(toIndentedString(commandsList)).append("\n");
    sb.append("    commandsOrBuilderList: ").append(toIndentedString(commandsOrBuilderList)).append("\n");
    sb.append("    defaultInstanceForType: ").append(toIndentedString(defaultInstanceForType)).append("\n");
    sb.append("    descriptorForType: ").append(toIndentedString(descriptorForType)).append("\n");
    sb.append("    initializationErrorString: ").append(toIndentedString(initializationErrorString)).append("\n");
    sb.append("    initialized: ").append(toIndentedString(initialized)).append("\n");
    sb.append("    parserForType: ").append(toIndentedString(parserForType)).append("\n");
    sb.append("    serializedSize: ").append(toIndentedString(serializedSize)).append("\n");
    sb.append("    unknownFields: ").append(toIndentedString(unknownFields)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

