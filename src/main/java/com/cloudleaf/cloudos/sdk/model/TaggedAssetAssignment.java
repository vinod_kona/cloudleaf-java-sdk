/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * TaggedAssetAssignment
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class TaggedAssetAssignment {
  @JsonProperty("assetExternalId")
  private String assetExternalId = null;

  @JsonProperty("assetId")
  private String assetId = null;

  @JsonProperty("assetName")
  private String assetName = null;

  @JsonProperty("createdBy")
  private String createdBy = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("lastSeen")
  private Date lastSeen = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("tag_id")
  private String tagId = null;

  @JsonProperty("updatedDt")
  private Date updatedDt = null;

  public TaggedAssetAssignment assetExternalId(String assetExternalId) {
    this.assetExternalId = assetExternalId;
    return this;
  }

   /**
   * Get assetExternalId
   * @return assetExternalId
  **/
  @ApiModelProperty(value = "")
  public String getAssetExternalId() {
    return assetExternalId;
  }

  public void setAssetExternalId(String assetExternalId) {
    this.assetExternalId = assetExternalId;
  }

  public TaggedAssetAssignment assetId(String assetId) {
    this.assetId = assetId;
    return this;
  }

   /**
   * Get assetId
   * @return assetId
  **/
  @ApiModelProperty(value = "")
  public String getAssetId() {
    return assetId;
  }

  public void setAssetId(String assetId) {
    this.assetId = assetId;
  }

  public TaggedAssetAssignment assetName(String assetName) {
    this.assetName = assetName;
    return this;
  }

   /**
   * Get assetName
   * @return assetName
  **/
  @ApiModelProperty(value = "")
  public String getAssetName() {
    return assetName;
  }

  public void setAssetName(String assetName) {
    this.assetName = assetName;
  }

  public TaggedAssetAssignment createdBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

   /**
   * Get createdBy
   * @return createdBy
  **/
  @ApiModelProperty(value = "")
  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public TaggedAssetAssignment id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public TaggedAssetAssignment lastSeen(Date lastSeen) {
    this.lastSeen = lastSeen;
    return this;
  }

   /**
   * Get lastSeen
   * @return lastSeen
  **/
  @ApiModelProperty(value = "")
  public Date getLastSeen() {
    return lastSeen;
  }

  public void setLastSeen(Date lastSeen) {
    this.lastSeen = lastSeen;
  }

  public TaggedAssetAssignment status(String status) {
    this.status = status;
    return this;
  }

   /**
   * Get status
   * @return status
  **/
  @ApiModelProperty(value = "")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public TaggedAssetAssignment tagId(String tagId) {
    this.tagId = tagId;
    return this;
  }

   /**
   * Get tagId
   * @return tagId
  **/
  @ApiModelProperty(value = "")
  public String getTagId() {
    return tagId;
  }

  public void setTagId(String tagId) {
    this.tagId = tagId;
  }

  public TaggedAssetAssignment updatedDt(Date updatedDt) {
    this.updatedDt = updatedDt;
    return this;
  }

   /**
   * Get updatedDt
   * @return updatedDt
  **/
  @ApiModelProperty(value = "")
  public Date getUpdatedDt() {
    return updatedDt;
  }

  public void setUpdatedDt(Date updatedDt) {
    this.updatedDt = updatedDt;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TaggedAssetAssignment taggedAssetAssignment = (TaggedAssetAssignment) o;
    return Objects.equals(this.assetExternalId, taggedAssetAssignment.assetExternalId) &&
        Objects.equals(this.assetId, taggedAssetAssignment.assetId) &&
        Objects.equals(this.assetName, taggedAssetAssignment.assetName) &&
        Objects.equals(this.createdBy, taggedAssetAssignment.createdBy) &&
        Objects.equals(this.id, taggedAssetAssignment.id) &&
        Objects.equals(this.lastSeen, taggedAssetAssignment.lastSeen) &&
        Objects.equals(this.status, taggedAssetAssignment.status) &&
        Objects.equals(this.tagId, taggedAssetAssignment.tagId) &&
        Objects.equals(this.updatedDt, taggedAssetAssignment.updatedDt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(assetExternalId, assetId, assetName, createdBy, id, lastSeen, status, tagId, updatedDt);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TaggedAssetAssignment {\n");
    
    sb.append("    assetExternalId: ").append(toIndentedString(assetExternalId)).append("\n");
    sb.append("    assetId: ").append(toIndentedString(assetId)).append("\n");
    sb.append("    assetName: ").append(toIndentedString(assetName)).append("\n");
    sb.append("    createdBy: ").append(toIndentedString(createdBy)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    lastSeen: ").append(toIndentedString(lastSeen)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    tagId: ").append(toIndentedString(tagId)).append("\n");
    sb.append("    updatedDt: ").append(toIndentedString(updatedDt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

