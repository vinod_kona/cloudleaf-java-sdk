/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.cloudleaf.cloudos.sdk.model.Command;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * PeripheralTasks
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class PeripheralTasks {
  @JsonProperty("peripheralId")
  private String peripheralId = null;

  @JsonProperty("tasks")
  private Map<String, List<Command>> tasks = null;

  @JsonProperty("type")
  private String type = null;

  public PeripheralTasks peripheralId(String peripheralId) {
    this.peripheralId = peripheralId;
    return this;
  }

   /**
   * Get peripheralId
   * @return peripheralId
  **/
  @ApiModelProperty(value = "")
  public String getPeripheralId() {
    return peripheralId;
  }

  public void setPeripheralId(String peripheralId) {
    this.peripheralId = peripheralId;
  }

  public PeripheralTasks tasks(Map<String, List<Command>> tasks) {
    this.tasks = tasks;
    return this;
  }

  public PeripheralTasks putTasksItem(String key, List<Command> tasksItem) {
    if (this.tasks == null) {
      this.tasks = new HashMap<String, List<Command>>();
    }
    this.tasks.put(key, tasksItem);
    return this;
  }

   /**
   * Get tasks
   * @return tasks
  **/
  @ApiModelProperty(value = "")
  public Map<String, List<Command>> getTasks() {
    return tasks;
  }

  public void setTasks(Map<String, List<Command>> tasks) {
    this.tasks = tasks;
  }

  public PeripheralTasks type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PeripheralTasks peripheralTasks = (PeripheralTasks) o;
    return Objects.equals(this.peripheralId, peripheralTasks.peripheralId) &&
        Objects.equals(this.tasks, peripheralTasks.tasks) &&
        Objects.equals(this.type, peripheralTasks.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(peripheralId, tasks, type);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PeripheralTasks {\n");
    
    sb.append("    peripheralId: ").append(toIndentedString(peripheralId)).append("\n");
    sb.append("    tasks: ").append(toIndentedString(tasks)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

