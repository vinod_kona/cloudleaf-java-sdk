/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.cloudleaf.cloudos.sdk.model.Command;
import com.cloudleaf.cloudos.sdk.model.Tasks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Peripheral Instance Information
 */
@ApiModel(description = "Peripheral Instance Information")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class Peripheral {
  @JsonProperty("config")
  private Tasks config = null;

  @JsonProperty("leafId")
  private String leafId = null;

  @JsonProperty("peripheralId")
  private String peripheralId = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("deviceClass")
  private String deviceClass = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("receiverId")
  private String receiverId = null;

  @JsonProperty("data")
  private Map<String, String> data = null;

  /**
   * Status
   */
  public enum StatusEnum {
    UNPROVISIONED("UNPROVISIONED"),
    
    PROVISIONED("PROVISIONED"),
    
    DISABLED("DISABLED");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("status")
  private StatusEnum status = null;

  @JsonProperty("lastUpdate")
  private Date lastUpdate = null;

  @JsonProperty("tasks")
  private Map<String, List<Command>> tasks = null;

  @JsonProperty("tenantId")
  private String tenantId = null;

  @JsonProperty("physicalId")
  private String physicalId = null;

  @JsonProperty("tags")
  private List<String> tags = null;

  @JsonProperty("createdAt")
  private Date createdAt = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("categoryId")
  private String categoryId = null;

  @JsonProperty("assetId")
  private String assetId = null;

  public Peripheral config(Tasks config) {
    this.config = config;
    return this;
  }

   /**
   * Get config
   * @return config
  **/
  @ApiModelProperty(value = "")
  public Tasks getConfig() {
    return config;
  }

  public void setConfig(Tasks config) {
    this.config = config;
  }

  public Peripheral leafId(String leafId) {
    this.leafId = leafId;
    return this;
  }

   /**
   * Get leafId
   * @return leafId
  **/
  @ApiModelProperty(value = "")
  public String getLeafId() {
    return leafId;
  }

  public void setLeafId(String leafId) {
    this.leafId = leafId;
  }

  public Peripheral peripheralId(String peripheralId) {
    this.peripheralId = peripheralId;
    return this;
  }

   /**
   * Peripheral Id
   * @return peripheralId
  **/
  @ApiModelProperty(required = true, value = "Peripheral Id")
  public String getPeripheralId() {
    return peripheralId;
  }

  public void setPeripheralId(String peripheralId) {
    this.peripheralId = peripheralId;
  }

  public Peripheral type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Peripheral type
   * @return type
  **/
  @ApiModelProperty(required = true, value = "Peripheral type")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Peripheral deviceClass(String deviceClass) {
    this.deviceClass = deviceClass;
    return this;
  }

   /**
   * Peripheral Device Class
   * @return deviceClass
  **/
  @ApiModelProperty(value = "Peripheral Device Class")
  public String getDeviceClass() {
    return deviceClass;
  }

  public void setDeviceClass(String deviceClass) {
    this.deviceClass = deviceClass;
  }

  public Peripheral name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Name
   * @return name
  **/
  @ApiModelProperty(value = "Name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Peripheral receiverId(String receiverId) {
    this.receiverId = receiverId;
    return this;
  }

   /**
   * Leaf Reader Id
   * @return receiverId
  **/
  @ApiModelProperty(required = true, value = "Leaf Reader Id")
  public String getReceiverId() {
    return receiverId;
  }

  public void setReceiverId(String receiverId) {
    this.receiverId = receiverId;
  }

  public Peripheral data(Map<String, String> data) {
    this.data = data;
    return this;
  }

  public Peripheral putDataItem(String key, String dataItem) {
    if (this.data == null) {
      this.data = new HashMap<String, String>();
    }
    this.data.put(key, dataItem);
    return this;
  }

   /**
   * Relevant peripheral data at registration time
   * @return data
  **/
  @ApiModelProperty(value = "Relevant peripheral data at registration time")
  public Map<String, String> getData() {
    return data;
  }

  public void setData(Map<String, String> data) {
    this.data = data;
  }

  public Peripheral status(StatusEnum status) {
    this.status = status;
    return this;
  }

   /**
   * Status
   * @return status
  **/
  @ApiModelProperty(value = "Status")
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Peripheral lastUpdate(Date lastUpdate) {
    this.lastUpdate = lastUpdate;
    return this;
  }

   /**
   * Time of last data update from this peripheral
   * @return lastUpdate
  **/
  @ApiModelProperty(value = "Time of last data update from this peripheral")
  public Date getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Date lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public Peripheral tasks(Map<String, List<Command>> tasks) {
    this.tasks = tasks;
    return this;
  }

  public Peripheral putTasksItem(String key, List<Command> tasksItem) {
    if (this.tasks == null) {
      this.tasks = new HashMap<String, List<Command>>();
    }
    this.tasks.put(key, tasksItem);
    return this;
  }

   /**
   * Map of tasks by service id
   * @return tasks
  **/
  @ApiModelProperty(value = "Map of tasks by service id")
  public Map<String, List<Command>> getTasks() {
    return tasks;
  }

  public void setTasks(Map<String, List<Command>> tasks) {
    this.tasks = tasks;
  }

  public Peripheral tenantId(String tenantId) {
    this.tenantId = tenantId;
    return this;
  }

   /**
   * Owning tenant
   * @return tenantId
  **/
  @ApiModelProperty(value = "Owning tenant")
  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }

  public Peripheral physicalId(String physicalId) {
    this.physicalId = physicalId;
    return this;
  }

   /**
   * Physical Id, e.g., the MAC Address
   * @return physicalId
  **/
  @ApiModelProperty(value = "Physical Id, e.g., the MAC Address")
  public String getPhysicalId() {
    return physicalId;
  }

  public void setPhysicalId(String physicalId) {
    this.physicalId = physicalId;
  }

  public Peripheral tags(List<String> tags) {
    this.tags = tags;
    return this;
  }

  public Peripheral addTagsItem(String tagsItem) {
    if (this.tags == null) {
      this.tags = new ArrayList<String>();
    }
    this.tags.add(tagsItem);
    return this;
  }

   /**
   * Tags associated with this peripheral
   * @return tags
  **/
  @ApiModelProperty(value = "Tags associated with this peripheral")
  public List<String> getTags() {
    return tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  public Peripheral createdAt(Date createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Peripheral provisioning date
   * @return createdAt
  **/
  @ApiModelProperty(value = "Peripheral provisioning date")
  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Peripheral description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Description
   * @return description
  **/
  @ApiModelProperty(value = "Description")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Peripheral categoryId(String categoryId) {
    this.categoryId = categoryId;
    return this;
  }

   /**
   * category Id
   * @return categoryId
  **/
  @ApiModelProperty(value = "category Id")
  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public Peripheral assetId(String assetId) {
    this.assetId = assetId;
    return this;
  }

   /**
   * asset Id
   * @return assetId
  **/
  @ApiModelProperty(value = "asset Id")
  public String getAssetId() {
    return assetId;
  }

  public void setAssetId(String assetId) {
    this.assetId = assetId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Peripheral peripheral = (Peripheral) o;
    return Objects.equals(this.config, peripheral.config) &&
        Objects.equals(this.leafId, peripheral.leafId) &&
        Objects.equals(this.peripheralId, peripheral.peripheralId) &&
        Objects.equals(this.type, peripheral.type) &&
        Objects.equals(this.deviceClass, peripheral.deviceClass) &&
        Objects.equals(this.name, peripheral.name) &&
        Objects.equals(this.receiverId, peripheral.receiverId) &&
        Objects.equals(this.data, peripheral.data) &&
        Objects.equals(this.status, peripheral.status) &&
        Objects.equals(this.lastUpdate, peripheral.lastUpdate) &&
        Objects.equals(this.tasks, peripheral.tasks) &&
        Objects.equals(this.tenantId, peripheral.tenantId) &&
        Objects.equals(this.physicalId, peripheral.physicalId) &&
        Objects.equals(this.tags, peripheral.tags) &&
        Objects.equals(this.createdAt, peripheral.createdAt) &&
        Objects.equals(this.description, peripheral.description) &&
        Objects.equals(this.categoryId, peripheral.categoryId) &&
        Objects.equals(this.assetId, peripheral.assetId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(config, leafId, peripheralId, type, deviceClass, name, receiverId, data, status, lastUpdate, tasks, tenantId, physicalId, tags, createdAt, description, categoryId, assetId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Peripheral {\n");
    
    sb.append("    config: ").append(toIndentedString(config)).append("\n");
    sb.append("    leafId: ").append(toIndentedString(leafId)).append("\n");
    sb.append("    peripheralId: ").append(toIndentedString(peripheralId)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    deviceClass: ").append(toIndentedString(deviceClass)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    receiverId: ").append(toIndentedString(receiverId)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    lastUpdate: ").append(toIndentedString(lastUpdate)).append("\n");
    sb.append("    tasks: ").append(toIndentedString(tasks)).append("\n");
    sb.append("    tenantId: ").append(toIndentedString(tenantId)).append("\n");
    sb.append("    physicalId: ").append(toIndentedString(physicalId)).append("\n");
    sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    categoryId: ").append(toIndentedString(categoryId)).append("\n");
    sb.append("    assetId: ").append(toIndentedString(assetId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

