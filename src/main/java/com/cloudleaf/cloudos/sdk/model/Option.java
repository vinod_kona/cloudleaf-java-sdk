/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.cloudleaf.cloudos.sdk.model.Descriptor;
import com.cloudleaf.cloudos.sdk.model.Option;
import com.cloudleaf.cloudos.sdk.model.ParserOption;
import com.cloudleaf.cloudos.sdk.model.UnknownFieldSet;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Option
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class Option {
  @JsonProperty("allFields")
  private Map<String, Object> allFields = null;

  @JsonProperty("defaultInstanceForType")
  private Option defaultInstanceForType = null;

  @JsonProperty("descriptorForType")
  private Descriptor descriptorForType = null;

  @JsonProperty("initializationErrorString")
  private String initializationErrorString = null;

  @JsonProperty("initialized")
  private Boolean initialized = null;

  /**
   * Gets or Sets optionType
   */
  public enum OptionTypeEnum {
    CONNECTION_INTERVAL("CONNECTION_INTERVAL"),
    
    NOTIFY_TIME_IN_MILLIS("NOTIFY_TIME_IN_MILLIS"),
    
    NOTIFY_TIME_OUT_IN_MILLIS("NOTIFY_TIME_OUT_IN_MILLIS"),
    
    PRIORITY("PRIORITY");

    private String value;

    OptionTypeEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static OptionTypeEnum fromValue(String text) {
      for (OptionTypeEnum b : OptionTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("optionType")
  private OptionTypeEnum optionType = null;

  @JsonProperty("parserForType")
  private ParserOption parserForType = null;

  @JsonProperty("serializedSize")
  private Integer serializedSize = null;

  @JsonProperty("unknownFields")
  private UnknownFieldSet unknownFields = null;

  @JsonProperty("val")
  private Long val = null;

  public Option allFields(Map<String, Object> allFields) {
    this.allFields = allFields;
    return this;
  }

  public Option putAllFieldsItem(String key, Object allFieldsItem) {
    if (this.allFields == null) {
      this.allFields = new HashMap<String, Object>();
    }
    this.allFields.put(key, allFieldsItem);
    return this;
  }

   /**
   * Get allFields
   * @return allFields
  **/
  @ApiModelProperty(value = "")
  public Map<String, Object> getAllFields() {
    return allFields;
  }

  public void setAllFields(Map<String, Object> allFields) {
    this.allFields = allFields;
  }

  public Option defaultInstanceForType(Option defaultInstanceForType) {
    this.defaultInstanceForType = defaultInstanceForType;
    return this;
  }

   /**
   * Get defaultInstanceForType
   * @return defaultInstanceForType
  **/
  @ApiModelProperty(value = "")
  public Option getDefaultInstanceForType() {
    return defaultInstanceForType;
  }

  public void setDefaultInstanceForType(Option defaultInstanceForType) {
    this.defaultInstanceForType = defaultInstanceForType;
  }

  public Option descriptorForType(Descriptor descriptorForType) {
    this.descriptorForType = descriptorForType;
    return this;
  }

   /**
   * Get descriptorForType
   * @return descriptorForType
  **/
  @ApiModelProperty(value = "")
  public Descriptor getDescriptorForType() {
    return descriptorForType;
  }

  public void setDescriptorForType(Descriptor descriptorForType) {
    this.descriptorForType = descriptorForType;
  }

  public Option initializationErrorString(String initializationErrorString) {
    this.initializationErrorString = initializationErrorString;
    return this;
  }

   /**
   * Get initializationErrorString
   * @return initializationErrorString
  **/
  @ApiModelProperty(value = "")
  public String getInitializationErrorString() {
    return initializationErrorString;
  }

  public void setInitializationErrorString(String initializationErrorString) {
    this.initializationErrorString = initializationErrorString;
  }

  public Option initialized(Boolean initialized) {
    this.initialized = initialized;
    return this;
  }

   /**
   * Get initialized
   * @return initialized
  **/
  @ApiModelProperty(value = "")
  public Boolean isInitialized() {
    return initialized;
  }

  public void setInitialized(Boolean initialized) {
    this.initialized = initialized;
  }

  public Option optionType(OptionTypeEnum optionType) {
    this.optionType = optionType;
    return this;
  }

   /**
   * Get optionType
   * @return optionType
  **/
  @ApiModelProperty(value = "")
  public OptionTypeEnum getOptionType() {
    return optionType;
  }

  public void setOptionType(OptionTypeEnum optionType) {
    this.optionType = optionType;
  }

  public Option parserForType(ParserOption parserForType) {
    this.parserForType = parserForType;
    return this;
  }

   /**
   * Get parserForType
   * @return parserForType
  **/
  @ApiModelProperty(value = "")
  public ParserOption getParserForType() {
    return parserForType;
  }

  public void setParserForType(ParserOption parserForType) {
    this.parserForType = parserForType;
  }

  public Option serializedSize(Integer serializedSize) {
    this.serializedSize = serializedSize;
    return this;
  }

   /**
   * Get serializedSize
   * @return serializedSize
  **/
  @ApiModelProperty(value = "")
  public Integer getSerializedSize() {
    return serializedSize;
  }

  public void setSerializedSize(Integer serializedSize) {
    this.serializedSize = serializedSize;
  }

  public Option unknownFields(UnknownFieldSet unknownFields) {
    this.unknownFields = unknownFields;
    return this;
  }

   /**
   * Get unknownFields
   * @return unknownFields
  **/
  @ApiModelProperty(value = "")
  public UnknownFieldSet getUnknownFields() {
    return unknownFields;
  }

  public void setUnknownFields(UnknownFieldSet unknownFields) {
    this.unknownFields = unknownFields;
  }

  public Option val(Long val) {
    this.val = val;
    return this;
  }

   /**
   * Get val
   * @return val
  **/
  @ApiModelProperty(value = "")
  public Long getVal() {
    return val;
  }

  public void setVal(Long val) {
    this.val = val;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Option option = (Option) o;
    return Objects.equals(this.allFields, option.allFields) &&
        Objects.equals(this.defaultInstanceForType, option.defaultInstanceForType) &&
        Objects.equals(this.descriptorForType, option.descriptorForType) &&
        Objects.equals(this.initializationErrorString, option.initializationErrorString) &&
        Objects.equals(this.initialized, option.initialized) &&
        Objects.equals(this.optionType, option.optionType) &&
        Objects.equals(this.parserForType, option.parserForType) &&
        Objects.equals(this.serializedSize, option.serializedSize) &&
        Objects.equals(this.unknownFields, option.unknownFields) &&
        Objects.equals(this.val, option.val);
  }

  @Override
  public int hashCode() {
    return Objects.hash(allFields, defaultInstanceForType, descriptorForType, initializationErrorString, initialized, optionType, parserForType, serializedSize, unknownFields, val);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Option {\n");
    
    sb.append("    allFields: ").append(toIndentedString(allFields)).append("\n");
    sb.append("    defaultInstanceForType: ").append(toIndentedString(defaultInstanceForType)).append("\n");
    sb.append("    descriptorForType: ").append(toIndentedString(descriptorForType)).append("\n");
    sb.append("    initializationErrorString: ").append(toIndentedString(initializationErrorString)).append("\n");
    sb.append("    initialized: ").append(toIndentedString(initialized)).append("\n");
    sb.append("    optionType: ").append(toIndentedString(optionType)).append("\n");
    sb.append("    parserForType: ").append(toIndentedString(parserForType)).append("\n");
    sb.append("    serializedSize: ").append(toIndentedString(serializedSize)).append("\n");
    sb.append("    unknownFields: ").append(toIndentedString(unknownFields)).append("\n");
    sb.append("    val: ").append(toIndentedString(val)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

