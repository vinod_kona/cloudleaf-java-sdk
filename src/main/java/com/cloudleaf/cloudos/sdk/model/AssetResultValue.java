/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * AssetResultValue
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class AssetResultValue {
  @JsonProperty("areaName")
  private Object areaName = null;

  @JsonProperty("cId")
  private String cId = null;

  @JsonProperty("receiverId")
  private Object receiverId = null;

  @JsonProperty("time")
  private Date time = null;

  @JsonProperty("value")
  private Object value = null;

  public AssetResultValue areaName(Object areaName) {
    this.areaName = areaName;
    return this;
  }

   /**
   * Get areaName
   * @return areaName
  **/
  @ApiModelProperty(value = "")
  public Object getAreaName() {
    return areaName;
  }

  public void setAreaName(Object areaName) {
    this.areaName = areaName;
  }

  public AssetResultValue cId(String cId) {
    this.cId = cId;
    return this;
  }

   /**
   * Get cId
   * @return cId
  **/
  @ApiModelProperty(value = "")
  public String getCId() {
    return cId;
  }

  public void setCId(String cId) {
    this.cId = cId;
  }

  public AssetResultValue receiverId(Object receiverId) {
    this.receiverId = receiverId;
    return this;
  }

   /**
   * Get receiverId
   * @return receiverId
  **/
  @ApiModelProperty(value = "")
  public Object getReceiverId() {
    return receiverId;
  }

  public void setReceiverId(Object receiverId) {
    this.receiverId = receiverId;
  }

  public AssetResultValue time(Date time) {
    this.time = time;
    return this;
  }

   /**
   * Get time
   * @return time
  **/
  @ApiModelProperty(value = "")
  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  public AssetResultValue value(Object value) {
    this.value = value;
    return this;
  }

   /**
   * Get value
   * @return value
  **/
  @ApiModelProperty(value = "")
  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AssetResultValue assetResultValue = (AssetResultValue) o;
    return Objects.equals(this.areaName, assetResultValue.areaName) &&
        Objects.equals(this.cId, assetResultValue.cId) &&
        Objects.equals(this.receiverId, assetResultValue.receiverId) &&
        Objects.equals(this.time, assetResultValue.time) &&
        Objects.equals(this.value, assetResultValue.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(areaName, cId, receiverId, time, value);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AssetResultValue {\n");
    
    sb.append("    areaName: ").append(toIndentedString(areaName)).append("\n");
    sb.append("    cId: ").append(toIndentedString(cId)).append("\n");
    sb.append("    receiverId: ").append(toIndentedString(receiverId)).append("\n");
    sb.append("    time: ").append(toIndentedString(time)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

