/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.model;

import java.util.Objects;
import java.util.Arrays;
import com.cloudleaf.cloudos.sdk.model.MessageLite;
import com.cloudleaf.cloudos.sdk.model.ParserMessageLite;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * MessageLite
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-14T14:41:29.164+05:30")

@JsonInclude(Include.NON_NULL)
public class MessageLite {
  @JsonProperty("defaultInstanceForType")
  private MessageLite defaultInstanceForType = null;

  @JsonProperty("initialized")
  private Boolean initialized = null;

  @JsonProperty("parserForType")
  private ParserMessageLite parserForType = null;

  @JsonProperty("serializedSize")
  private Integer serializedSize = null;

  public MessageLite defaultInstanceForType(MessageLite defaultInstanceForType) {
    this.defaultInstanceForType = defaultInstanceForType;
    return this;
  }

   /**
   * Get defaultInstanceForType
   * @return defaultInstanceForType
  **/
  @ApiModelProperty(value = "")
  public MessageLite getDefaultInstanceForType() {
    return defaultInstanceForType;
  }

  public void setDefaultInstanceForType(MessageLite defaultInstanceForType) {
    this.defaultInstanceForType = defaultInstanceForType;
  }

  public MessageLite initialized(Boolean initialized) {
    this.initialized = initialized;
    return this;
  }

   /**
   * Get initialized
   * @return initialized
  **/
  @ApiModelProperty(value = "")
  public Boolean isInitialized() {
    return initialized;
  }

  public void setInitialized(Boolean initialized) {
    this.initialized = initialized;
  }

  public MessageLite parserForType(ParserMessageLite parserForType) {
    this.parserForType = parserForType;
    return this;
  }

   /**
   * Get parserForType
   * @return parserForType
  **/
  @ApiModelProperty(value = "")
  public ParserMessageLite getParserForType() {
    return parserForType;
  }

  public void setParserForType(ParserMessageLite parserForType) {
    this.parserForType = parserForType;
  }

  public MessageLite serializedSize(Integer serializedSize) {
    this.serializedSize = serializedSize;
    return this;
  }

   /**
   * Get serializedSize
   * @return serializedSize
  **/
  @ApiModelProperty(value = "")
  public Integer getSerializedSize() {
    return serializedSize;
  }

  public void setSerializedSize(Integer serializedSize) {
    this.serializedSize = serializedSize;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MessageLite messageLite = (MessageLite) o;
    return Objects.equals(this.defaultInstanceForType, messageLite.defaultInstanceForType) &&
        Objects.equals(this.initialized, messageLite.initialized) &&
        Objects.equals(this.parserForType, messageLite.parserForType) &&
        Objects.equals(this.serializedSize, messageLite.serializedSize);
  }

  @Override
  public int hashCode() {
    return Objects.hash(defaultInstanceForType, initialized, parserForType, serializedSize);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MessageLite {\n");
    
    sb.append("    defaultInstanceForType: ").append(toIndentedString(defaultInstanceForType)).append("\n");
    sb.append("    initialized: ").append(toIndentedString(initialized)).append("\n");
    sb.append("    parserForType: ").append(toIndentedString(parserForType)).append("\n");
    sb.append("    serializedSize: ").append(toIndentedString(serializedSize)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

