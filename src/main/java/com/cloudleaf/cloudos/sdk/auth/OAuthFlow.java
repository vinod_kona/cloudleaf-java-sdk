package com.cloudleaf.cloudos.sdk.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}