/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.api;

import com.cloudleaf.cloudos.sdk.model.Category;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for CustomerControllerApi
 */
@Ignore
public class CustomerControllerApiTest {

    private final CustomerControllerApi api = new CustomerControllerApi();

    
    /**
     * add category
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void addCategoryUsingPOSTTest() {
        String custId = null;
        Category category = null;
        Category response = api.addCategoryUsingPOST(custId, category);

        // TODO: test validations
    }
    
    /**
     * get categories
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getCategoriesUsingGETTest() {
        List<Category> response = api.getCategoriesUsingGET();

        // TODO: test validations
    }
    
    /**
     * remove category
     *
     * Return Status OK if successful
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void removeCategoryUsingDELETETest() {
        String catId = null;
        String response = api.removeCategoryUsingDELETE(catId);

        // TODO: test validations
    }
    
    /**
     * update category
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updateCategoryUsingPUTTest() {
        String catId = null;
        Category category = null;
        Category response = api.updateCategoryUsingPUT(catId, category);

        // TODO: test validations
    }
    
}
