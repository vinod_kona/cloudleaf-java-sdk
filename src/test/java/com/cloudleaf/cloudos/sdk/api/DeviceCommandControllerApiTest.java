/*
 * CloudLeaf API
 * CloudLeaf REST API Description
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cloudleaf.cloudos.sdk.api;

import com.cloudleaf.cloudos.sdk.model.CmdSpecifierRequest;
import com.cloudleaf.cloudos.sdk.model.CommandSpecifier;
import com.cloudleaf.cloudos.sdk.model.ResponseEntity;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for DeviceCommandControllerApi
 */
@Ignore
public class DeviceCommandControllerApiTest {

    private final DeviceCommandControllerApi api = new DeviceCommandControllerApi();

    
    /**
     * Delete commands by device Id 
     *
     * Delete commands by device Id 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteCommandsByDeviceIdUsingDELETETest() {
        String deviceId = null;
        String issuedfrom = null;
        Long fromTime = null;
        ResponseEntity response = api.deleteCommandsByDeviceIdUsingDELETE(deviceId, issuedfrom, fromTime);

        // TODO: test validations
    }
    
    /**
     * executeTagAction
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void executeTagActionUsingPOSTTest() {
        String deviceId = null;
        CmdSpecifierRequest commandSpecifier = null;
        Object response = api.executeTagActionUsingPOST(deviceId, commandSpecifier);

        // TODO: test validations
    }
    
    /**
     * Get Commands issued by a source 
     *
     * Get Commands issued by a source 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getCmdBySeqNoUsingGETTest() {
        String userId = null;
        Long seqNo = null;
        CommandSpecifier response = api.getCmdBySeqNoUsingGET(userId, seqNo);

        // TODO: test validations
    }
    
    /**
     * Get Commands issued by a source 
     *
     * Get Commands issued by a source 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getCmdsByFromUsingGETTest() {
        String userId = null;
        List<CommandSpecifier> response = api.getCmdsByFromUsingGET(userId);

        // TODO: test validations
    }
    
    /**
     * Get Commands History by Device Id and cmd 
     *
     * Get Commands History by Device Id and cmd
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getCmdsHistoryByCmdUsingGETTest() {
        String deviceId = null;
        String cmd = null;
        Long fromTime = null;
        Long toTime = null;
        Integer limit = null;
        List<CommandSpecifier> response = api.getCmdsHistoryByCmdUsingGET(deviceId, cmd, fromTime, toTime, limit);

        // TODO: test validations
    }
    
    /**
     * Get Commands History by Device Id 
     *
     * Get Commands History by Device Id
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getCmdsHistoryByDeviceIdUsingGETTest() {
        String deviceId = null;
        Long fromTime = null;
        Long toTime = null;
        Integer limit = null;
        List<CommandSpecifier> response = api.getCmdsHistoryByDeviceIdUsingGET(deviceId, fromTime, toTime, limit);

        // TODO: test validations
    }
    
    /**
     * Get Commands History by Device Id , Issued from and cmd 
     *
     * Get Commands History by Device Id, Issued from and cmd
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getCmdsHistoryByFromUsingGETTest() {
        String deviceId = null;
        String cmd = null;
        String userId = null;
        Long fromTime = null;
        Long toTime = null;
        Integer limit = null;
        List<CommandSpecifier> response = api.getCmdsHistoryByFromUsingGET(deviceId, cmd, userId, fromTime, toTime, limit);

        // TODO: test validations
    }
    
    /**
     * Get Most Recent Commands by Device Id and Command
     *
     * Get Most Recent Commands by Device Id and Command
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getMostRecentCmdByCmdUsingGETTest() {
        String deviceId = null;
        String cmd = null;
        CommandSpecifier response = api.getMostRecentCmdByCmdUsingGET(deviceId, cmd);

        // TODO: test validations
    }
    
    /**
     * Get Most Recent Commands by Device Id
     *
     * Get Most Recent Commands by Device Id
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getMostRecentCmdsByDeviceIdUsingGETTest() {
        String deviceId = null;
        List<CommandSpecifier> response = api.getMostRecentCmdsByDeviceIdUsingGET(deviceId);

        // TODO: test validations
    }
    
}
